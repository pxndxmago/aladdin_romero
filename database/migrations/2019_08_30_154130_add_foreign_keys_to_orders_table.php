<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->foreign('order_status_code', 'orders_ref_code_id_foreign')->references('id')->on('ref_order_status_codes')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id', 'orders_users_id_foreign')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropForeign('orders_ref_code_id_foreign');
			$table->dropForeign('orders_users_id_foreign');
		});
	}

}
