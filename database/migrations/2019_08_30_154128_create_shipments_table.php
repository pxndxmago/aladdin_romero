<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shipments', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('order_id')->unsigned()->nullable()->index('shipments_orders_id_foreign');
			$table->bigInteger('invoice_id')->unsigned()->nullable()->index('shipments_invoices_id_foreign');
			$table->string('shipment_tracking_number', 250)->nullable();
			$table->dateTime('shipment_date')->nullable();
			$table->string('order_shipment_details', 5000)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shipments');
	}

}
