<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('category_id')->unsigned()->nullable()->index('products_categories_id_foreign');
			$table->string('product_name', 500)->nullable();
			$table->float('product_price', 10, 0)->nullable();
			$table->bigInteger('product_parent_id')->unsigned()->nullable();
			$table->string('product_size', 250)->nullable();
			$table->text('product_description', 65535)->nullable();
			$table->text('product_details', 65535)->nullable();
			$table->integer('units_in_stock')->nullable();
			$table->text('images', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
