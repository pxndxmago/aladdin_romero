<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvoiceLineItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoice_line_items', function(Blueprint $table)
		{
			$table->foreign('invoice_id')->references('id')->on('invoices')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('id', 'invoice_line_items_order_items_id_foreign')->references('id')->on('order_items')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('product_id', 'invoice_line_items_products_id_foreign')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoice_line_items', function(Blueprint $table)
		{
			$table->dropForeign('invoice_line_items_invoice_id_foreign');
			$table->dropForeign('invoice_line_items_order_items_id_foreign');
			$table->dropForeign('invoice_line_items_products_id_foreign');
		});
	}

}
