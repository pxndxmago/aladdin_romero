<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToShipmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('shipments', function(Blueprint $table)
		{
			$table->foreign('invoice_id', 'shipments_invoices_id_foreign')->references('id')->on('invoices')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('order_id', 'shipments_orders_id_foreign')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('shipments', function(Blueprint $table)
		{
			$table->dropForeign('shipments_invoices_id_foreign');
			$table->dropForeign('shipments_orders_id_foreign');
		});
	}

}
