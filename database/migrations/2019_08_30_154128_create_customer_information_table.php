<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerInformationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_information', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('first_name', 500)->nullable();
			$table->string('last_name', 500)->nullable();
			$table->string('dni', 250)->nullable();
			$table->string('telephone', 250)->nullable();
			$table->string('mobile', 250)->nullable();
			$table->string('email', 250)->nullable();
			$table->string('city', 250)->nullable();
			$table->string('province', 250)->nullable();
			$table->string('country', 250)->nullable();
			$table->string('address_1', 5000)->nullable();
			$table->string('address_2', 5000)->nullable();
			$table->bigInteger('user_id')->unsigned()->nullable()->index('customer_information_users_id_foreign');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_information');
	}

}
