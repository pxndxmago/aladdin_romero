<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->foreign('order_id', 'invoices_orders_id_foreign')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('invoice_status_code', 'invoices_ref_invoice_status_codes_foreign')->references('id')->on('ref_invoice_status_codes')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->dropForeign('invoices_orders_id_foreign');
			$table->dropForeign('invoices_ref_invoice_status_codes_foreign');
		});
	}

}
