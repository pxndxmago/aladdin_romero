<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoiceLineItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_line_items', function(Blueprint $table)
		{
			$table->bigInteger('id')->unsigned()->primary();
			$table->bigInteger('invoice_id')->unsigned()->nullable()->index('invoice_line_items_invoice_id_foreign');
			$table->bigInteger('product_id')->unsigned()->nullable()->index('invoice_line_items_products_id_foreign');
			$table->string('product_title', 500)->nullable();
			$table->integer('product_quantity')->nullable();
			$table->float('product_price', 10, 0)->nullable();
			$table->float('derived_product_cost', 10, 0)->nullable();
			$table->float('derived_vat', 10, 0)->nullable();
			$table->float('derived_total_cost', 10, 0)->nullable();
			$table->string('other_line_item_details', 5000)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_line_items');
	}

}
