$(function() {
    var errors = $('#div_errors').alert(); //Div que contiene los errores 

    $('.vertical-quantity').each(function(index, value) {
        var prod = $(this);
        $(this).on("change paste keyup", function() {
            setTimeout(function() {
                var url = $('#form_cart').attr('action') + "/update/" + prod.attr('product_id') + "/" + prod.val();
                $.ajax({
                    url: url,
                    type: "get",
                    success: function() {
                        location.reload();
                    }
                });
            }, 200)
        });
    });

    $("#btn_apply_discount").on("click", function() {

        var discountCode = $("#txt_discount_code").val();
        var url = $('#form_cart').attr('action') + "/addDiscountCode/" + discountCode;
        if (discountCode.trim() != '') {
            $.ajax({
                url: url,
                type: "get",
                success: function(response) {
                    alertify.success(response.message);
                    if (typeof response.route !== 'undefined') {
                        setTimeout(function() {
                            window.location.href = response.route;
                        }, 800);
                    }
                },
                error: function(response) {
                    alertify.error(response.responseJSON.message);
                    if (typeof response.responseJSON.route !== 'undefined') {
                        setTimeout(function() {
                            window.location.href = response.responseJSON.route;
                        }, 800);
                    }
                }
            });
        }
    });

    $("#btn_remove_discount").on("click", function(event) {
        event.preventDefault();
        var url = $('#form_cart').attr('action') + "/removeDiscountCode";
        $.ajax({
            url: url,
            type: "get",
            success: function(response) {
                // location.reload();
                window.location.href = response;
                // console.log(response)
            }
        });
    });

    $('#country').change(function() {
        var country = $('#country').val();
        var url = $('meta[name=url]').attr("content") + '/location/provinces/' + country;
        $.ajax({
            url: url,
            type: "get",
            success: function(response) {
                var $province = $("#province");
                var $city = $("#city");
                $province.html('');
                $city.html('');
                $.each(response, function() {
                    $province.append($("<option />").val(this.id).text(this.province));
                });
            }
        });
    });

    $('#province').change(function() {
        var province = $('#province').val();
        var url = $('meta[name=url]').attr("content") + '/location/cities/' + province;
        $.ajax({
            url: url,
            type: "get",
            success: function(response) {
                var $city = $("#city");
                $city.html('');
                $.each(response, function() {
                    $city.append($("<option />").val(this.id).text(this.city));
                });
            }
        });
    });

    function limpiar_form_address() {
        $('#id_address').val('');
        $('#address_1').val('');
        $('#address_2').val('');

    }
    $('.btn-new-address').on('click', function() {
        limpiar_form_address();
    });

    $('.btn_edit_address').on('click', function() {
        limpiar_form_address();
        $('#id_address').val($(this).attr('id'));
        var url = $('meta[name=url]').attr("content") + "/checkout/edit_customer_add/" + $(this).attr('id');

        $.ajax({
            url: url,
            type: "get",
            success: function(response) {

                $('#address_1').val(response.address_1);
                $('#address_2').val(response.address_2);
            },
            error: function(response) {
                alertify.error("error");
            }
        });
        $('#addressModal').modal('show');

    });

    $('#btn_save_cust_inf').on('click', function() {
        var form_ship = $('#form_customer_inf');
        var url = form_ship.attr('action');
        $.ajax({
            url: url,
            type: "post",
            data: form_ship.serialize(),
            success: function(response) {
                if (response.ok) {
                    location.href = response.redirect
                }
                if (typeof response.errors !== 'undefined') {
                    // console.log(JSON.stringify(response));

                    errors.html("");
                    errors.append("<strong> Revise los siguientes errores </strong>").show();

                    if (typeof response.errors.first_name !== 'undefined') {
                        errors.append("</br>" + response.errors.first_name);
                    }
                    if (typeof response.errors.last_name !== 'undefined') {
                        errors.append("</br>" + response.errors.last_name);
                    }
                    if (typeof response.errors.dni !== 'undefined') {
                        errors.append("</br>" + response.errors.dni);
                    }
                    if (typeof response.errors.telephone !== 'undefined') {
                        errors.append("</br>" + response.errors.telephone);
                    }
                    if (typeof response.errors.mobile !== 'undefined') {
                        errors.append("</br>" + response.errors.mobile);
                    }
                    if (typeof response.errors.email !== 'undefined') {
                        errors.append("</br>" + response.errors.email);
                    }
                }


            },
            error: function(response) {
                alertify.error("error");
                console.log("ERROR")

            }
        });
    })


    $('#btn_place_order').on('click', function() {
        var url = $('meta[name=url]').attr("content") + "/payment/place_order";

        alertify.confirm("Mensaje", "¿Esta seguro que desea realizar esta orden?",
            function() {
                $.ajax({
                    url: url,
                    type: "get",
                    success: function(response) {
                        if (response.ok) {
                            alertify.success(response.message);
                            setTimeout(function() {
                                location.href = response.route;
                            }, 400)
                        }

                    },
                    error: function(response) {
                        alertify.error("error");
                    }
                });
            },
            function() {

            }
        );
    })

    // se establece el metodo de envio con la ruta setShippingMethod

    $('input[type=radio][name=shipping_method]').change(function() {
        var url = $('meta[name=url]').attr("content") + "/cart/setShippingMethod/" + this.value;

        $.ajax({
            url: url,
            type: "get",
            success: function() {
                location.reload();
            },
            error: function() {
                alert("Error");
            }
        });

    });

    var voucher = new Dropzone("#my-dropzone", {
        url: "/payment",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        paramName: "vouchers",
        maxFilesize: 10,
        autoProcessQueue: false,
        addRemoveLinks: true,
        acceptedFiles: "image/png, image/jpg, image/jpeg",
        maxFiles: 2,
        uploadMultiple: true,
        dictRemoveFile: "Remover archivo",
        dictDefaultMessage: "Suelte aqui o seleccione la imagen del </br> comprobante de pago"
    });

    $('#uploadVoucher').on('click', function() {
        voucher.processQueue();
    });
    voucher.on('success', function() {
        location.reload();
    })


})