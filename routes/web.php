<?php
use Illuminate\Support\Facades\Hash;
use App\DiscountCode;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {

        Route::group(['prefix' => 'admin'], function () {
                Voyager::routes();
        });

        Auth::routes();
        
        Route::get('/my-account', 'MyAccountController@index')->name('myaccount.index')->middleware('auth');
        Route::get('/my-account/account-information', 'MyAccountController@account_information')->name('myaccount.account-information')->middleware('auth');
        Route::get('/my-account/my-orders', 'MyAccountController@my_orders')->name('myaccount.my-orders')->middleware('auth');
        Route::get('/my-account/show-order/{order}', 'MyAccountController@show_order')->name('myaccount.show-order')->middleware('signed')->middleware('auth');
        

        Route::get('/', function(){
                return redirect()->route('home');
        });
        Route::get('/home', 'HomeController@index')->name('home');
        
        Route::get('/shop', 'ShopController@index')->name('shop');
        Route::get('/shop/{product_slug}', 'ShopController@product_description')
                ->name('shop.show')
                ->where('product_slug', '[0-9]+');
        
        
        Route::get('/checkout', 'CheckoutController@index')->name('checkout');
        Route::get('/payment', 'PaymentController@index')->name('payment');
        Route::get('/payment/place_order', 'PaymentController@place_order')->name('payment.place_order');
        Route::post('/payment', 'PaymentController@store')->name('payment.store');

        Route::post('/checkout/store_customer_inf', 'CheckoutController@store_customer_information')->name('checkout.store_cust_inf');
        Route::post('/checkout/store_customer_add', 'CheckoutController@store_customer_address')->name('checkout.store_cust_add');
        Route::get('/checkout/edit_customer_add/{address}', 'CheckoutController@edit_customer_address')->name('checkout.edit_cust_add');
        
        Route::get('lang/{lang}', function ($lang) {
                session(['lang' => $lang]);
                return \Redirect::back();
        })->where([
                'lang' => 'en|es'
        ]);

        // Route::bind('product', function($id){
                
        //         return App\Product::where('id',$id)->first();
        
        // });
        
        Route::get('cart/show','CartController@show')
                ->name('cart-show');
        
        Route::get('cart/add/{product}','CartController@add')
                ->name('cart-add');
        
        Route::get('cart/delete/{product}','CartController@delete')
                ->name('cart-delete');
        
        Route::get('cart/trash','CartController@trash')
                ->name('cart-trash');
        
        Route::get('cart/update/{product}/{quantity}','CartController@update')
                ->name('cart-update');
        
        Route::get('cart/addDiscountCode/{discountCode}','CartController@addDiscountCode')
                ->name('cart-addDiscountCode')
                /*->where('discountCode', '[0-9]+')*/;

        Route::get('cart/removeDiscountCode','CartController@removeDiscountCode')
                        ->name('cart-removeDiscountCode');

        Route::get('cart/setAddress/{address}','CartController@setAddress')
                        ->name('cart-setAddress');
                        
        Route::get('cart/setShippingMethod/{shippingMethod}','CartController@setShippingMethod')
                        ->name('cart-setShippingMethod');
        
        Route::get('location/provinces/{country_id}', 'LocationController@provinces');

        Route::get('location/cities/{province_id}', 'LocationController@cities');

        Route::get('about', function(){
                return view('about');
        })->name('about');


        
});

Route::get('/clearsessions', function (Request $request)
{
    $request->session()->flush();

    return redirect('/');
});