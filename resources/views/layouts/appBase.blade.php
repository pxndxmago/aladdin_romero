<!DOCTYPE html>
<html lang="{{App::getLocale()}}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url" content="{{url('')}}">
    <title>{{ setting('site.title') }}</title>

    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Porto - Bootstrap eCommerce Template">
    <meta name="author" content="SW-THEMES">

    <!-- Favicon -->
    {{-- Se carga la el icono con el helper voyager, la imagen se sube y modifica desde el admin --}}
    <link rel="icon" type="image/png" href="{{ Voyager::image(setting('site.image_logo_solo')) }}">


    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{asset('assets/css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/alertify.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themes/semantic.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}">
    
    
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="{{asset('assets/css/themes/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-treeview.css')}}">
    
    @yield('css')
    <style>
        .dropzone {
            background: whitesmoke;
            border-radius: 5px;
            border: 2px dashed rgb(0, 135, 247);
            border-image: none;
            /* max-width: 500px; */
            /* margin-left: auto;
            margin-right: auto; */
            color: rgba(0,0,0,.60);
            font-weight: 500;
            font-size: initial;
            text-transform: uppercase;
        }
    </style>
</head>
@php
    $child_categories = App\Category::with('children')->whereNull('parent_category_id')->get();
    $request = app('request')->all();
@endphp
<body>
   
    <div id="login-popup" class="mfp-content mfp-hide">
        <div class="modal-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="title mb-2">@lang('Login')</h2>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            
                            <label for="login-email">@lang('Email address') <span class="required">*</span></label>
                            <input type="hidden" name="redirect_route" value="{{\Request::fullUrl()}}">
                            <input id="login_email" type="email" name="login_email" value="{{ old('email') }}" class="form-input form-wide mb-2 @error('login_email') is-invalid @enderror" required="" autocomplete="email" autofocus> @error('login_email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span> @enderror

                            <label for="login-password">Password <span class="required">*</span></label>
                            <input id="login_password" type="password" class="form-input form-wide mb-2 @error('password') is-invalid @enderror" id="login-password" name="login_password" required autocomplete="current-password"> @error('login_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span> @enderror

                            <div class="form-footer">

                                <button type="submit" class="btn btn-primary btn-md">
                                    {{ __('Login') }}
                                </button>
                                <div class="custom-control custom-checkbox form-footer-right">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old( 'remember') ? 'checked' : '' }}>

                                    <label class="custom-control-label form-footer-right" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            <!-- End .form-footer -->
                            <a href="#" class="forget-password"> Forgot your password?</a>
                        </form>
                    </div>
                    <!-- End .col-md-6 -->

                    <div class="col-md-6">
                        <h2 class="title mb-2">{{__('register')}}</h2>

                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <label for="name" class="">{{ __('Name') }}</label>
                            <input id="name" type="text" class="form-input form-wide mb-2 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus> @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span> @enderror


                            <label for="email" class="">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-input form-wide mb-2 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"> @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span> @enderror

                            <label for="password" class="">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-input form-wide mb-2 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"> @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span> @enderror

                            <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-input form-wide mb-2" name="password_confirmation" required autocomplete="new-password">
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-md">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                            {{-- ***************************************** --}}

                            {{-- <label for="register-email">Email address <span class="required">*</span></label>
                            <input type="email" class="form-input form-wide mb-2" id="register-email" required="">

                            <label for="register-password">Password <span class="required">*</span></label>
                            <input type="password" class="form-input form-wide mb-2" id="register-password" required="">

                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary btn-md">Register</button>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="newsletter-signup">
                                    <label class="custom-control-label" for="newsletter-signup">Sing up our
                                        Newsletter</label>
                                </div>
                                <!-- End .custom-checkbox -->
                            </div>  --}}
                        </form>
                    </div>
                </div>
                <!-- End .row -->
            </div>
            <!-- End .container -->

            <div class="social-login-wrapper">
                <p>Access your account through your social networks.</p>

                <div class="btn-group">
                    <a class="btn btn-social-login btn-md btn-gplus mb-1"><i
                                class="icon-gplus"></i><span>Google</span></a>
                    <a class="btn btn-social-login btn-md btn-facebook mb-1"><i
                                class="icon-facebook"></i><span>Facebook</span></a>
                    <a class="btn btn-social-login btn-md btn-twitter mb-1"><i
                                class="icon-twitter"></i><span>Twitter</span></a>
                </div>
            </div>
            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        </div>
    </div>
    <div class="page-wrapper">
        <header class="header">
            <div class="header-top">
                <div class="container">
                    <div class="header-left header-dropdowns">
                        {{-- <div class="header-dropdown">
                            <a href="#">USD</a>
                            <div class="header-menu">
                                <ul>
                                    <li><a href="#">EUR</a></li>
                                    <li><a href="#">USD</a></li>
                                </ul>
                            </div>
                            <!-- End .header-menu -->
                        </div> --}}
                        <!-- End .header-dropown -->

                        <div class="header-dropdown">
                            <a href="#"><img src="{{asset('assets/images/flags/'.App::getLocale().'.png')}}" alt="{{App::getLocale()}}">{{App::getLocale()}}</a>
                            <div class="header-menu">
                                <ul>
                                    <li>
                                        <a href="{{ url('lang', ['en']) }}"><img src="{{asset('assets/images/flags/en.png')}}" alt="English">@lang('english')</a>
                                    </li>
                                    <li>
                                    <a href="{{ url('lang', ['es']) }}"><img src="{{asset('assets/images/flags/es.png')}}" alt="Spanish">{{__('spanish')}}</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- End .header-menu -->
                        </div>
                        <!-- End .header-dropown -->
                        <!-- End .dropdown -->
                    </div>
                    <!-- End .header-left -->

                    <div class="header-right">
                        {{-- <p class="welcome-msg">Default welcome msg! </p> --}}
                        @guest
                        <p class="welcome-msg"><a href="#" class="login-link">{{ __('Login') }}</a></p>
                        @else
                        <div class="header-dropdown">
                            <a id="navbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="header-menu">
                                <ul>
                                <li><a href="{{route('myaccount.index')}}">@lang('Account')</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                                <!-- End .dropdownmenu-wrapper -->
                            </div>
                            <!-- End .dropdown-menu -->
                        </div>
                        @endguest
                        <!-- End .header-dropown -->
                    </div>
                    <!-- End .header-right -->
                </div>
                <!-- End .container -->
            </div>
            <!-- End .header-top -->

            <div class="header-middle">
                <div class="container">
                    <div class="header-left">
                        <button class="mobile-menu-toggler" type="button">
                            <i class="icon-menu"></i>
                        </button>
                        <div class="header-search">
                            <a href="#" class="search-toggle" role="button"><i class="icon-magnifier"></i></a>
                            <form method="get" action="{{route('shop', $request)}}">
                                <div class="header-search-wrapper">
                                    <input type="search" class="form-control" name="search" id="search" placeholder="@lang('Search')...">
                                    <div class="select-custom">
                                        <select id="category" name="category">
                                            <option value="">@lang('All categories')</option>
                                            @foreach ($child_categories as $child_category)
                                                <option value="{{$child_category->category_name}}">&#8226; {{$child_category->category_name}}</option>
                                                @foreach ($child_category->children as $child_category2)
                                                    <option value="{{$child_category2->category_name}}">&nbsp;&nbsp;- {{$child_category2->category_name}}</option>
                                                    @foreach ($child_category2->children as $child_category3)
                                                        <option value="{{$child_category3->category_name}}">&nbsp;&nbsp;&nbsp; ☼<span class="glyphicon glyphicon-chevron-right"></span> {{$child_category3->category_name}}</option>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- End .select-custom -->
                                    <button class="btn" type="submit"><i class="icon-magnifier"></i></button>
                                </div>
                                <!-- End .header-search-wrapper -->
                            </form>
                        </div>
                        <!-- End .header-search -->
                    </div>
                    <!-- End .header-left -->

                    <div class="header-center">
                        <a href="{{route('home')}}" class="logo">
                            <img src="{{ Voyager::image(setting('site.logo')) }}" alt="Porto Logo">
                        </a>
                    </div>
                    <!-- End .headeer-center -->

                    <div class="header-right"> 
                        <div class="header-contact">
                            <span>@lang('Call us now')</span>
                            <a href="tel:#"><strong>+123 5678 890</strong></a>
                        </div>
                        <!-- End .header-contact -->

                        @php 
                            $items = \Session::get('cart')['items'];
                            $subtotal = 0;
                        @endphp
                        <div class="dropdown cart-dropdown">
                            <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                <span class="cart-count">{{($items!=null)?count($items):0}}</span>
                            </a>

                            <div class="dropdown-menu">
                                <div class="dropdownmenu-wrapper">
                                    <div class="dropdown-cart-header">
                                        <span>
                                            @if($items!=null)
                                                {{count($items)}}
                                                @if(count($items)==1)
                                                    @lang('item')
                                                @else
                                                    @lang('items')
                                                @endif
                                            @else
                                                @lang('No items in cart')
                                            @endif    
                                        </span>

                                        <a href="{{route('cart-show')}}">@lang('View Cart')</a>
                                    </div>
                                    <!-- End .dropdown-cart-header -->
                                    
                                    @if ($items!=null && $items>0)
                                        <div class="dropdown-cart-products">
                                            @if(isset($items) && !empty($items))
                                                @foreach ($items as $key => $item)
                                                    @if($key == 'discountCode')
                                                        @continue
                                                    @endif
                                                    
                                                    @php 
                                                        $subtotal = $subtotal + $item->product_price * $item->quantity; 
                                                        $images = json_decode( $item->images); 
                                                    @endphp
                                                    <div class="product">
                                                        <div class="product-details">
                                                            <h4 class="product-title">
                                                                <a href="{{route('shop.show',$item->id)}}">{{$item->product_name}}</a>
                                                            </h4>
            
                                                            <span class="cart-product-info">
                                                                <span class="cart-product-qty">{{$item->quantity}}</span> x <strong>${{$item->product_price}}</strong>
                                                            </span>
                                                        </div>
                                                        <!-- End .product-details -->
            
                                                        <figure class="product-image-container">
                                                            <a href="{{route('shop.show',$item->id)}}" class="product-image">
                                                                <img src="{{Voyager::image($images[rand(0, sizeof($images)-1)])}}" alt="product">
                                                            </a>
                                                            <a href="{{route('cart-delete',$item->id)}}" class="btn-remove" title="Remove Product"><i
                                                                    class="icon-cancel"></i></a>
                                                        </figure>
                                                    </div>
                                                @endforeach
                                            @endif
                                            
                                            <!-- End .product -->

                                        </div>
                                        <!-- End .cart-product -->
                                        @php
                                            $total = round(($subtotal * (1+(setting('site.tax_rate')/100))),2) ;
                                        @endphp
                                        <div class="dropdown-cart-total">
                                            <span>Total</span>

                                            <span class="cart-total-price">${{$total}}</span>
                                        </div>
                                        <!-- End .dropdown-cart-total -->

                                        <div class="dropdown-cart-action">
                                            <a href="{{route('checkout')}}" class="btn btn-block">@lang('Checkout')</a>
                                        </div>
                                    @endif        
                                
                                    <!-- End .dropdown-cart-total -->
                                </div>
                                <!-- End .dropdownmenu-wrapper -->
                            </div>
                            <!-- End .dropdown-menu -->
                        </div>
                        <!-- End .dropdown -->
                    </div>
                    <!-- End .header-right -->
                </div>
                <!-- End .container -->
            </div>
            <!-- End .header-middle -->
            
            <div class="header-bottom sticky-header">
                <div class="container">
                    <nav class="main-nav">
                        <ul class="menu sf-arrows">
                            <li ><a href="{{route('home')}}">{{__('Home')}}</a></li>
                            <li class="megamenu-container">
                                <a href="{{route('shop')}}" class="sf-with-ul">{{__('Categories')}}</a>
                                <div class="megamenu">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                @foreach ($child_categories as $category)
                                                    <div class="col-lg-4">
                                                        <div class="menu-title"> 
                                                        <a href="{{route('shop', ['category'=>$category->category_name])}}">{{$category->category_name}}</a>
                                                        </div>
                                                        <ul>
                                                            @foreach ($category->children as $category_child)
                                                                @php
                                                                    $children_2 = $category_child->children;
                                                                    $has_children_2 = false;
                                                                    if($children_2!=null){
                                                                        if(count($children_2)>0){
                                                                            $has_children_2 = true;
                                                                        }
                                                                    }
                                                                @endphp
                                                                <li><a href="{{route('shop', ['category'=>$category_child->category_name])}}" class={{($has_children_2)?'sf-with-ul':''}}>{{$category_child->category_name}}</a>
                                                                    @if ($has_children_2)
                                                                        <ul>
                                                                            @foreach ($children_2 as $category_child_2)
                                                                                <li><a href="{{route('shop', ['category'=>$category_child_2->category_name])}}">{{$category_child_2->category_name}}</a></li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @endif
                                                                </li>
                                                            @endforeach
                                                            
                                                        </ul>
                                                    </div>
                                                    <!-- End .col-lg-4 -->
                                                @endforeach
                                            </div>
                                            <!-- End .row -->
                                        </div>
                                        <!-- End .col-lg-8 -->
                                        {{-- <div class="col-lg-4">
                                            <div class="banner">
                                                <a href="#">
                                                    <img src="assets/images/menu-banner.jpg" alt="Menu banner" class="product-promo">
                                                </a>
                                            </div>
                                            <!-- End .banner -->
                                        </div> --}}
                                        <!-- End .col-lg-4 -->
                                    </div>
                                    <!-- End .row -->
                                </div>
                                <!-- End .megamenu -->
                            </li>
                            <li ><a href="{{route('about')}}">{{__('About')}}</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- End .header-bottom -->
            </div>
            <!-- End .header-bottom -->
        </header>
        <!-- End .header -->

        @yield('content')

        <footer class="footer">
            <div class="footer-middle">
                <div class="container">
                    <div class="footer-ribbon">
                        Get in touch
                    </div>
                    <!-- End .footer-ribbon -->
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="widget">
                                        <ul class="contact-info">
                                            <li>
                                                <span class="contact-info-label">Address:</span>123 Street Name, City, England
                                            </li>
                                            <li>
                                                <span class="contact-info-label">Phone:</span>Toll Free <a href="tel:">(123) 456-7890</a>
                                            </li>
                                            <li>
                                                <span class="contact-info-label">Email:</span> <a href="mailto:mail@example.com">mail@example.com</a>
                                            </li>
                                            <li>
                                                <span class="contact-info-label">Working Days/Hours:</span> Mon - Sun / 9:00AM - 8:00PM
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- End .widget -->
                                </div>
                                <!-- End .col-md-5 -->
                                <div class="col-md-3">
                                    <div class="widget">
                                        <h4 class="widget-title">My Account</h4>

                                        <ul class="links">
                                            <li><a href="{{route('about')}}">{{__('About')}}</a></li>
                                            <li><a href="contact.html">Contact Us</a></li>
                                            <li><a href="my-account.html">My Account</a></li>
                                            <li><a href="#">Orders History</a></li>
                                        </ul>
                                    </div>
                                    <!-- End .widget -->
                                </div>
                                <!-- End .col-md-3 -->
                                <!-- End .col-md-5 -->
                            </div>
                            <!-- End .row -->
                        </div>
                        <!-- End .col-lg-8 -->

                        {{-- <div class="col-lg-4">
                            <div class="widget widget-newsletter">
                                <h4 class="widget-title">Subscribe newsletter</h4>
                                <p>Get all the latest information on Events,Sales and Offers. Sign up for newsletter today
                                </p>
                                <form action="#">
                                    <input type="email" class="form-control" placeholder="Email address" required="">

                                    <input type="submit" class="btn" value="Subscribe">
                                </form>
                            </div>
                            <!-- End .widget -->
                        </div> --}}
                        <!-- End .col-lg-4 -->
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .container -->
            </div>
            <!-- End .footer-middle -->

            <div class="container">
                <div class="footer-bottom">
                    <p class="footer-copyright">Porto eCommerce. &copy; 2018. All Rights Reserved</p>
                    <img src="assets/images/payments.png" alt="payment methods" class="footer-payments">

                    <div class="social-icons">
                        <a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
                        <a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
                        <a href="#" class="social-icon" target="_blank"><i class="icon-linkedin"></i></a>
                    </div>
                    <!-- End .social-icons -->
                </div>
                <!-- End .footer-bottom -->
            </div>
            <!-- End .containr -->
        </footer>
        <!-- End .footer -->
    </div>
    <!-- End .page-wrapper -->

    <div class="mobile-menu-overlay"></div>
    <!-- End .mobil-menu-overlay -->

    <div class="mobile-menu-container">
        <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-cancel"></i></span>
            <nav class="mobile-nav">
                <ul class="mobile-menu">
                    <li><a href="{{route('home')}}">@lang('Home')</a></li>
                    <li>
                        <a href="category.html">@lang('Categories')</a>
                        <ul>
                            @foreach ($child_categories as $category)
                                <li > 
                                    <a href="{{route('shop', ['category'=>$category->category_name])}}">{{$category->category_name}}</a>
                                    <ul>
                                        @foreach ($category->children as $category_child)
                                            @php
                                                $children_2 = $category_child->children;
                                                $has_children_2 = false;
                                                if($children_2!=null){
                                                    if(count($children_2)>0){
                                                        $has_children_2 = true;
                                                    }
                                                }
                                            @endphp
                                            <li><a href="{{route('shop', ['category'=>$category_child->category_name])}}" class={{($has_children_2)?'sf-with-ul':''}}>{{$category_child->category_name}}</a>
                                                @if ($has_children_2)
                                                    <ul>
                                                        @foreach ($children_2 as $category_child_2)
                                                            <li><a href="{{route('shop', ['category'=>$category_child_2->category_name])}}">{{$category_child_2->category_name}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                        
                                    </ul>
                                </li>
                                <!-- End .col-lg-4 -->
                            @endforeach
                        </ul>
                    </li>
                    <li><a href="{{route('about')}}">{{__('About')}}</a></li>
                </ul>
            </nav>
            <!-- End .mobile-nav -->
            <!-- End .social-icons -->
        </div>
        <!-- End .mobile-menu-wrapper -->
    </div>
    <!-- End .mobile-menu-container -->

    <div class="newsletter-popup mfp-hide" id="newsletter-popup-form" style="background-image: url(assets/images/newsletter_popup_bg.jpg)">
        <div class="newsletter-popup-content">
            <img src="assets/images/logo-black.png" alt="Logo" class="logo-newsletter">
            <h2>BE THE FIRST TO KNOW</h2>
            <p>Subscribe to the Porto eCommerce newsletter to receive timely updates from your favorite products.</p>
            <form action="#">
                <div class="input-group">
                    <input type="email" class="form-control" id="newsletter-email" name="newsletter-email" placeholder="Email address" required="">
                    <input type="submit" class="btn" value="Go!">
                </div>
                <!-- End .from-group -->
            </form>
            <div class="newsletter-subscribe">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="1">
                        Don't show this popup again
                    </label>
                </div>
            </div>
        </div>
        <!-- End .newsletter-popup-content -->
    </div>
    <!-- End .newsletter-popup -->

    <a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>

    <!-- Plugins JS File -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins.min.js')}}"></script>
    <script src="{{asset('assets/js/nouislider.min.js')}}"></script>
    <script src="{{asset('assets/js/alertify.min.js')}}"></script>

    <!-- Main JS File -->
    <script src="{{asset('assets/js/main.min.js')}}"></script>
    <script src="{{asset('assets/js/dropzone.js')}}"></script>

    <script src="{{asset('assets/js/bootstrap-treeview.js')}}"></script>

    @yield('js')
    <script>
        Dropzone.autoDiscover = false;
    </script>

    
    <script src="{{asset('js/functions.js')}}"></script>
</body>

</html>