@extends('layouts.appBase')

@section('css')
    {{-- <link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}"> --}}
@endsection

@section('content')
<main class="main">
    {{-- <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
            </ol>
        </div><!-- End .container -->
    </nav> --}}

    <div class="container">
        <div class="row">
            
            @yield('content-2')

            <aside class="sidebar col-lg-3">
                <div class="widget widget-dashboard">
                    <h3 class="widget-title">My Account</h3>

                    <ul class="list">
                        {{-- <li class="active"><a href="#">Account Dashboard</a></li> --}}
                        <li><a href="{{route('myaccount.account-information')}}">@lang('Account Information')</a></li>
                        <li><a href="{{route('myaccount.my-orders')}}">@lang('Orders')</a></li>
                        {{-- <li><a href="#">Billing Agreements</a></li> --}}
                        <li><a href="#">Recurring Profiles</a></li>
                        <li><a href="#">My Product Reviews</a></li>
                        {{-- <li><a href="#">My Tags</a></li> --}}
                        {{-- <li><a href="#">My Wishlist</a></li> --}}
                        {{-- <li><a href="#">My Applications</a></li> --}}
                        {{-- <li><a href="#">Newsletter Subscriptions</a></li> --}}
                        {{-- <li><a href="#">My Downloadable Products</a></li> --}}
                    </ul>
                </div><!-- End .widget -->
            </aside><!-- End .col-lg-3 -->
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="mb-5"></div><!-- margin -->
</main><!-- End .main -->
@endsection

@section('js')
    {{-- <script src="{{asset('assets/js/dropzone.js')}}"></script> --}}
@endsection