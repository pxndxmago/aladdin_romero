@extends('my-account')
@section('content-2')
    <div class="col-lg-9 order-lg-last dashboard-content">
        <h2>Edit Account Information</h2>
        
        <form id="form_customer_inf" action="{{route('checkout.store_cust_inf')}}">
            @csrf
            <div class="row">
                <div class="col-sm-11">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger collapse" role="alert" id="div_errors">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group required-field">
                                <label>@lang('First Name') </label>
                                <input  name="first_name" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->first_name:"")}}">
                            </div><!-- End .form-group -->
    
                            <div class="form-group required-field">
                                <label>@lang('Last Name') </label>
                                <input name="last_name" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->last_name:"")}}">
                            </div><!-- End .form-group -->
                            <div class="form-group required-field">
                                <label>@lang('Dni') </label>
                                <input name="dni" type="text" class="form-control" value="{{(($cust_inf!=null)?$cust_inf->dni:"")}}">
                            </div><!-- End .form-group --> 
                        </div>
                        
                        <div class="col-md-6">
                            
                            <div class="form-group required-field">
                                <label>@lang('Email')  </label>
                                <input name="email" type="email" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->email:"")}}">
                            </div><!-- End .form-group -->
    
                            <div class="form-group required-field">
                                <label>@lang('Telephone') </label>
                                <input name="telephone" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->telephone:"")}}">
                            </div><!-- End .form-group -->
    
                            <div class="form-group required-field">
                                <label>@lang('Mobile')  </label>
                                <input name="mobile" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->mobile:"")}}">
                            </div><!-- End .form-group -->

                        </div>
                    </div><!-- End .row -->
                </div><!-- End .col-sm-11 -->
            </div><!-- End .row -->

            <div id="account-chage-pass">
                <h3 class="mb-2">@lang('Change Password')</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required-field">
                            <label for="acc-pass2">Password</label>
                            <input type="password" class="form-control" id="acc-pass2" name="acc-pass2">
                        </div><!-- End .form-group -->
                    </div><!-- End .col-md-6 -->

                    <div class="col-md-6">
                        <div class="form-group required-field">
                            <label for="acc-pass3">Confirm Password</label>
                            <input type="password" class="form-control" id="acc-pass3" name="acc-pass3">
                        </div><!-- End .form-group -->
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End #account-chage-pass -->

            <div class="required text-right">* Required Field</div>
            <div class="form-footer">
                <a href="#"><i class="icon-angle-double-left"></i>Back</a>

                <div class="form-footer-right">
                    <button id="btn_save_cust_inf" type="button" class="btn btn-primary float-right">@lang('Save')</button>
                </div>
            </div><!-- End .form-footer -->
        </form>
    </div><!-- End .col-lg-9 -->
@endsection