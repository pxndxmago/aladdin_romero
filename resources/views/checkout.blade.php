@extends('layouts.appBase')
@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Checkout</li>
            </ol>
        </div><!-- End .container -->
    </nav>
    {{-- Address modal --}}
    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="form_shipping_add" method="post" action="{{route('checkout.store_cust_add')}}">
                    @csrf
                    <input id="id_address" name="id_address" type="hidden">
                    <div class="modal-header">
                        <h3 class="modal-title" id="addressModalLabel">Shipping Address</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div><!-- End .modal-header -->

                    <div class="modal-body">
                        <div class="form-group required-field">
                            <label>@lang('Country')</label>
                            <select class="form-control" name="country" id="country">
                                <option value="0">@lang('Select a country')</option>
                                @foreach ($countries as $country)
                                    <option {{(($cust_inf!=null)?($cust_inf->country_name== $country->country)?"SELECTED":"":"")}} value="{{$country->id}}">{{$country->country}}</option>
                                @endforeach
                            </select>
                        </div><!-- End .form-group -->
            
                        <div class="form-group required-field">
                            <label>@lang('Province')</label>
                            <select class="form-control" name="province" id="province">
                                <option value="0">@lang('Select a province')</option>
                                @if($cust_inf!=null)
                                    <option selected value="{{$cust_inf->province}}">{{$cust_inf->province_name}}</option>                                            
                                @endif
                            </select>
                        </div><!-- End .form-group -->
                        
                        <div class="form-group required-field">
                            <label>@lang('City')</label>
                            <select class="form-control" name="city" id="city">
                                <option value="0">@lang('Select a city')</option>
                                @if($cust_inf!=null)
                                    <option selected value="{{$cust_inf->city}}">{{$cust_inf->city_name}}</option>                                            
                                @endif     
                            </select>
                        </div><!-- End .form-group -->
            
                        <div class="form-group required-field">
                            <label>@lang('Address')  </label>
                            <input id="address_1" name="address_1" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->address_1:"")}}">
                        </div><!-- End .form-group -->
            
                        <div class="form-group required-field">
                            <label>@lang('Reference')  </label>
                            <input id="address_2" name="address_2" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->address_2:"")}}">
                        </div><!-- End .form-group -->
                    </div><!-- End .modal-body -->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">@lang('Cancel')</button>
                        <button  type="submit" class="btn btn-primary btn-sm">@lang('Save changes')</button>
                    </div><!-- End .modal-footer -->
                </form>
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </div><!-- End .modal -->
    
    
    <div class="container">
        <ul class="checkout-progress-bar">
            <li class="active">
                <span>@lang('Shipping')</span>
            </li>
            <li>
                <span>@lang('Review &amp; Payments')</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-lg-8">
                <ul class="checkout-steps">
                    <form id="form_customer_inf" action="{{route('checkout.store_cust_inf')}}">
                        @csrf
                        <li>
                            <h2 class="step-title">@lang('Shipping address')</h2>
                            @guest
                                <p>@lang('You must to be logged in first')</p>
                                <button type="button"  class="btn btn-primary login-link">@lang('Login')</button>
                            @else
                                {{-- ******************** Datos de usuario ********************* --}}
                                
                                <div class="alert alert-danger collapse alert-dismissible {{(\Session::has('errors_'))?'show':''}} " role="alert" id="div_errors">
                                    @if (\Session::has('errors_'))
                                        @foreach (\Session::get('errors_') as $error)
                                            <span>{{$error}}</span></br>
                                        @endforeach
                                    @endif
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>

                                <div class="checkout-discount">
                                    <h4 >
                                        <a data-toggle="collapse" href="#customer_information_section" class="collapsed" role="button" aria-expanded="false" aria-controls="shipment-information-section">Informacion de usuario</a>
                                    </h4>

                                    <div class="collapse" id="customer_information_section">
                                        <div class="form-group required-field">
                                            <label>@lang('First Name') </label>
                                        <input  name="first_name" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->first_name:"")}}">
                                        </div><!-- End .form-group -->

                                        <div class="form-group required-field">
                                            <label>@lang('Last Name') </label>
                                            <input name="last_name" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->last_name:"")}}">
                                        </div><!-- End .form-group -->
                                        
                                        <div class="form-group required-field">
                                            <label>@lang('Dni') </label>
                                            <input name="dni" type="text" class="form-control" value="{{(($cust_inf!=null)?$cust_inf->dni:"")}}">
                                        </div><!-- End .form-group -->
                                        
                                        <div class="form-group required-field">
                                            <label>@lang('Email')  </label>
                                            <input name="email" type="email" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->email:"")}}">
                                        </div><!-- End .form-group -->

                                        <div class="form-group required-field">
                                            <label>@lang('Telephone') </label>
                                            <input name="telephone" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->telephone:"")}}">
                                        </div><!-- End .form-group -->

                                        <div class="form-group required-field">
                                            <label>@lang('Mobile')  </label>
                                            <input name="mobile" type="text" class="form-control" required="" value="{{(($cust_inf!=null)?$cust_inf->mobile:"")}}">
                                        </div><!-- End .form-group -->
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="checkout-steps-action">
                                                    <button id="btn_save_cust_inf" type="button" class="btn btn-primary float-right">@lang('Save')</button>                                                    
                                                </div><!-- End .checkout-steps-action -->
                                            </div><!-- End .col-lg-8 -->
                                        </div><!-- End .row -->
                                    </div>
                                </div>
                                {{-- ******************** Datos de envio ********************* --}}
                                <div class="checkout-discount">
                                    <h4 > 
                                        <a data-toggle="collapse" href="#shipment-information-section" class="collapsed" role="button" aria-expanded="false" aria-controls="shipment-information-section">Informacion de envio</a>
                                    </h4>
                            
                                    <div class="collapse show" id="shipment-information-section">
                                            <div class="shipping-step-addresses">
                                                
                                                @foreach ($cust_add as $address)
                                                    {{-- Si en el carrito existe una direccion ingresada se consulta por cada iteracion si es la que se encuentra seleccionada se marca como active  --}}
                                                    <div class="shipping-address-box {{(isset($cart['shipping_address'])?(($cart['shipping_address']!=null )?($cart['shipping_address']->id == $address->id)?'active':'':''):"")}}">
                                                        <address>
                                                                {{$address->address_1}} <br>
                                                                {{$address->address_2}} <br><br>
                                                                {{$address->city}} <br>
                                                                {{$address->province}} <br>
                                                                {{$address->country}} <br>

                                                        </address>
                
                                                        <button type="button" id="{{$address->id}}" class="btn btn-sm btn-link btn_edit_address">
                                                            @lang('Edit')
                                                        </button>
                                                        
                                                        <div class="address-box-action clearfix">
                                                            <a href="{{route('cart-setAddress', $address->id)}}" class="btn btn-sm btn-outline-secondary float-right">
                                                                @lang('Ship Here')
                                                            </a>
                                                        </div><!-- End .address-box-action -->
                                                    </div><!-- End .shipping-address-box -->
                                                    
                                                @endforeach
                                            </div><!-- End .shipping-step-addresses -->
                                        <a href="#" class="btn btn-sm btn-outline-secondary btn-new-address" data-toggle="modal" data-target="#addressModal">+ @lang('New Address')</a>
                                    </div>
                                </div>
                            @endguest
                        </li>
                        @auth
                        <li>
                            <div class="checkout-step-shipping">
                                <h2 class="step-title">@lang('Shipping Methods')</h2>
                                
                                <table class="table table-step-shipping">
                                    <tbody>
                                        @foreach ($shipping_methods as $key => $method)
                                            <tr>
                                                <td><input {{(isset($cart['shipping_method'])?($cart['shipping_method']!=null)?(($cart['shipping_method']->id==$method->id)?"checked='checked'":''):'':'')}} type="radio" name="shipping_method" value="{{$method->id}}"></td>
                                                <td><strong>${{$method->cost}}</strong></td>
                                                <td>{{$method->shipping_method}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- End .checkout-step-shipping -->
                        </li>  
                        @endauth
                    </form>        
                </ul>
            </div><!-- End .col-lg-8 -->

            <div class="col-lg-4">
                <div class="order-summary">
                    <h3>@lang('Your products')</h3>

                    <h4>
                        <a data-toggle="collapse" href="#order-cart-section" class="collapsed" role="button" aria-expanded="false" aria-controls="order-cart-section">
                            @if($cart!=null)
                                {{count($cart['items'])}}
                                @if(count($cart['items'])==1)
                                    @lang('item')
                                @else
                                    @lang('items')
                                @endif
                            @else
                                @lang('No items in cart')
                            @endif
                        </a>
                    </h4>

                    <div class="collapse" id="order-cart-section">
                        <table class="table table-mini-cart">
                            <tbody>
                                @isset($cart)
                                    @foreach ($cart['items'] as $key => $item)
                                        @if($key == 'discountCode')
                                            @continue
                                        @endif
                                        @php
                                            $images = json_decode( $item->images);
                                        @endphp
                                        <tr>
                                            <td class="product-col">
                                                <figure class="product-image-container">
                                                    <a href="{{route('shop.show',$item->id)}}" class="product-image">
                                                        <img src="{{Voyager::image($images[0])}}" alt="product">
                                                    </a>
                                                </figure>
                                                <div>
                                                    <h2 class="product-title">
                                                        <a href="{{route('shop.show', $item->id)}}">{{$item->product_name}}</a>
                                                    </h2>
                                                    <span class="product-qty">{{$item->quantity}}</span>
                                                </div>
                                            </td>
                                            <td class="price-col">
                                                ${{$item->product_price}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>    
                        </table>
                    </div><!-- End #order-cart-section -->
                </div><!-- End .order-summary -->
            </div><!-- End .col-lg-4 -->
        </div><!-- End .row -->

        @auth
            <div class="row">
                <div class="col-lg-8">
                    <div class="checkout-steps-action">
                        <a id="btn_next_checkout" href="{{route('payment')}}" class="btn btn-primary float-right">@lang('Next')</a>
                    </div><!-- End .checkout-steps-action -->
                </div><!-- End .col-lg-8 -->
            </div><!-- End .row -->
        @endauth
        <div class="mb-6"></div>
    </div><!-- End .container -->
@endsection