@php
    $images_carr = json_decode( $product->images);
@endphp

<div class="product-slider-container product-item">
    <div product_id="{{$product->id}}" class=" product-single-carousel owl-carousel owl-theme">
        @foreach ($images_carr as $image_carr)
            <div class="product-item">
                <img class="product-single-image" src="{{Voyager::image($image_carr)}}" data-zoom-image="{{Voyager::image($image_carr)}}"/>
            </div>
        @endforeach
    </div>
    <!-- End .product-single-carousel -->
</div>
<div product_id="{{$product->id}}" class="prod-thumbnail row owl-dots" id='carousel-custom-dots{{$product->id}}'>
    @foreach ($images_carr as $image_dot)
        <div class="col-3 owl-dot">
            <img src="{{Voyager::image($image_dot)}}"/>
        </div>
    @endforeach
</div>