@extends('my-account')
@section('content-2')
<div class="col-lg-9 order-lg-last dashboard-content">
    <h2>@lang('Orders')</h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="text-align: center;">@lang('Date order placed')</th>
                <th style="text-align: right;">@lang('Order #')</th>
                <th style="text-align: right;">@lang('Amount')</th>
                <th style="text-align: center;">@lang('Status')</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $key => $order)
                <tr>
                    <td style="text-align: center;">{{$order->date_order_placed}}</td>
                <td style="text-align: right;"><a href="{{URL::signedRoute('myaccount.show-order',$order->order_id)}}">{{$order->order_id}}</a></td>
                    <td style="text-align: right;">${{$order->amount}}</td>
                    <td style="text-align: center;">{{$order->order_status_description}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection