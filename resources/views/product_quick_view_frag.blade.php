<div product_id="{{$product->id}}" id="product_quick_view{{$product->id}}" class="product-single-container product-single-default product-quick-view container mfp-hide">
    <div class="row">
        <div class="col-lg-6 col-md-6 product-single-gallery">
            @include('product_carrousel_frag',compact('product'))
        </div><!-- End .col-lg-7 -->

        <div class="col-lg-6 col-md-6">
            <div class="product-single-details">
                <h1 class="product-title">{{$product->product_name}}</h1>

                <div class="ratings-container">
                    <div class="product-ratings">
                        <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                    </div><!-- End .product-ratings -->

                    <a href="#" class="rating-link">( 6 Reviews )</a>
                </div><!-- End .product-container -->

                <div class="price-box">
                    {{-- <span class="old-price">$81.00</span> --}}
                    <span class="product-price">${{$product->product_price}}</span>
                </div><!-- End .price-box -->

                <div class="product-desc">
                    <p>{{$product->product_quick_description}}</p>
                </div><!-- End .product-desc -->

                {{-- <div class="product-filters-container">
                    <div class="product-single-filter">
                        <label>Colors:</label>
                        <ul class="config-swatch-list">
                            <li class="active">
                                <a href="#" style="background-color: #6085a5;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #ab6e6e;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #b19970;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #11426b;"></a>
                            </li>
                        </ul>
                    </div><!-- End .product-single-filter -->
                </div><!-- End .product-filters-container --> --}}

                <div class="product-action">
                    <div class="product-single-qty">
                        <input class="horizontal-quantity form-control" type="text">
                    </div><!-- End .product-single-qty -->

                    <a href="{{route('cart-add',$product->id)}}" class="paction add-cart" title="@lang('Add to Cart')">
                        <span>@lang('Add to Cart')</span>
                    </a>
                    {{-- <a href="#" class="paction add-wishlist" title="@lang('Add to Wishlist')">
                        <span>@lang('Add to Wishlist')</span>
                    </a> --}}
                    {{-- <a href="#" class="paction add-compare" title="@lang('Add to Compare')">
                        <span>@lang('Add to Compare')</span>
                    </a> --}}
                </div><!-- End .product-action -->

                <div class="product-single-share">
                    <label>Share:</label>
                    <!-- www.addthis.com share plugin-->
                    <div class="addthis_inline_share_toolbox"></div>
                </div><!-- End .product single-share -->
            </div><!-- End .product-single-details -->
        </div><!-- End .col-lg-5 -->
    </div><!-- End .row -->
</div><!-- End .product-single-container -->
