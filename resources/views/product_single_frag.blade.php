@php
    $images = json_decode( $product->images)
@endphp
<div class="product">
    <figure class="product-image-container">
        <a href="{{route('shop.show',[$product->id])}}" class="product-image">
            <img src="{{Voyager::image($images[rand(0, sizeof($images)-1)])}}" alt="product">
        </a>
        <a href="product_quick_view{{$product->id}}" class="btn-quickview">{{__('quick view')}}</a>
    </figure>
    <div class="product-details">
        {{-- <div class="ratings-container">
            <div class="product-ratings">
                <span class="ratings" style="width:80%"></span><!-- End .ratings -->
            </div><!-- End .product-ratings -->
        </div><!-- End .product-container --> --}}
        <h2 class="product-title">
            <a href="{{ route('shop.show', [$product->id]) }}">{{$product->product_name}}</a>
        </h2>
        <div class="price-box">
        <span class="product-price">${{$product->product_price}}</span>
        </div><!-- End .price-box -->

        <div class="product-action">
            {{-- <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                <span>Add to Wishlist</span>
            </a> --}}  

            <a href="{{route('cart-add',[$product->id])}}" class="paction add-cart" title="@lang('Add to Cart')">
                <span>@lang('Add to Cart')</span>
            </a>

            {{-- <a href="#" class="paction add-compare" title="Add to Compare">
                <span>Add to Compare</span>
            </a> --}}
        </div><!-- End .product-action -->
    </div><!-- End .product-details -->
</div><!-- End .product -->