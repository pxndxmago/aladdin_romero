@extends('layouts.appBase')

@section('content')

<main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav mb-1">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Shopping Cart</li>
                </ol>
            </div><!-- End .container -->
        </nav>

        <form id="form_cart" action="{{url('/').'/cart'}}">
        </form>
    
        <div class="container">
            <div class="row">
                @if(!empty($cart['items']))
                    <div class="col-lg-8">
                        <div class="cart-table-container">
                            <table class="table table-cart">
                                <thead>
                                    <tr>
                                        <th class="product-col">@lang('Product')</th>
                                        <th class="price-col">@lang('Price')</th>
                                        <th class="qty-col">@lang('Quantity')</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cart['items'] as $key => $item)
                                        @if($key == 'discountCode')
                                            @continue
                                        @endif
                                        @php
                                            $images = json_decode( $item->images)
                                        @endphp
                                        <tr class="product-row">
                                            <td class="product-col">
                                                <figure class="product-image-container">
                                                    <a href="{{route('shop.show',$item->id)}}" class="product-image">
                                                    <img src="{{Voyager::image($images[0])}}" alt="product">
                                                    </a>
                                                </figure>
                                                <h2 class="product-title">
                                                    <a href="{{route('shop.show', $item->id)}}">{{$item->product_name}}</a>
                                                </h2>
                                            </td>
                                            <td>${{$item->product_price}}</td>
                                            <td>
                                                <input product_id="{{$item->id}}" class="vertical-quantity form-control" type="text" value="{{$item->quantity}}">
                                            </td>
                                            <td>${{$item->product_price * $item->quantity}}</td>
                                        </tr>
                                        <tr class="product-action-row">
                                            <td colspan="4" class="clearfix">
                                                {{-- <div class="float-left">
                                                    <a href="#" class="btn-move">@lang('Move to Wishlist')</a>
                                                </div><!-- End .float-left --> --}}
                                                
                                                <div class="float-right">
                                                    {{-- <a href="#" title="Edit product" class="btn-edit"><span class="sr-only">@lang('Edit')</span><i class="icon-pencil"></i></a> --}}
                                                    <a href="{{route('cart-delete',$item->id)}}" title="Remove product" class="btn-remove"><span class="sr-only">@lang('Remove')</span></a>
                                                </div><!-- End .float-right -->
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="4" class="clearfix">
                                            <div class="float-left">
                                                <a href="{{route('shop')}}" class="btn btn-outline-secondary">@lang('Continue Shopping')</a>
                                            </div><!-- End .float-left -->

                                            <div class="float-right">
                                                <a href="{{route('cart-trash')}}" class="btn btn-outline-secondary btn-clear-cart">@lang('Clear Shopping Cart')</a>
                                            </div><!-- End .float-right -->
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- End .cart-table-container -->

                        <div class="cart-discount">
                            <h4>@lang('Apply Discount')</h4>
                            <form action="#">
                                <div class="input-group">
                                    <input id="txt_discount_code" type="text" class="form-control form-control-sm" placeholder="@lang('Enter discount code')" required="">
                                    <div class="input-group-append">
                                        <button id="btn_apply_discount" class="btn btn-sm btn-primary" type="button">@lang('Apply Discount')</button>
                                    </div>
                                </div><!-- End .input-group -->
                            </form>
                        </div><!-- End .cart-discount -->
                    </div><!-- End .col-lg-8 -->

                    <div class="col-lg-4">
                        <div class="cart-summary">
                            <h3>@lang('Your products')</h3>

                            {{-- <h4>
                                <a data-toggle="collapse" href="#total-estimate-section" class="collapsed" role="button" aria-expanded="false" aria-controls="total-estimate-section">Estimate Shipping and Tax</a>
                            </h4>

                            <div class="collapse" id="total-estimate-section">
                                <form action="#">
                                    <div class="form-group form-group-sm">
                                        <label>Country</label>
                                        <div class="select-custom">
                                            <select class="form-control form-control-sm">
                                                <option value="USA">United States
                                                <option value="Turkey">Turkey
                                                <option value="China">China
                                                <option value="Germany">Germany
                                            </select>
                                        </div><!-- End .select-custom -->
                                    </div><!-- End .form-group -->

                                    <div class="form-group form-group-sm">
                                        <label>State/Province</label>
                                        <div class="select-custom">
                                            <select class="form-control form-control-sm">
                                                <option value="CA">California
                                                <option value="TX">Texas
                                            </select>
                                        </div><!-- End .select-custom -->
                                    </div><!-- End .form-group -->

                                    <div class="form-group form-group-sm">
                                        <label>Zip/Postal Code</label>
                                        <input type="text" class="form-control form-control-sm">
                                    </div><!-- End .form-group -->

                                    <div class="form-group form-group-custom-control">
                                        <label>Flat Way</label>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="flat-rate">
                                            <label class="custom-control-label" for="flat-rate">Fixed $5.00</label>
                                        </div><!-- End .custom-checkbox -->
                                    </div><!-- End .form-group -->

                                    <div class="form-group form-group-custom-control">
                                        <label>Best Rate</label>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="best-rate">
                                            <label class="custom-control-label" for="best-rate">Table Rate $15.00</label>
                                        </div><!-- End .custom-checkbox -->
                                    </div><!-- End .form-group -->
                                </form>
                            </div><!-- End #total-estimate-section --> --}}

                            <table class="table table-totals">
                                <tbody>
                                    <tr>
                                        <td>Subtotal</td>
                                        <td>${{$subtotal}}</td>
                                    </tr>
                                    @if (isset($cart['discountCode']))
                                    <tr>
                                        <td>@lang('Discount code'): <strong>{{$cart['discountCode']->code}}</strong> <a id="btn_remove_discount" href="#" title="Remove product" class="btn-remov"><i class="icon-cancel"></i></a> </td>                                          
                                        <td>
                                            {{-- <a href="#" title="Edit product" class="btn-edit"><span class="sr-only">Editar</span><i class="icon-pencil"></i></a> --}}
                                            <strong>-${{$discount}}</strong>
                                        </td>                                            
                                    </tr>
                                    <tr>
                                        <td>@lang('Subtotal - discount'): </td>                                          
                                        <td>
                                            ${{$subtotal_d}}
                                        </td>                                            
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>@lang('Tax')</td>
                                        <td>${{$tax}}</td>
                                    </tr>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>@lang('Order Total')</td>
                                        <td>${{$total}}</td>
                                    </tr>
                                    
                                </tfoot>
                            </table>
                            
                            <div class="checkout-methods">
                                <a href="{{route('checkout')}}" class="btn btn-block btn-sm btn-primary">@lang('Go to Checkout')</a>
                                {{-- <a href="#" class="btn btn-link btn-block">Check Out with Multiple Addresses</a> --}}
                            </div><!-- End .checkout-methods -->
                        </div><!-- End .cart-summary -->
                    </div><!-- End .col-lg-4 -->
                @else
                <div class="header-center">
                    <p><img src="https://media.giphy.com/media/1MzKjWBamP58Q/giphy.gif"></img></p>
                </div>
                @endif
            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb-6"></div><!-- margin -->
    </main><!-- End .main -->
@endsection