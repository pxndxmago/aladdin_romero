@extends('layouts.appBase')

@section('content')
    <main class="main">
        <div class="home-slider-container d-none d-sm-none d-md-block">
            <div class="home-slider owl-carousel ">
                @foreach($slider_images as $slider_image) 
                    <div class="home-slide">
                    <a href="{{ url(($slider_image->url!=null)?$slider_image->url:'') }}"><div class="slide-bg owl-lazy" data-src="{{Voyager::image($slider_image->image_path)}}"></div></a>
                        <!-- End .slide-bg -->
                        {{-- Condicion para ubicar el texto del slider a derecha o izquierda--}}
                        
                        <div class="home-slide-content container">
                        @if(!$slider_image->text_right)
                            <div class="col-md-6 offset-md-6 col-lg-5 offset-lg-7">
                        @endif
                            <h4>{{$slider_image->label_1}}</h4>
                            <h1>{{$slider_image->label_2}}</h1>
                            <h3>{{$slider_image->label_3}}</h3>
                        @if(!$slider_image->text_right)
                            </div>
                        @endif
                        </div><!-- End .home-slide-content -->
                    </div><!-- End .home-slide -->
                @endforeach
            </div><!-- End .home-slider -->
        </div><!-- End .home-slider-container -->

        <div class="info-boxes-container">
            <div class="container">
                <div class="info-box">
                    <i class="icon-shipping"></i>

                    <div class="info-box-content">
                        <h4>FREE SHIPPING & RETURN</h4>
                        <p>Free shipping on all orders over $99.</p>
                    </div><!-- End .info-box-content -->
                </div><!-- End .info-box -->

                <div class="info-box">
                    <i class="icon-us-dollar"></i>

                    <div class="info-box-content">
                        <h4>MONEY BACK GUARANTEE</h4>
                        <p>100% money back guarantee</p>
                    </div><!-- End .info-box-content -->
                </div><!-- End .info-box -->

                
            </div><!-- End .container -->
        </div><!-- End .info-boxes-container -->

        <div class="home-product-tabs">
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">
                    {{-- <li class="nav-item">
                        <a class="nav-link active" id="featured-products-tab" data-toggle="tab"
                            href="#featured-products" role="tab" aria-controls="featured-products"
                            aria-selected="true">Featured Products</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" id="latest-products-tab" data-toggle="tab" href="#latest-products"
                            role="tab" aria-controls="latest-products" aria-selected="false">{{__('latest products')}}</a>
                    </li>
                </ul>
            </div><!-- End .container -->
            <div class="tab-content">
                <div class="tab-pane fade show active" id="featured-products" role="tabpanel"
                    aria-labelledby="featured-products-tab">
                    <div class="container">
                        <div class="tab-products-carousel owl-carousel owl-theme">
                            @foreach ($latest_products as $product)
                                @include('product_single_frag', compact('product'))
                            @endforeach
                        </div><!-- End .products-carousel -->
                        
                            @foreach ($latest_products as $product)
                                @include('product_quick_view_frag', compact('product'))
                            @endforeach
                    </div><!-- End .container -->
                </div><!-- End .tab-pane -->
            </div><!-- End .tab-content -->
        </div><!-- End .home-product-tabs -->

        {{-- <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="feature-box">
                        <i class="icon-star"></i>

                        <div class="feature-box-content">
                            <h3>Dedicated Service</h3>
                            <p>Consult our specialists for help with an order, customization, or design advice</p>
                            <a href="#" class="btn btn-sm btn-outline-dark">Get in touch</a>
                        </div><!-- End .feature-box-content -->
                    </div><!-- End .feature-box -->
                </div><!-- End .col-md-4 -->

                <div class="col-md-4">
                    <div class="feature-box">
                        <i class="icon-reply"></i>

                        <div class="feature-box-content">
                            <h3>Free returns</h3>
                            <p>We stand behind our goods and services and want you to be satisfied with them.</p>
                            <a href="#" class="btn btn-sm btn-outline-dark">Return Policy</a>
                        </div><!-- End .feature-box-content -->
                    </div><!-- End .feature-box -->
                </div><!-- End .col-md-4 -->

                <div class="col-md-4">
                    <div class="feature-box">
                        <i class="icon-paper-plane"></i>

                        <div class="feature-box-content">
                            <h3>international shipping</h3>
                            <p>Currently over 50 countries qualify for express international shipping.</p>
                            <a href="#" class="btn btn-sm btn-outline-dark">Learn More</a>
                        </div><!-- End .feature-box-content -->
                    </div><!-- End .feature-box -->
                </div><!-- End .col-md-4 -->
            </div><!-- End .row -->
        </div><!-- End .container --> --}}

        <div class="mb-1"></div><!-- margin -->

        {{-- <div class="promo-section" style="background-image: url(assets/images/promo-bg.jpg)">
            <div class="container">
                <h3>the perfect gift</h3>
                <h4>Check off your list with our best gifts</h4>
                <a href="#" class="btn btn-primary">Shop Now</a>
            </div><!-- End .container -->
        </div><!-- End .promo-section --> --}}

        {{-- <div class="partners-container">
            <div class="container">
                <div class="partners-carousel owl-carousel owl-theme">
                    <a href="#" class="partner">
                        <img src="assets\images\logos\1.png" alt="logo">
                    </a>
                    <a href="#" class="partner">
                        <img src="assets\images\logos\2.png" alt="logo">
                    </a>
                    <a href="#" class="partner">
                        <img src="assets\images\logos\3.png" alt="logo">
                    </a>
                    <a href="#" class="partner">
                        <img src="assets\images\logos\4.png" alt="logo">
                    </a>
                    <a href="#" class="partner">
                        <img src="assets\images\logos\5.png" alt="logo">
                    </a>
                    <a href="#" class="partner">
                        <img src="assets\images\logos\2.png" alt="logo">
                    </a>
                    <a href="#" class="partner">
                        <img src="assets\images\logos\1.png" alt="logo">
                    </a>
                </div><!-- End .partners-carousel -->
            </div><!-- End .container -->
        </div><!-- End .partners-container --> --}}

        {{-- <div class="container">
            <div class="row">
                <div class="col-6 col-lg-3">
                    <div class="product-column">
                        <h3 class="h4 title">Featured</h3>

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-1.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Ring</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$45.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-2.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Headphone</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="old-price">$60.00</span>
                                    <span class="product-price">$45.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-3.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Shoes</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$50.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .product-column -->
                </div><!-- End .col-lg-3 -->

                <div class="col-6 col-lg-3">
                    <div class="product-column">
                        <h3 class="h4 title">New</h3>

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-1.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Ring</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$13.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-3.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Headphone</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="old-price">$42.00</span>
                                    <span class="product-price">$27.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-2.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Shoes</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$35.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .product-column -->
                </div><!-- End .col-lg-3 -->

                <div class="col-6 col-lg-3">
                    <div class="product-column">
                        <h3 class="h4 title">Hot</h3>

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-4.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Ring</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$13.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-5.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Headphone</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="old-price">$42.00</span>
                                    <span class="product-price">$27.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-6.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Shoes</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$35.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .product-column -->
                </div><!-- End .col-lg-3 -->

                <div class="col-6 col-lg-3">
                    <div class="product-column">
                        <h3 class="h4 title">Sale</h3>

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-7.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Ring</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$13.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-8.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Headphone</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="old-price">$42.00</span>
                                    <span class="product-price">$27.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product product-sm">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="assets\images\products\small\product-9.jpg" alt="product">
                                </a>
                            </figure>
                            <div class="product-details">
                                <h2 class="product-title">
                                    <a href="product.html">Shoes</a>
                                </h2>
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <div class="price-box">
                                    <span class="product-price">$35.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .product-column -->
                </div><!-- End .col-lg-3 -->
            </div><!-- End .container -->
        </div><!-- End .container --> --}}

        <div class="mb-6"></div><!-- margin -->
    </main><!-- End .main -->
@endsection
