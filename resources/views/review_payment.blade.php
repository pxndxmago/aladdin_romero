@extends('layouts.appBase')
@section('content')
<div class="container">
        <ul class="checkout-progress-bar">
            <li>
                <span>@lang('Shipping')</span>
            </li>
            <li class="active">
                <span>@lang('Review &amp; Payments')</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-lg-4">
                <div class="order-summary">
                    <h3>@lang('Your products')</h3>

                    <h4>
                        <a data-toggle="collapse" href="#order-cart-section" class="collapsed" role="button" aria-expanded="false" aria-controls="order-cart-section">
                            @if($cart['items']!=null)
                                {{count($cart['items'])}}
                                @if(count($cart['items'])==1)
                                    @lang('item')
                                @else
                                    @lang('items')
                                @endif
                            @else
                                @lang('No items in cart')
                            @endif
                        </a>
                    </h4>

                    <div class="collapse" id="order-cart-section">
                        <table class="table table-mini-cart">
                            <tbody>
                                @isset($cart)
                                    @foreach ($cart['items'] as $key=> $item)
                                        @if($key == 'discountCode')
                                            @continue
                                        @endif
                                        @php
                                            $images = json_decode( $item->images); 
                                        @endphp
                                        <tr>
                                            <td class="product-col">
                                                <figure class="product-image-container">
                                                    <a href="{{route('shop.show',$item->id)}}" class="product-image">
                                                        <img src="{{Voyager::image($images[0])}}" alt="product">
                                                    </a>
                                                </figure>
                                                <div>
                                                    <h2 class="product-title">
                                                        <a href="product.html">{{$item->product_name}}</a>
                                                    </h2>
                                                    <span class="product-qty">{{$item->quantity}}</span>
                                                </div>
                                            </td>
                                            <td class="price-col">${{$item->product_price}}</td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>    
                        </table>
                    </div><!-- End #order-cart-section -->
                </div><!-- End .order-summary -->
                <!-- End .order-summary -->

                <div class="checkout-info-box">
                    <h3 class="step-title">@lang('Ship To'):
                        <a href="{{route('checkout')}}" title="Edit" class="step-title-edit"><span class="sr-only">@lang('Edit')</span><i class="icon-pencil"></i></a>
                    </h3>

                    <address>
                        @php
                            $cust_add = $cart['shipping_address'];
                            
                        @endphp
                        {{$cust_add->address_1}} <br>
                        {{$cust_add->address_2}}<br>
                        {{$cust_add->province}}<br>
                        {{$cust_add->city}}<br>
                        {{$cust_inf->telephone}} <br>
                        {{$cust_inf->mobile}}
                    </address>
                </div>
                <!-- End .checkout-info-box -->

                <div class="checkout-info-box">
                    <h3 class="step-title">@lang('Shipping Method'):
                        <a href="#" title="Edit" class="step-title-edit"><span class="sr-only">Edit</span><i class="icon-pencil"></i></a>
                    </h3>
                    @isset($cart['shipping_method'])
                        @if ($cart['shipping_method']!=null)
                        <p>{{$cart['shipping_method']->shipping_method}} - ${{$cart['shipping_method']->cost}}</p>
                            
                        @endif
                    @endisset
                </div>
                <!-- End .checkout-info-box -->
            </div>
            <!-- End .col-lg-4 -->

            <div class="col-lg-8 order-lg-first">
                <div class="checkout-payment">
                    <h2 class="step-title">@lang('Terms and conditions'):</h2>
                    <div class="container">
                            {!! setting('site.terminos_condiciones') !!}
                    </div>
                    
                    {{-- <div class="form-group-custom-control">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="change-bill-address" value="1">
                            <label class="custom-control-label" for="change-bill-address">My billing and shipping address are the same</label>
                        </div>
                        <!-- End .custom-checkbox -->
                    </div> --}}
                    <!-- End .form-group -->

                    <!-- End #checkout-shipping-address -->
                    {{-- <div id="new-checkout-address" class="show">
                        <form action="#">
                            <div class="form-group required-field">
                                <label>First Name </label>
                                <input type="text" class="form-control" required="">
                            </div>
                            <!-- End .form-group -->
                            
                            <div class="form-group required-field">
                                <label>Last Name </label>
                                <input type="text" class="form-control" required="">
                            </div>
                            <!-- End .form-group -->
                            
                            <div class="form-group">
                                <label>Company </label>
                                <input type="text" class="form-control">
                            </div>
                            <!-- End .form-group -->
                            
                            <div class="form-group required-field">
                                <label>Street Address </label>
                                <input type="text" class="form-control" required="">
                                <input type="text" class="form-control" required="">
                            </div>
                            <!-- End .form-group -->
                            
                            <div class="form-group required-field">
                                <label>City  </label>
                                <input type="text" class="form-control" required="">
                            </div>
                            <!-- End .form-group -->
                            
                            <div class="form-group">
                                <label>State/Province</label>
                                <div class="select-custom">
                                    <select class="form-control">
                                        <option value="CA">California
                                            <option value="TX">Texas
                                            </select>
                                        </div>
                                        <!-- End .select-custom -->
                                    </div>
                                    <!-- End .form-group -->
                                    
                                    <div class="form-group required-field">
                                        <label>Zip/Postal Code </label>
                                        <input type="text" class="form-control" required="">
                                    </div>
                                    <!-- End .form-group -->
                                    
                                    <div class="form-group">
                                        <label>Country</label>
                                        <div class="select-custom">
                                            <select class="form-control">
                                                <option value="USA">United States
                                                    <option value="Turkey">Turkey
                                                        <option value="China">China
                                                            <option value="Germany">Germany
                                                            </select>
                                                        </div>
                                                        <!-- End .select-custom -->
                                                    </div>
                                                    <!-- End .form-group -->
                                                    
                                                    <div class="form-group required-field">
                                                        <label>Phone Number </label>
                                                        <div class="form-control-tooltip">
                                                            <input type="tel" class="form-control" required="">
                                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                </div>
                                <!-- End .form-control-tooltip -->
                            </div>
                            <!-- End .form-group -->
                            
                            <div class="form-group-custom-control">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="address-save">
                                    <label class="custom-control-label" for="address-save">Save in Address book</label>
                                </div>
                                <!-- End .custom-checkbox -->
                            </div>
                            <!-- End .form-group -->
                        </form>
                    </div> --}}
                    <!-- End #new-checkout-address -->
                    
                    <div class="clearfix">
                            <a href="{{route('checkout')}}" class="btn btn-primary float-left">@lang('Back')</a>
                            <button id="btn_place_order" type="button" class="btn btn-primary float-right">@lang('Place Order')</button>
                    </div>
                    <!-- End .clearfix -->
                </div>
                <!-- End .checkout-payment -->
                
                
                {{-- <div class="checkout-discount">
                    <h4>
                        <a data-toggle="collapse" href="#checkout-discount-section" class="collapsed" role="button" aria-expanded="false" aria-controls="checkout-discount-section">Apply Discount Code</a>
                    </h4>
                    
                    <div class="collapse" id="checkout-discount-section">
                        <form action="#">
                            <input type="text" class="form-control form-control-sm" placeholder="Enter discount code" required="">
                            <button class="btn btn-sm btn-outline-secondary" type="submit">Apply Discount</button>
                        </form>
                    </div>
                    <!-- End .collapse -->
                </div> --}}
                <!-- End .checkout-discount -->
            </div>
            <!-- End .col-lg-8 -->
        </div>
        <!-- End .row -->
    </div>
    <!-- End .container -->

    <div class="mb-6"></div>
@endsection
