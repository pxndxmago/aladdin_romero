
@foreach ($categories as $category) 
    <li><span class="caret"><a href="{{route('shop', ['category'=>$category->category_name])}}">{{$category->category_name}}</a></span>
        <ul class="nested category-list">
            @include('category_frag', ['categories'=>$category->children])
        </ul>
    </li>
@endforeach
