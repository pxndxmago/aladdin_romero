@extends('layouts.appBase')

@section('content')
    <main class="main">
        <div class="page-header page-header-bg" style="background-image: url('assets/images/page-header-bg.jpg');">
            <div class="container">
                <h1>
                    @lang('About Us')
                </h1>
            </div><!-- End .container -->
        </div>
        <div class="history-section">
            <div class="container">
                <p class="lead text-center">CRISOL, es el sitio de compras online del Ecuador, en donde se puede encontrar variedad de productos en línea de hogar, tecnología, electrónica entre otros a los precios más bajos del mercado.
                    CRISOL facilita la vida de las personas para que realicen sus compras desde la comodidad de su hogar o trabajo desde una Tablet, laptop o un Smartphone; ya que en la actualidad por las múltiples responsabilidades se hace complicado asistir a una tienda a realizar la búsqueda de cualquier artículo, en CRISOL tenemos como filosofía dar la facilidad a los clientes de adquirir cualquier tipo de producto las 24 horas los 365 días a la semana.
                    Contamos con altos estándares de seguridad lo que certifica que nuestros clientes reciban sus productos en perfectas condiciones y en un mínimo de tiempo; de esta manera garantizamos el 100% de satisfacción de cada uno de nuestros usuarios.
                    Se parte de nuestra comunidad y atrévete a realizar todas tus compras de manera online, olvídate de las filas de las cajas de los centros comerciales, ahorra tiempo, economiza tu dinero, todo eso gracias a CRISOL.</p>
                <div class="mb-6"></div>

            </div>
        </div>
    </main><!-- End .main -->
@endsection
