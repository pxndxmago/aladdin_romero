@extends('my-account')
@section('content-2')
<div class="col-lg-9 order-lg-last dashboard-content">
    <h2>@lang('Order') # {{$order->id}}</h2>
    <h3>{{$invoice->invoice_status_description}}</h3>
    @if ($f_payable)
        <span class="alert-danger">
            <strong >@lang('You must to pay this invoice so you will receive your order')</strong>
        </span>
    @endif
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center;">@lang('Product')</th>
                    <th style="text-align: center;">@lang('Quantity')</th>
                    <th style="text-align: center;">@lang('Derived product cost')</th>
                    <th style="text-align: center;">@lang('Derived Vat')</th>
                    <th style="text-align: right;">@lang('Total')</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $tax = 0;
                    $subtotal_d = 0;
                    $total = 0;
                @endphp
                @foreach ($order_items as $item)
                    @php
                        $images = json_decode( $item->images);
                        $tax = $tax + $item->derived_vat;
                        $subtotal_d = $subtotal_d + $item->derived_product_cost ;
                        $total = $total + $item->derived_total_cost
                    @endphp
                    <tr>
                        <td class="product-col">
                            <figure class="product-image-container">
                                <a href="{{route('shop.show',$item->product_id)}}" class="product-image">
                                        <img src="{{Voyager::image($images[0])}}" alt="product">
                                </a>
                            </figure>

                        </td>
                        <td>{{$item->product_quantity}}</td>
                        <td>{{$item->derived_product_cost}}</td>
                        <td>{{$item->derived_vat}}</td>
                        <td>{{$item->derived_total_cost}}</td>
                    </tr>
                @endforeach 
            </tbody>
        </table>
        <div class="cart-summary">
            <table class="table table-totals">
                <tbody>
                    <tr>
                        <td>Subtotal</td>
                        <td>${{$subtotal_d}}</td>
                    </tr>
                    <tr>
                        <td>@lang('Tax')</td>
                        <td>${{$tax}}</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>@lang('Order Total')</td>
                        <td>${{$total}}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        @if ($f_payable)
            <div class="row">
                <div class="col-md-6">
                    <form class="dropzone needsclick dz-clickable" id="my-dropzone">
                        <input id="invoice_id" name="invoice_id" value="{{$invoice->id}}" type="hidden">
                    </form>
                </div>
                <div class="col-md-6">
                    <button id="uploadVoucher" class="btn btn-outline-secondary" type="reset">@lang('Send') <i class="far fa-paper-plane"></i> </button>
                </div>
            </div>
        @endif
        @if (isset($payments) and $payments!=null )
            @if (count($payments)>0)
                <h2>@lang('Payments')</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align: center;">@lang('Date order placed')</th>
                            <th style="text-align: right;">@lang('Amount')</th>
                            <th style="text-align: right;">@lang('Amount')</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($payments as $key => $payment)
                            <tr>
                                <td style="text-align: center;">{{$payment->payment_date}}</td>
                                <td style="text-align: right;">${{($payment->payment_amount==null)?"0.00":$payment->payment_amount}}</td>
                                <td style="text-align: right;">{{($payment->f_checked === null)?"Sin revisar":(($payment->f_checked==1)? "Aceptado" : "Rechazado")}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endif
        {{-- <div id="template-preview">
            <div class="dz-preview dz-file-preview well" id="dz-preview-template">
                    <div class="dz-details">
                            <div class="dz-filename"><span data-dz-name></span></div>
                            <div class="dz-size" data-dz-size></div>
                    </div>
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <div class="dz-success-mark"><span></span></div>
                    <div class="dz-error-mark"><span></span></div>
                    <div class="dz-error-message"><span data-dz-errormessage></span></div>
            </div>
        </div> --}}
</div>
@endsection