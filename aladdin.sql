-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-09-2019 a las 22:55:01
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aladdin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `parent_category_id`, `category_name`, `category_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, NULL, 'Tecnología', 'electronicos', '2019-09-26 16:49:00', '2019-09-26 21:11:51', NULL),
(9, 8, 'Computacion', 'computacion', '2019-09-26 16:49:00', '2019-09-26 21:13:27', NULL),
(10, 9, 'Accesorios de computación', 'accesorios computacion', '2019-09-26 16:50:00', '2019-09-26 21:13:59', NULL),
(11, 16, 'Monitores', 'monitores', '2019-09-26 17:14:00', '2019-09-26 21:13:45', NULL),
(12, 8, 'Seguridad', 'seguridad', '2019-09-26 17:15:00', '2019-09-26 21:12:39', NULL),
(13, 8, 'Audio', 'audio', '2019-09-26 17:15:00', '2019-09-26 21:13:14', NULL),
(14, 13, 'Audifonos', 'audifonos', '2019-09-26 17:15:00', '2019-09-26 21:15:03', NULL),
(15, 8, 'Impresoras', 'impresoras', '2019-09-26 17:16:00', '2019-09-26 21:14:42', NULL),
(16, 8, 'Video', 'video', '2019-09-26 17:18:00', '2019-09-26 21:13:00', NULL),
(17, 8, 'Televisores', 'televisores', '2019-09-26 17:27:00', '2019-09-26 21:12:07', NULL),
(18, 9, 'Laptops', 'laptops', '2019-09-26 17:46:00', '2019-09-26 21:12:52', NULL),
(19, NULL, 'Hogar', NULL, '2019-09-26 19:21:58', '2019-09-26 19:21:58', NULL),
(20, 19, 'Decoración', NULL, '2019-09-26 19:22:28', '2019-09-26 19:22:28', NULL),
(21, 20, 'Figuras decorativas', NULL, '2019-09-26 19:54:37', '2019-09-26 19:54:37', NULL),
(22, 20, 'Cojines Almohadas', NULL, '2019-09-26 19:54:00', '2019-09-26 20:11:53', NULL),
(23, 20, 'Adornos navideños', NULL, '2019-09-26 19:57:53', '2019-09-26 19:57:53', NULL),
(24, 19, 'Dormitorio', NULL, '2019-09-26 20:33:21', '2019-09-26 20:33:21', NULL),
(25, 19, 'Cocina', NULL, '2019-09-26 20:40:22', '2019-09-26 20:40:22', NULL),
(26, 13, 'Parlantes', NULL, '2019-09-26 21:15:22', '2019-09-26 21:15:22', NULL),
(27, NULL, 'Belleza', NULL, '2019-09-26 21:27:38', '2019-09-26 21:27:38', NULL),
(28, 27, 'Cuidado capilar', NULL, '2019-09-26 21:28:07', '2019-09-26 21:28:07', NULL),
(29, 27, 'Cuidado del cuerpo y SPA', NULL, '2019-09-26 21:28:33', '2019-09-26 21:28:33', NULL),
(30, 27, 'Cuidado del rostro', NULL, '2019-09-26 21:29:03', '2019-09-26 21:46:03', '2019-09-26 21:46:03'),
(31, NULL, 'Accesorios', NULL, '2019-09-30 21:48:31', '2019-09-30 21:48:31', NULL),
(32, 31, 'Relojes', NULL, '2019-09-30 21:48:51', '2019-09-30 21:48:51', NULL),
(33, 32, 'Smartwatch', NULL, '2019-09-30 21:54:30', '2019-09-30 21:54:30', NULL),
(34, 32, 'Relojes de mujer', NULL, '2019-09-30 21:54:42', '2019-09-30 21:54:42', NULL),
(35, 32, 'Relojes de hombre', NULL, '2019-09-30 21:54:55', '2019-09-30 21:54:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `city`, `created_at`, `updated_at`, `deleted_at`, `province_id`, `country_id`) VALUES
(1, 'CUENCA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(2, 'GIRÓN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(3, 'GUALACEO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(4, 'NABÓN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(5, 'PAUTE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(6, 'PUCARA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(7, 'SAN FERNANDO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(8, 'SANTA ISABEL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(9, 'SIGSIG', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(10, 'OÑA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(11, 'CHORDELEG', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(12, 'EL PAN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(13, 'SEVILLA DE ORO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(14, 'GUACHAPALA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(15, 'CAMILO PONCE ENRÍQUEZ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 1, NULL),
(16, 'GUARANDA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(17, 'CHILLANES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(18, 'CHIMBO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(19, 'ECHEANDÍA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(20, 'SAN MIGUEL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(21, 'CALUMA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(22, 'LAS NAVES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 2, NULL),
(23, 'AZOGUES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(24, 'BIBLIÁN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(25, 'CAÑAR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(26, 'LA TRONCAL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(27, 'EL TAMBO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(28, 'DÉLEG', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(29, 'SUSCAL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 3, NULL),
(30, 'TULCÁN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 4, NULL),
(31, 'BOLÍVAR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 4, NULL),
(32, 'ESPEJO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 4, NULL),
(33, 'MIRA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 4, NULL),
(34, 'MONTÚFAR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 4, NULL),
(35, 'SAN PEDRO DE HUACA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 4, NULL),
(36, 'LATACUNGA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(37, 'LA MANÁ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(38, 'PANGUA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(39, 'PUJILI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(40, 'SALCEDO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(41, 'SAQUISILÍ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(42, 'SIGCHOS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 5, NULL),
(43, 'RIOBAMBA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(44, 'ALAUSI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(45, 'COLTA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(46, 'CHAMBO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(47, 'CHUNCHI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(48, 'GUAMOTE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(49, 'GUANO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(50, 'PALLATANGA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(51, 'PENIPE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(52, 'CUMANDÁ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 6, NULL),
(53, 'MACHALA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(54, 'ARENILLAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(55, 'ATAHUALPA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(56, 'BALSAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(57, 'CHILLA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(58, 'EL GUABO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(59, 'HUAQUILLAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(60, 'MARCABELÍ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(61, 'PASAJE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(62, 'PIÑAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(63, 'PORTOVELO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(64, 'SANTA ROSA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(65, 'ZARUMA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(66, 'LAS LAJAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 7, NULL),
(67, 'ESMERALDAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(68, 'ELOY ALFARO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(69, 'MUISNE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(70, 'QUININDÉ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(71, 'SAN LORENZO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(72, 'ATACAMES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(73, 'RIOVERDE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(74, 'LA CONCORDIA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 8, NULL),
(75, 'GUAYAQUIL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(76, 'ALFREDO BAQUERIZO MORENO (JUJÁN)', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(77, 'BALAO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(78, 'BALZAR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(79, 'COLIMES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(80, 'DAULE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(81, 'DURÁN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(82, 'EL EMPALME', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(83, 'EL TRIUNFO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(84, 'MILAGRO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(85, 'NARANJAL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(86, 'NARANJITO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(87, 'PALESTINA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(88, 'PEDRO CARBO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(89, 'SAMBORONDÓN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(90, 'SANTA LUCÍA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(91, 'SALITRE (URBINA JADO)', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(92, 'SAN JACINTO DE YAGUACHI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(93, 'PLAYAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(94, 'SIMÓN BOLÍVAR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(95, 'CORONEL MARCELINO MARIDUEÑA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(96, 'LOMAS DE SARGENTILLO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(97, 'NOBOL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(98, 'GENERAL ANTONIO ELIZALDE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(99, 'ISIDRO AYORA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 9, NULL),
(100, 'IBARRA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 10, NULL),
(101, 'ANTONIO ANTE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 10, NULL),
(102, 'COTACACHI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 10, NULL),
(103, 'OTAVALO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 10, NULL),
(104, 'PIMAMPIRO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 10, NULL),
(105, 'SAN MIGUEL DE URCUQUÍ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 10, NULL),
(106, 'LOJA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(107, 'CALVAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(108, 'CATAMAYO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(109, 'CELICA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(110, 'CHAGUARPAMBA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(111, 'ESPÍNDOLA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(112, 'GONZANAMÁ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(113, 'MACARÁ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(114, 'PALTAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(115, 'PUYANGO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(116, 'SARAGURO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(117, 'SOZORANGA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(118, 'ZAPOTILLO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(119, 'PINDAL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(120, 'QUILANGA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(121, 'OLMEDO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 11, NULL),
(122, 'BABAHOYO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(123, 'BABA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(124, 'MONTALVO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(125, 'PUEBLOVIEJO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(126, 'QUEVEDO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(127, 'URDANETA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(128, 'VENTANAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(129, 'VÍNCES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(130, 'PALENQUE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(131, 'BUENA FÉ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(132, 'VALENCIA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(133, 'MOCACHE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(134, 'QUINSALOMA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 12, NULL),
(135, 'PORTOVIEJO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(136, 'BOLÍVAR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(137, 'CHONE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(138, 'EL CARMEN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(139, 'FLAVIO ALFARO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(140, 'JIPIJAPA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(141, 'JUNÍN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(142, 'MANTA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(143, 'MONTECRISTI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(144, 'PAJÁN', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(145, 'PICHINCHA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(146, 'ROCAFUERTE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(147, 'SANTA ANA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(148, 'SUCRE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(149, 'TOSAGUA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(150, '24 DE MAYO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(151, 'PEDERNALES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(152, 'OLMEDO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(153, 'PUERTO LÓPEZ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(154, 'JAMA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(155, 'JARAMIJÓ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(156, 'SAN VICENTE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 13, NULL),
(157, 'MORONA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(158, 'GUALAQUIZA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(159, 'LIMÓN INDANZA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(160, 'PALORA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(161, 'SANTIAGO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(162, 'SUCÚA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(163, 'HUAMBOYA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(164, 'SAN JUAN BOSCO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(165, 'TAISHA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(166, 'LOGROÑO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(167, 'PABLO SEXTO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(168, 'TIWINTZA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 14, NULL),
(169, 'TENA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 15, NULL),
(170, 'ARCHIDONA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 15, NULL),
(171, 'EL CHACO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 15, NULL),
(172, 'QUIJOS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 15, NULL),
(173, 'CARLOS JULIO AROSEMENA TOLA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 15, NULL),
(174, 'PASTAZA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 16, NULL),
(175, 'MERA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 16, NULL),
(176, 'SANTA CLARA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 16, NULL),
(177, 'ARAJUNO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 16, NULL),
(178, 'QUITO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(179, 'CAYAMBE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(180, 'MEJIA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(181, 'PEDRO MONCAYO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(182, 'RUMIÑAHUI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(183, 'SAN MIGUEL DE LOS BANCOS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(184, 'PEDRO VICENTE MALDONADO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(185, 'PUERTO QUITO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 17, NULL),
(186, 'AMBATO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(187, 'BAÑOS DE AGUA SANTA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(188, 'CEVALLOS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(189, 'MOCHA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(190, 'PATATE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(191, 'QUERO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(192, 'SAN PEDRO DE PELILEO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(193, 'SANTIAGO DE PÍLLARO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(194, 'TISALEO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 18, NULL),
(195, 'ZAMORA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(196, 'CHINCHIPE', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(197, 'NANGARITZA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(198, 'YACUAMBI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(199, 'YANTZAZA (YANZATZA)', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(200, 'EL PANGUI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(201, 'CENTINELA DEL CÓNDOR', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(202, 'PALANDA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(203, 'PAQUISHA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 19, NULL),
(204, 'SAN CRISTÓBAL', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 20, NULL),
(205, 'ISABELA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 20, NULL),
(206, 'SANTA CRUZ', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 20, NULL),
(207, 'LAGO AGRIO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(208, 'GONZALO PIZARRO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(209, 'PUTUMAYO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(210, 'SHUSHUFINDI', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(211, 'SUCUMBÍOS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(212, 'CASCALES', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(213, 'CUYABENO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 21, NULL),
(214, 'ORELLANA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 22, NULL),
(215, 'AGUARICO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 22, NULL),
(216, 'LA JOYA DE LOS SACHAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 22, NULL),
(217, 'LORETO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 22, NULL),
(218, 'SANTO DOMINGO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 23, NULL),
(219, 'SANTA ELENA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 24, NULL),
(220, 'LA LIBERTAD', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 24, NULL),
(221, 'SALINAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 24, NULL),
(222, 'LAS GOLONDRINAS', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 90, NULL),
(223, 'MANGA DEL CURA', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 90, NULL),
(224, 'EL PIEDRERO', '2019-09-11 14:36:57', '2019-09-11 14:37:26', NULL, 90, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `country`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ECUADOR', '2019-09-10 19:26:13', '2019-09-10 19:26:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address_1` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `customer_addresses`
--

INSERT INTO `customer_addresses` (`id`, `address_1`, `address_2`, `created_at`, `updated_at`, `deleted_at`, `user_id`, `city_id`) VALUES
(15, 'Guayacanes mzn 230', 'frente a la sanja', '2019-09-19 19:45:26', '2019-09-19 21:50:31', '2019-09-19 21:50:31', 3, 75),
(16, 'balsas de oro', 'frente a la sanja', '2019-09-19 19:45:49', '2019-09-19 20:17:22', '2019-09-19 20:17:22', 3, 75),
(17, 'sauces 9', 'frente a la sanja', '2019-09-19 20:17:22', '2019-09-19 21:25:15', '2019-09-19 21:25:15', 3, 17),
(19, 'sauces 9', 'frente al parque', '2019-09-19 21:47:58', '2019-09-20 14:12:20', '2019-09-20 14:12:20', 3, 46),
(20, 'la ciudad', 'del espejo', '2019-09-19 21:50:12', '2019-09-19 21:50:12', NULL, 3, 32),
(21, 'Guayacanes mzn 250', 'frente a la sanja', '2019-09-19 21:50:31', '2019-09-19 21:50:31', NULL, 3, 75),
(22, 'sauces 9', 'frente al parque', '2019-09-20 14:12:20', '2019-09-20 14:12:20', NULL, 1, 130),
(23, '123', '6666', '2019-09-20 14:34:35', '2019-09-20 14:34:35', NULL, 1, 90),
(24, 'por la montaña', 'por las llamas', '2019-09-25 19:38:47', '2019-09-25 19:38:47', NULL, 4, 30),
(25, 'dsee', 'dde', '2019-09-26 16:07:52', '2019-09-26 16:07:52', NULL, 7, 75),
(26, NULL, NULL, '2019-09-26 21:50:40', '2019-09-26 21:50:40', NULL, 1, NULL),
(27, 'sss', 'sss', '2019-09-30 14:43:16', '2019-09-30 14:43:16', NULL, 6, 75),
(28, 'ddd', 'ddd', '2019-09-30 19:11:18', '2019-09-30 19:11:18', NULL, 8, 75),
(29, 'no se', 'no me preguntes', '2019-09-30 21:07:03', '2019-09-30 21:07:03', NULL, 9, 32),
(30, 'a', 'a', '2019-09-30 21:28:35', '2019-09-30 21:28:35', NULL, 9, 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer_information`
--

CREATE TABLE `customer_information` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dni` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `customer_information`
--

INSERT INTO `customer_information` (`id`, `first_name`, `last_name`, `dni`, `telephone`, `mobile`, `email`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '0993411408', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-11 20:43:57', '2019-09-11 20:43:57'),
(5, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '0993411408', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-11 20:44:23', '2019-09-11 20:44:23'),
(6, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '0993411408', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-11 22:44:40', '2019-09-11 22:44:40'),
(14, 'ruffo', 'perrito', '085214578', '4444', '55555', 'ruffito@noexiste.com', 3, NULL, '2019-09-19 16:09:35', '2019-09-19 16:09:35'),
(18, 'ruffo', 'perrito', '0919459396', '4444', '55555', 'ruffito@noexiste.com', 3, NULL, '2019-09-20 20:35:39', '2019-09-20 20:35:39'),
(19, 'ruffo', 'perrito', '0919459398', '4444', '55555', 'ruffito@noexiste.com', 3, NULL, '2019-09-20 20:38:16', '2019-09-20 20:38:16'),
(20, 'patricio', 'estrella rosada', '0914512457', '042621878', '0996148267', 'patricio@gmail.com', 4, NULL, '2019-09-25 19:28:44', '2019-09-25 19:28:44'),
(21, 'Monica', 'Gomez', '09283772663', '2322456', '0947772334', 'mgomez@romerodyasociados.com', 7, NULL, '2019-09-26 16:06:41', '2019-09-26 16:06:41'),
(22, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '09935', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-26 22:19:22', '2019-09-26 22:19:22'),
(23, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '09935551', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-26 22:29:07', '2019-09-26 22:29:07'),
(24, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '0993777', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-26 22:32:23', '2019-09-26 22:32:23'),
(25, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '099377789', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-26 22:35:06', '2019-09-26 22:35:06'),
(26, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '88883', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-26 22:36:00', '2019-09-26 22:36:00'),
(27, 'arturo moises', 'ronquillo mackliff', '0919459396', '010101', '88883', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-27 16:51:33', '2019-09-27 16:51:33'),
(28, 'arturo moises', 'ronquillo mackliff', '0919459396', '010101', 'a', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 14:24:08', '2019-09-30 14:24:08'),
(29, 'arturo moises', 'ronquillo mackliff', '0919459396', '010101', '55555', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 14:25:34', '2019-09-30 14:25:34'),
(30, 'dd', 'dd', '55145', '0994526564', '478445', 'abimorales2393@gmail.com', 6, NULL, '2019-09-30 14:44:21', '2019-09-30 14:44:21'),
(31, 'moshiii', 'ronquillo mackliff', '0919459396', '010101', '55555', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 15:28:10', '2019-09-30 15:28:10'),
(32, 'moshiii', 'ronquillo mackliff', '0919459396', '010101', '55555', 'arturo.ronquillo94', 1, NULL, '2019-09-30 15:31:46', '2019-09-30 15:31:46'),
(33, 'art', 'ronquillomackliff', '0919459396', '010101', '55555', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:18:08', '2019-09-30 16:18:08'),
(34, 'arturomoises', 'ronquillomackliff', '0919459396', '010101', '55555', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:19:53', '2019-09-30 16:19:53'),
(35, 'arturo moises', 'ronquillomackliff', '0919459396', '010101', '55555', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:27:27', '2019-09-30 16:27:27'),
(36, 'arturo moises', 'ronquillomackliff', '0919459396', '010101', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:28:17', '2019-09-30 16:28:17'),
(37, 'arturo moises', 'ronquillomackliff', '0919459396', '042621878', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:29:26', '2019-09-30 16:29:26'),
(38, 'arturo moi', 'ronquillomackliff', '0919459396', '042621878', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:30:06', '2019-09-30 16:30:06'),
(39, 'arturo moi', 'ronquillo mm', '0919459396', '042621878', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:30:14', '2019-09-30 16:30:14'),
(40, 'arturo moises', 'ronquillo mackliff', '0919459396', '042621878', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:30:27', '2019-09-30 16:30:27'),
(41, 'arturo moises', 'ronquillo mackliff', '0919459396', '04262187854654', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:36:09', '2019-09-30 16:36:09'),
(42, 'arturo moises', 'ronquillo mackliff', '0919459396', '814521458', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:45:45', '2019-09-30 16:45:45'),
(43, 'arturo moises', 'ronquillo mackliff', '0919459394', '814521458', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 16:47:37', '2019-09-30 16:47:37'),
(44, 'arturo moises', 'ronquillo mackliff', '0919459394', '0426218784', '0996148267', 'arturo.ronquillo94@gmail.com', 1, NULL, '2019-09-30 17:33:32', '2019-09-30 17:33:32'),
(45, 'Abigail', 'Morales', '0940706351', '0994526564', '0994526564', 'abimorales2393@gmail.com', 8, NULL, '2019-09-30 18:59:46', '2019-09-30 18:59:46'),
(46, '', 'la gata', '0914512456', '0426189745', '0995214545', 'michi@gmail.com', 9, NULL, '2019-09-30 20:57:55', '2019-09-30 20:57:55'),
(47, 'michi', 'la gata', '0914512456', '0426189745', '0995214545', 'michi@gmail.com', 9, NULL, '2019-09-30 21:06:45', '2019-09-30 21:06:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 5, 'image_path', 'image', 'Image Path', 0, 1, 1, 1, 1, 1, '{}', 2),
(24, 5, 'label_1', 'text', 'Label 1', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 5, 'label_2', 'text', 'Label 2', 0, 1, 1, 1, 1, 1, '{}', 4),
(26, 5, 'label_3', 'text', 'Label 3', 0, 1, 1, 1, 1, 1, '{}', 5),
(27, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(28, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(29, 5, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 9),
(30, 5, 'text_right', 'checkbox', 'Text Right', 0, 1, 1, 1, 1, 1, '{}', 6),
(31, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(32, 6, 'parent_category_id', 'text', 'Parent Category Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(33, 6, 'category_name', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 3),
(34, 6, 'category_description', 'text', 'Descripcion', 0, 1, 1, 1, 1, 1, '{}', 4),
(35, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(36, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(37, 6, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 7),
(38, 6, 'category_hasone_category_relationship', 'relationship', 'Categoría padre', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"parent_category_id\",\"key\":\"id\",\"label\":\"category_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(39, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 7, 'category_id', 'text', 'Category Id', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\"},\"validation\":{\"rule\":[\"required\"]}}', 3),
(41, 7, 'product_name', 'text', 'Product Name', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"},\"min\":0}', 2),
(42, 7, 'product_price', 'number', 'Product Price', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\"},\"min\":0,\"validation\":{\"rule\":[\"required\"]}}', 4),
(43, 7, 'product_parent_id', 'text', 'Product Parent Id', 0, 1, 1, 1, 1, 1, '{}', 14),
(44, 7, 'product_size', 'text', 'Product Size', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\"}}', 6),
(45, 7, 'product_description', 'rich_text_box', 'Product Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(46, 7, 'product_details', 'rich_text_box', 'Product Details', 0, 1, 1, 1, 1, 1, '{}', 10),
(47, 7, 'units_in_stock', 'number', 'Units In Stock', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"2\"},\"min\":0,\"validation\":{\"rule\":[\"required\"]}}', 5),
(48, 7, 'images', 'multiple_images', 'Images', 0, 1, 1, 1, 1, 1, '{}', 7),
(49, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(50, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(51, 7, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 13),
(52, 7, 'product_belongsto_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"category_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(53, 7, 'product_belongsto_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"product_parent_id\",\"key\":\"id\",\"label\":\"product_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15),
(54, 7, 'product_quick_description', 'text', 'Product Quick Description', 0, 1, 1, 1, 1, 1, '{}', 14),
(67, 11, 'ci', 'text', 'Ci', 0, 1, 1, 1, 1, 1, '{}', 2),
(68, 11, 'code', 'text', 'Code', 0, 1, 1, 1, 1, 1, '{}', 3),
(69, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(70, 11, 'updated_at', 'timestamp', 'Updated At', 0, 1, 0, 0, 0, 0, '{}', 6),
(71, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(73, 11, 'discount_percentage', 'text', 'Discount Percentage', 1, 1, 1, 1, 1, 1, '{}', 6),
(74, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(76, 12, 'cost', 'number', 'Cost', 0, 1, 1, 1, 1, 1, '{\"min\":0}', 3),
(77, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(78, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(79, 12, 'shipping_method', 'text', 'Shipping Method', 0, 1, 1, 1, 1, 1, '{}', 2),
(80, 13, 'id', 'text', 'Id', 1, 1, 1, 0, 0, 0, '{}', 1),
(81, 13, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(82, 13, 'order_status_code', 'text', 'Order Status Code', 1, 1, 1, 1, 1, 1, '{}', 3),
(83, 13, 'date_order_placed', 'text', 'Date Order Placed', 0, 1, 1, 1, 1, 1, '{}', 4),
(84, 13, 'order_details', 'text', 'Order Details', 0, 1, 1, 1, 1, 1, '{}', 5),
(85, 13, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 6),
(86, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(87, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(88, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(89, 15, 'order_status_description', 'text', 'Order Status Description', 0, 1, 1, 1, 1, 1, '{}', 2),
(90, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(91, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(92, 15, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 5),
(94, 13, 'order_hasone_ref_order_status_code_relationship', 'relationship', 'ref_order_status_codes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\RefOrderStatusCode\",\"table\":\"ref_order_status_codes\",\"type\":\"belongsTo\",\"column\":\"order_status_code\",\"key\":\"id\",\"label\":\"order_status_description\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(95, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(96, 16, 'invoice_status_description', 'text', 'Invoice Status Description', 0, 1, 1, 1, 1, 1, '{}', 2),
(97, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(98, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(99, 16, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 5),
(100, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(101, 18, 'invoice_id', 'text', 'Invoice Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(102, 18, 'product_id', 'text', 'Product Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(103, 18, 'product_title', 'text', 'Product Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(104, 18, 'product_quantity', 'text', 'Product Quantity', 0, 1, 1, 1, 1, 1, '{}', 5),
(105, 18, 'product_price', 'text', 'Product Price', 0, 1, 1, 1, 1, 1, '{}', 6),
(106, 18, 'derived_product_cost', 'text', 'Derived Product Cost', 0, 1, 1, 1, 1, 1, '{}', 7),
(107, 18, 'derived_vat', 'text', 'Derived Vat', 0, 1, 1, 1, 1, 1, '{}', 8),
(108, 18, 'derived_total_cost', 'text', 'Derived Total Cost', 0, 1, 1, 1, 1, 1, '{}', 9),
(109, 18, 'other_line_item_details', 'text', 'Other Line Item Details', 0, 1, 1, 1, 1, 1, '{}', 10),
(110, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(111, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(112, 18, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 13),
(113, 13, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, '{}', 9),
(114, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(115, 19, 'invoice_id', 'text', 'Invoice Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(116, 19, 'payment_date', 'text', 'Payment Date', 0, 1, 1, 1, 1, 1, '{}', 3),
(117, 19, 'payment_amount', 'text', 'Payment Amount', 0, 1, 1, 1, 1, 1, '{}', 4),
(118, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(119, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(120, 19, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 7),
(122, 19, 'f_checked', 'checkbox', 'F Checked', 0, 1, 1, 1, 1, 1, '{}', 9),
(123, 19, 'voucher_images', 'multiple_images', 'Voucher Images', 0, 1, 1, 1, 1, 1, '{}', 8),
(124, 18, 'invoice_line_item_belongsto_invoice_relationship', 'relationship', 'invoices', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Invoice\",\"table\":\"invoices\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":null}', 14),
(127, 13, 'order_hasmany_invoice_relationship', 'relationship', 'invoices', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Invoice\",\"table\":\"invoices\",\"type\":\"hasMany\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"invoice_details\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(128, 13, 'discount_code', 'text', 'Discount Code', 0, 1, 1, 1, 1, 1, '{}', 10),
(129, 20, 'id', 'text', 'Id', 1, 1, 1, 0, 0, 0, '{}', 0),
(130, 20, 'order_id', 'text', 'Order Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(131, 20, 'invoice_status_code', 'text', 'Invoice Status Code', 0, 1, 1, 1, 1, 1, '{}', 3),
(132, 20, 'invoice_date', 'text', 'Invoice Date', 0, 1, 1, 1, 1, 1, '{}', 4),
(133, 20, 'invoice_details', 'text', 'Invoice Details', 0, 1, 1, 1, 1, 1, '{}', 5),
(134, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(135, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(136, 20, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 8),
(137, 20, 'invoice_hasmany_payment_relationship', 'relationship', 'payments', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Payment\",\"table\":\"payments\",\"type\":\"hasMany\",\"column\":\"invoice_id\",\"key\":\"id\",\"label\":\"created_at\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(138, 20, 'invoice_hasone_ref_invoice_status_code_relationship', 'relationship', 'ref_invoice_status_codes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\RefInvoiceStatusCode\",\"table\":\"ref_invoice_status_codes\",\"type\":\"belongsTo\",\"column\":\"invoice_status_code\",\"key\":\"id\",\"label\":\"invoice_status_description\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(139, 13, 'order_hasmany_order_item_relationship', 'relationship', 'order_items', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\OrderItem\",\"table\":\"order_items\",\"type\":\"hasMany\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"other_order_item_details\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(140, 20, 'invoice_hasmany_invoice_line_item_relationship', 'relationship', 'invoice_line_items', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\InvoiceLineItem\",\"table\":\"invoice_line_items\",\"type\":\"hasMany\",\"column\":\"invoice_id\",\"key\":\"id\",\"label\":\"other_line_item_details\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-08-31 00:22:39', '2019-08-31 00:22:39'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-08-31 00:22:39', '2019-08-31 00:22:39'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-08-31 00:22:39', '2019-08-31 00:22:39'),
(5, 'slider_images', 'slider-images', 'Slider Image', 'Slider Images', 'voyager-polaroid', 'App\\SliderImage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-02 19:36:14', '2019-09-02 20:03:04'),
(6, 'categories', 'categories', 'Categoria', 'Categorias', 'voyager-categories', 'App\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-02 22:49:21', '2019-09-03 00:02:45'),
(7, 'products', 'products', 'Product', 'Products', 'voyager-watch', 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-03 00:12:38', '2019-09-26 20:57:35'),
(11, 'discount_codes', 'discount-codes', 'Discount Code', 'Discount Codes', NULL, 'App\\DiscountCode', NULL, 'App\\Http\\Controllers\\DiscountCodesController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 01:04:39', '2019-09-17 00:51:11'),
(12, 'shipping_methods', 'shipping-methods', 'Shipping Method', 'Shipping Methods', NULL, 'App\\ShippingMethod', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-17 23:28:15', '2019-09-18 18:28:05'),
(13, 'orders', 'orders', 'Order', 'Orders', 'voyager-buy', 'App\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-19 00:11:46', '2019-09-25 19:08:40'),
(15, 'ref_order_status_codes', 'ref-order-status-codes', 'Ref Order Status Code', 'Ref Order Status Codes', NULL, 'App\\RefOrderStatusCode', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-19 00:15:01', '2019-09-19 00:22:59'),
(16, 'ref_invoice_status_codes', 'ref-invoice-status-codes', 'Ref Invoice Status Code', 'Ref Invoice Status Codes', NULL, 'App\\RefInvoiceStatusCode', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-20 18:10:41', '2019-09-20 18:11:54'),
(18, 'invoice_line_items', 'invoice-line-items', 'Invoice Line Item', 'Invoice Line Items', NULL, 'App\\InvoiceLineItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-20 18:33:41', '2019-09-20 18:33:58'),
(19, 'payments', 'payments', 'Payment', 'Payments', NULL, 'App\\Payment', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-24 14:34:23', '2019-09-25 14:21:39'),
(20, 'invoices', 'invoices', 'Invoice', 'Invoices', NULL, 'App\\Invoice', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-24 22:02:19', '2019-09-25 19:13:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discount_codes`
--

CREATE TABLE `discount_codes` (
  `ci` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `discount_percentage` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `discount_codes`
--

INSERT INTO `discount_codes` (`ci`, `code`, `created_at`, `updated_at`, `id`, `discount_percentage`) VALUES
('eyJpdiI6IjVwdVMxUStEbHNLWkJWbnFOemRnZlE9PSIsInZhbHVlIjoiWXFWaXZmMG5VUmtBVjl5RHFMRExjdTRMYW8yMXlobG92dk9qcEpGSVV4QT0iLCJtYWMiOiI0MmE4M2YwYmE1M2QwYjQ2OTRhZjU0NjVlNTg3NDZkYTdhM2I5MWE4OTJjNWQ1Nzg1NDc0ZGRkZTlmNWM1Mjg5In0=', '16546563266', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 52, 77),
('eyJpdiI6IjFscGZzNGJIOVhLbHhCRDd1TkZPdnc9PSIsInZhbHVlIjoiNDZLNUo1cVdKTUJJUG1NSVh5SFdTVnlDYzY0WEQraWYyQ2FGYkRHbFdIWT0iLCJtYWMiOiI1MWRiNGVmY2Y0MTRmNjg5NWJlODNhZDQwNWZlYjMxNTQ2NGQ3OGE3ZTljZjJlZjk0MGFjNGI5YjgyMzM0MmQ0In0=', '16546563267', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 53, 91),
('eyJpdiI6InNFSThwdVRndlFiWXYxSUdNRjRNa0E9PSIsInZhbHVlIjoiN1o3VEkzdG9VZDN5MTA4Wk5pRGtJc0M4OHdIeU15WjBodHppK3NRbzZ5ND0iLCJtYWMiOiJkNmFiZTFlZmQwYjE4ODM1NDMyYWQ1NGY0MTA5NzM2ZjkzZjk4NWQ5NmRjZjQ3MTQ1NzBkMjE0OWY3YzA2MjcwIn0=', '16546563268', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 54, 41),
('eyJpdiI6ImNLZEJhdzdzbFRlWE5PZFRtXC85N29BPT0iLCJ2YWx1ZSI6IlhvUTFoY0xZTUJmUFQxVDJMRTE3RjBJZnZhTHlhNXdtdmVzNUM4d0RsTk09IiwibWFjIjoiMDM0ZTE3NTFjYmUxZmVhMTg3NTc5ZjE2MmJhYzM3MDkzN2Q2ZjcwYzM3MTA4MjE4NTU3NmIwZWVjNmYxODJiMyJ9', '16546563269', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 55, 39),
('eyJpdiI6IkE0ZjdvVlRCK093aFc2XC9oOXJJS01nPT0iLCJ2YWx1ZSI6IjdHc2lDcjFDMmtreGlCZ2thNFp2SWZHQXU4bEJId0FKQlhqM01DaEF2WGs9IiwibWFjIjoiYzUxNTBhMzMwMDU2NzcyZTk4M2VkYzNiOWU2NDliZGEyMjY2MGFjZjM1MzcxMzNiNDliNGJjZjcxMTE4MTU2YiJ9', '16546563270', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 56, 51),
('eyJpdiI6IlFGS2lmQmJhdXlXaXhBTUNhcm5yRUE9PSIsInZhbHVlIjoiVUgyRVJrSFo3Zkl4RWRWMlN1SkFNczhsWGR1aFRJSkJNOFNQaFRpVGJmZz0iLCJtYWMiOiIzNTcwMWVhZDFiM2FhZDQ1NjA1NTE3ZDNiM2E5ZTVhOGMzZDI4MWUyYWJjZWEyMWQ3Yjk5NjZlN2I1NWQyYzM4In0=', '16546563271', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 57, 90),
('eyJpdiI6IlZ4elhyUXFzakt6WGxjbzdjeis2Ync9PSIsInZhbHVlIjoia3JORkk4cWF2azVPaDQyNTVZKzhnRzhqbUQ4cUlxWkN2TzZFeE5zbUxDbz0iLCJtYWMiOiJkNGVhMzk0OTM4NWFjOTczY2I0MzYwNDgyZjY1YWRhY2YzYTE3YWY0ZDUyOTc2ZTMyZjM1Y2Y3ZDZlYzIxZjNiIn0=', '16546563272', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 58, 54),
('eyJpdiI6IjE3WEM3RDZtUklNT2VrMmw4Rlp2bHc9PSIsInZhbHVlIjoieWZuc3Y2QTlWU2FyNVlpTm9uWGNIV2tsb3BUQjFhUVlPMTJIVnJiS1YrUT0iLCJtYWMiOiJjYmE3NjcxZTdhNzkxODJhMWIwYjNjNDQ0M2I5MWZlY2NjZTMxNDdmNGFhNmQ3OTNhYTkxODU5MjIwZTBmZTEzIn0=', '16546563273', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 59, 89),
('eyJpdiI6Ind4MEFTTDI0T0J0QjNrbGk4RW1cL3NRPT0iLCJ2YWx1ZSI6IjEwcitPd2NEY3BQWDJDZzJ1cHVuSXFaMUEzWEhOYjduM29aUU1BNXp6M1k9IiwibWFjIjoiMjdmMzIzMjNiZDgzODZjZjFkOWRhNjE1ZGNhMzMwZDIxNjk5ZmIyNjljMTkwNjY5YjkxNWJjNjJhOTRhZjI5NCJ9', '16546563274', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 60, 91),
('eyJpdiI6InpmcE8xaFNXVXlNMU9CYkNtNVM0S3c9PSIsInZhbHVlIjoiN0VFVlN0M2kzek5kdVNVMnorZVNMclhWcTA0bGNuNGJFY0pXUUlBNjZKbz0iLCJtYWMiOiJkZDdhMTkyMGQ0ZTgyMjhlMTY1MWNjMWM0NjkzODg4ODFlNmI2MzA1NTdlNTAzYTM5Y2I2MDUwMWUxNmMyYjBjIn0=', '16546563275', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 61, 72),
('eyJpdiI6InBiUG16VElPbk1Ta0gzNHRJMWZ6RGc9PSIsInZhbHVlIjoiM2VTMEVtRjl5KzdtR2JyR0QyS1MyM21FTDZlRUVvVmlSR1ZScnZneVhQQT0iLCJtYWMiOiJmN2ZiZDNkNDY2MDRjMDA3MzYzYzMxNGMzNjkwYjM0OWU1YzgxMDhhMWExMGEzOWJiOWY5NjJlMDhlYzk1OTBhIn0=', '16546563276', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 62, 100),
('eyJpdiI6InJ2TjA2V05BZVNNRzZteURKa0ZLTUE9PSIsInZhbHVlIjoiaGhyQlZmMldHNmQzbEZJK1JyK3hmU2t6U1hDYnR6Q1FwNm1qb1dSSmliTT0iLCJtYWMiOiI1NGIxNTY5ZGQ2NDM2YjY2MTFiMjczZmQ4YjcxZmI0MzJiZWNlMDEyMWUwNmFkYThkNzk0NGY5ZWI5ZjhiM2RhIn0=', '16546563277', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 63, 80),
('eyJpdiI6IjVkS3ljSVorcFNZRmpSOEFSMFhtU3c9PSIsInZhbHVlIjoiZFUzbTZYamI1elBpODVHaGZPV2R1blgwQVdoREgrdXRwZ1Z2NjU5T0dkcz0iLCJtYWMiOiI5NzE5MTEwMjg1NzBlZjAyYTNlZWEyZWI0OTg1OTk0YTdlMjNkNDFlODE5OWNkNDRkNzNiZWEzZjI1YWQwMmRlIn0=', '16546563278', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 64, 36),
('eyJpdiI6InJodG9VRU5jb3pVY2x4ek5CTVBZU2c9PSIsInZhbHVlIjoiQVNmcjY3THFNM3NPRXlZMURqdW9zXC82Q2hudFFtN045SDdIMllmODRyU0E9IiwibWFjIjoiNmRjMzdkOTUzNzk0ZGRhODVlMDllMzZiMzU1MWI2ZDgyZmRkZThhNGU4YTU1MmZkZjZlMjQ3Y2EzYjA2NTZiMiJ9', '16546563279', '2019-09-17 02:25:42', '2019-09-17 02:25:42', 65, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_status_code` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `invoice_details` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoices`
--

INSERT INTO `invoices` (`id`, `order_id`, `invoice_status_code`, `invoice_date`, `invoice_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, '2019-09-28 10:00:59', 'factura de prueba', '2019-09-28 15:00:59', '2019-09-30 17:13:17', NULL),
(2, 2, 1, '2019-09-30 14:16:32', 'factura de prueba', '2019-09-30 19:16:32', '2019-09-30 19:16:32', NULL),
(4, 4, 1, '2019-09-30 14:20:26', 'factura de prueba', '2019-09-30 19:20:26', '2019-09-30 19:20:26', NULL),
(5, 5, 2, '2019-09-30 14:26:12', 'factura de prueba', '2019-09-30 19:26:12', '2019-09-30 19:27:14', NULL),
(6, 6, 1, '2019-09-30 15:36:01', 'factura de prueba', '2019-09-30 20:36:01', '2019-09-30 20:36:01', NULL),
(7, 7, 1, '2019-09-30 16:09:56', 'factura de prueba', '2019-09-30 21:09:56', '2019-09-30 21:09:56', NULL),
(8, 8, 1, '2019-09-30 16:14:34', 'factura de prueba', '2019-09-30 21:14:34', '2019-09-30 21:14:34', NULL),
(9, 9, 1, '2019-09-30 16:17:20', 'factura de prueba', '2019-09-30 21:17:20', '2019-09-30 21:17:20', NULL),
(10, 10, 1, '2019-09-30 16:19:47', 'factura de prueba', '2019-09-30 21:19:47', '2019-09-30 21:19:47', NULL),
(11, 11, 1, '2019-09-30 16:28:48', 'factura de prueba', '2019-09-30 21:28:48', '2019-09-30 21:28:48', NULL),
(12, 12, 1, '2019-09-30 17:08:12', 'factura de prueba', '2019-09-30 22:08:12', '2019-09-30 22:08:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice_line_items`
--

CREATE TABLE `invoice_line_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `derived_product_cost` double DEFAULT NULL,
  `derived_vat` double DEFAULT NULL,
  `derived_total_cost` double DEFAULT NULL,
  `other_line_item_details` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoice_line_items`
--

INSERT INTO `invoice_line_items` (`id`, `invoice_id`, `product_id`, `product_title`, `product_quantity`, `product_price`, `derived_product_cost`, `derived_vat`, `derived_total_cost`, `other_line_item_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 56, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 1, 40.17, 40.17, 4.82, 44.99, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S x 1', '2019-09-28 15:00:59', '2019-09-28 15:00:59', NULL),
(2, 2, 102, 'Plancha 3 en 1 para cabello, alisado, risado y ondas', 2, 28, 56, 6.72, 62.72, 'Plancha 3 en 1 para cabello, alisado, risado y ondas x 2', '2019-09-30 19:16:32', '2019-09-30 19:16:32', NULL),
(3, 4, 57, 'Disco Duro Ext 2.5 Adata 1 Tb 3.0 Hd710Mp Military Pro', 1, 79.46, 79.46, 9.54, 89, 'Disco Duro Ext 2.5 Adata 1 Tb 3.0 Hd710Mp Military Pro x 1', '2019-09-30 19:20:26', '2019-09-30 19:20:26', NULL),
(4, 5, 56, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 1, 40.17, 40.17, 4.82, 44.99, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S x 1', '2019-09-30 19:26:12', '2019-09-30 19:26:12', NULL),
(5, 6, 56, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 1, 40.17, 40.17, 4.82, 44.99, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S x 1', '2019-09-30 20:36:01', '2019-09-30 20:36:01', NULL),
(6, 7, 57, 'Disco Duro Ext 2.5 Adata 1 Tb 3.0 Hd710Mp Military Pro', 1, 79.46, 79.46, 9.54, 89, 'Disco Duro Ext 2.5 Adata 1 Tb 3.0 Hd710Mp Military Pro x 1', '2019-09-30 21:09:56', '2019-09-30 21:09:56', NULL),
(7, 8, 56, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 1, 40.17, 40.17, 4.82, 44.99, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S x 1', '2019-09-30 21:14:34', '2019-09-30 21:14:34', NULL),
(8, 9, 60, 'Camara De Seguridad Btg Bolide Tipo Domo 2Mp 4 En 1 2.8-12Mm Gris Varifocal', 1, 69.66, 69.66, 8.36, 78.02, 'Camara De Seguridad Btg Bolide Tipo Domo 2Mp 4 En 1 2.8-12Mm Gris Varifocal x 1', '2019-09-30 21:17:20', '2019-09-30 21:17:20', NULL),
(9, 10, 56, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 1, 40.17, 40.17, 4.82, 44.99, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S x 1', '2019-09-30 21:19:47', '2019-09-30 21:19:47', NULL),
(10, 11, 56, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 6, 40.17, 241.02, 28.92, 269.94, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S x 6', '2019-09-30 21:28:48', '2019-09-30 21:28:48', NULL),
(11, 12, 107, 'Pulsera Deportiva, Reloj Deportivo Smartwatch Bluetooth M2', 2, 10.99, 21.98, 2.64, 24.62, 'Pulsera Deportiva, Reloj Deportivo Smartwatch Bluetooth M2 x 2', '2019-09-30 22:08:12', '2019-09-30 22:08:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-08-31 00:22:39', '2019-08-31 00:22:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-08-31 00:22:40', '2019-08-31 00:22:40', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-08-31 00:22:40', '2019-09-25 15:18:39', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 4, '2019-08-31 00:22:40', '2019-09-25 15:18:39', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 3, '2019-08-31 00:22:40', '2019-09-25 15:18:39', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2019-08-31 00:22:40', '2019-09-25 15:18:39', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-08-31 00:22:40', '2019-09-02 19:38:13', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-08-31 00:22:40', '2019-09-25 15:13:46', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 5, '2019-08-31 00:22:40', '2019-09-25 15:13:46', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 3, '2019-08-31 00:22:40', '2019-09-25 15:13:46', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 7, '2019-08-31 00:22:40', '2019-09-25 15:18:39', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 4, '2019-08-31 00:22:42', '2019-09-25 15:13:46', 'voyager.hooks', NULL),
(12, 1, 'Slider Images', '', '_self', 'voyager-polaroid', '#000000', 13, 3, '2019-09-02 19:36:15', '2019-09-03 00:13:43', 'voyager.slider-images.index', 'null'),
(13, 1, 'Tienda', '', '_self', 'voyager-shop', '#000000', NULL, 2, '2019-09-02 19:38:38', '2019-09-03 00:13:43', NULL, ''),
(14, 1, 'Categorias', '', '_self', 'voyager-categories', '#000000', 13, 2, '2019-09-02 22:49:21', '2019-09-03 00:13:43', 'voyager.categories.index', 'null'),
(15, 1, 'Products', '', '_self', 'voyager-watch', NULL, 13, 1, '2019-09-03 00:12:39', '2019-09-03 00:13:43', 'voyager.products.index', NULL),
(19, 1, 'Discount Codes', '', '_self', NULL, NULL, NULL, 8, '2019-09-14 01:04:39', '2019-09-25 15:18:39', 'voyager.discount-codes.index', NULL),
(20, 1, 'Shipping Methods', '', '_self', NULL, NULL, NULL, 9, '2019-09-17 23:28:15', '2019-09-25 15:18:39', 'voyager.shipping-methods.index', NULL),
(21, 1, 'Orders', '', '_self', 'voyager-buy', NULL, 27, 1, '2019-09-19 00:11:46', '2019-09-25 15:38:20', 'voyager.orders.index', NULL),
(22, 1, 'Ref Order Status Codes', '', '_self', NULL, NULL, NULL, 10, '2019-09-19 00:15:01', '2019-09-25 15:18:39', 'voyager.ref-order-status-codes.index', NULL),
(23, 1, 'Ref Invoice Status Codes', '', '_self', NULL, NULL, NULL, 11, '2019-09-20 18:10:42', '2019-09-25 15:18:39', 'voyager.ref-invoice-status-codes.index', NULL),
(24, 1, 'Invoice Line Items', '', '_self', NULL, NULL, NULL, 12, '2019-09-20 18:33:41', '2019-09-25 15:18:39', 'voyager.invoice-line-items.index', NULL),
(25, 1, 'Payments', '', '_self', NULL, NULL, 27, 3, '2019-09-24 14:34:24', '2019-09-25 15:38:33', 'voyager.payments.index', NULL),
(26, 1, 'Invoices', '', '_self', NULL, '#000000', 27, 2, '2019-09-24 22:02:20', '2019-09-25 15:39:08', 'voyager.invoices.index', 'null'),
(27, 1, 'Gestion de ordenes', '', '_self', 'voyager-ship', '#000000', 13, 4, '2019-09-25 15:15:56', '2019-09-25 15:38:33', NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_30_154128_create_categories_table', 1),
(24, '2019_08_30_154128_create_customer_information_table', 1),
(25, '2019_08_30_154128_create_invoice_line_items_table', 1),
(26, '2019_08_30_154128_create_invoices_table', 1),
(27, '2019_08_30_154128_create_order_items_table', 1),
(28, '2019_08_30_154128_create_orders_table', 1),
(29, '2019_08_30_154128_create_payments_table', 1),
(30, '2019_08_30_154128_create_products_table', 1),
(31, '2019_08_30_154128_create_ref_invoice_status_codes_table', 1),
(32, '2019_08_30_154128_create_ref_order_status_codes_table', 1),
(33, '2019_08_30_154128_create_shipments_table', 1),
(34, '2019_08_30_154130_add_foreign_keys_to_customer_information_table', 1),
(35, '2019_08_30_154130_add_foreign_keys_to_invoice_line_items_table', 2),
(36, '2019_08_30_154130_add_foreign_keys_to_invoices_table', 2),
(37, '2019_08_30_154130_add_foreign_keys_to_order_items_table', 3),
(38, '2019_08_30_154130_add_foreign_keys_to_orders_table', 3),
(39, '2019_08_30_154130_add_foreign_keys_to_payments_table', 3),
(40, '2019_08_30_154130_add_foreign_keys_to_products_table', 3),
(41, '2019_08_30_154130_add_foreign_keys_to_shipments_table', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_status_code` bigint(20) UNSIGNED NOT NULL,
  `date_order_placed` datetime DEFAULT NULL,
  `order_details` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `discount_code` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_status_code`, `date_order_placed`, `order_details`, `deleted_at`, `created_at`, `updated_at`, `amount`, `discount_code`) VALUES
(1, 1, 1, '2019-09-28 10:00:59', 'orden de prueba', NULL, '2019-09-28 15:00:59', '2019-09-28 15:00:59', 44.99, NULL),
(2, 8, 1, '2019-09-30 14:16:32', 'orden de prueba', NULL, '2019-09-30 19:16:32', '2019-09-30 19:16:32', 62.72, NULL),
(4, 1, 1, '2019-09-30 14:20:26', 'orden de prueba', NULL, '2019-09-30 19:20:26', '2019-09-30 19:20:26', 89, NULL),
(5, 1, 2, '2019-09-30 14:26:12', 'orden de prueba', NULL, '2019-09-30 19:26:00', '2019-09-30 19:29:20', 44.99, NULL),
(6, 1, 1, '2019-09-30 15:36:01', 'orden de prueba', NULL, '2019-09-30 20:36:01', '2019-09-30 20:36:01', 44.99, NULL),
(7, 9, 1, '2019-09-30 16:09:56', 'orden de prueba', NULL, '2019-09-30 21:09:56', '2019-09-30 21:09:56', 89, NULL),
(8, 9, 1, '2019-09-30 16:14:34', 'orden de prueba', NULL, '2019-09-30 21:14:34', '2019-09-30 21:14:34', 44.99, NULL),
(9, 9, 1, '2019-09-30 16:17:20', 'orden de prueba', NULL, '2019-09-30 21:17:20', '2019-09-30 21:17:20', 78.02, NULL),
(10, 9, 1, '2019-09-30 16:19:47', 'orden de prueba', NULL, '2019-09-30 21:19:47', '2019-09-30 21:19:47', 44.99, NULL),
(11, 9, 1, '2019-09-30 16:28:48', 'orden de prueba', NULL, '2019-09-30 21:28:48', '2019-09-30 21:28:48', 269.94, NULL),
(12, 9, 1, '2019-09-30 17:08:12', 'orden de prueba', NULL, '2019-09-30 22:08:12', '2019-09-30 22:08:12', 24.62, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `other_order_item_details` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `product_quantity`, `other_order_item_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 56, 1, 'Detalles prueba', '2019-09-28 15:00:59', '2019-09-28 15:00:59', NULL),
(2, 2, 102, 2, 'Detalles prueba', '2019-09-30 19:16:32', '2019-09-30 19:16:32', NULL),
(3, 4, 57, 1, 'Detalles prueba', '2019-09-30 19:20:26', '2019-09-30 19:20:26', NULL),
(4, 5, 56, 1, 'Detalles prueba', '2019-09-30 19:26:12', '2019-09-30 19:26:12', NULL),
(5, 6, 56, 1, 'Detalles prueba', '2019-09-30 20:36:01', '2019-09-30 20:36:01', NULL),
(6, 7, 57, 1, 'Detalles prueba', '2019-09-30 21:09:56', '2019-09-30 21:09:56', NULL),
(7, 8, 56, 1, 'Detalles prueba', '2019-09-30 21:14:34', '2019-09-30 21:14:34', NULL),
(8, 9, 60, 1, 'Detalles prueba', '2019-09-30 21:17:20', '2019-09-30 21:17:20', NULL),
(9, 10, 56, 1, 'Detalles prueba', '2019-09-30 21:19:47', '2019-09-30 21:19:47', NULL),
(10, 11, 56, 6, 'Detalles prueba', '2019-09-30 21:28:48', '2019-09-30 21:28:48', NULL),
(11, 12, 107, 2, 'Detalles prueba', '2019-09-30 22:08:12', '2019-09-30 22:08:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `voucher_images` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_checked` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`id`, `invoice_id`, `payment_date`, `payment_amount`, `created_at`, `updated_at`, `deleted_at`, `voucher_images`, `f_checked`) VALUES
(1, 1, '2019-09-30 12:13:17', NULL, '2019-09-30 17:13:17', '2019-09-30 17:13:17', NULL, '[\"\\\\vouchers\\\\4zqCors1JoPuY4FtcLjfqX2doGcBsmYqzYuPSgha.png\"]', NULL),
(2, 5, '2019-09-30 14:27:13', NULL, '2019-09-30 19:27:00', '2019-09-30 19:28:17', NULL, '[\"\\\\vouchers\\\\ZXyFxkfm7PecWtF0oxFUDxAiouTIfBZkypcfPPZV.jpeg\"]', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(2, 'browse_bread', NULL, '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(3, 'browse_database', NULL, '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(4, 'browse_media', NULL, '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(5, 'browse_compass', NULL, '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(6, 'browse_menus', 'menus', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(7, 'read_menus', 'menus', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(8, 'edit_menus', 'menus', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(9, 'add_menus', 'menus', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(10, 'delete_menus', 'menus', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(11, 'browse_roles', 'roles', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(12, 'read_roles', 'roles', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(13, 'edit_roles', 'roles', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(14, 'add_roles', 'roles', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(15, 'delete_roles', 'roles', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(16, 'browse_users', 'users', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(17, 'read_users', 'users', '2019-08-31 00:22:40', '2019-08-31 00:22:40'),
(18, 'edit_users', 'users', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(19, 'add_users', 'users', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(20, 'delete_users', 'users', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(21, 'browse_settings', 'settings', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(22, 'read_settings', 'settings', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(23, 'edit_settings', 'settings', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(24, 'add_settings', 'settings', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(25, 'delete_settings', 'settings', '2019-08-31 00:22:41', '2019-08-31 00:22:41'),
(26, 'browse_hooks', NULL, '2019-08-31 00:22:42', '2019-08-31 00:22:42'),
(27, 'browse_slider_images', 'slider_images', '2019-09-02 19:36:15', '2019-09-02 19:36:15'),
(28, 'read_slider_images', 'slider_images', '2019-09-02 19:36:15', '2019-09-02 19:36:15'),
(29, 'edit_slider_images', 'slider_images', '2019-09-02 19:36:15', '2019-09-02 19:36:15'),
(30, 'add_slider_images', 'slider_images', '2019-09-02 19:36:15', '2019-09-02 19:36:15'),
(31, 'delete_slider_images', 'slider_images', '2019-09-02 19:36:15', '2019-09-02 19:36:15'),
(32, 'browse_categories', 'categories', '2019-09-02 22:49:21', '2019-09-02 22:49:21'),
(33, 'read_categories', 'categories', '2019-09-02 22:49:21', '2019-09-02 22:49:21'),
(34, 'edit_categories', 'categories', '2019-09-02 22:49:21', '2019-09-02 22:49:21'),
(35, 'add_categories', 'categories', '2019-09-02 22:49:21', '2019-09-02 22:49:21'),
(36, 'delete_categories', 'categories', '2019-09-02 22:49:21', '2019-09-02 22:49:21'),
(37, 'browse_products', 'products', '2019-09-03 00:12:38', '2019-09-03 00:12:38'),
(38, 'read_products', 'products', '2019-09-03 00:12:38', '2019-09-03 00:12:38'),
(39, 'edit_products', 'products', '2019-09-03 00:12:38', '2019-09-03 00:12:38'),
(40, 'add_products', 'products', '2019-09-03 00:12:38', '2019-09-03 00:12:38'),
(41, 'delete_products', 'products', '2019-09-03 00:12:38', '2019-09-03 00:12:38'),
(57, 'browse_discount_codes', 'discount_codes', '2019-09-14 01:04:39', '2019-09-14 01:04:39'),
(58, 'read_discount_codes', 'discount_codes', '2019-09-14 01:04:39', '2019-09-14 01:04:39'),
(59, 'edit_discount_codes', 'discount_codes', '2019-09-14 01:04:39', '2019-09-14 01:04:39'),
(60, 'add_discount_codes', 'discount_codes', '2019-09-14 01:04:39', '2019-09-14 01:04:39'),
(61, 'delete_discount_codes', 'discount_codes', '2019-09-14 01:04:39', '2019-09-14 01:04:39'),
(62, 'browse_shipping_methods', 'shipping_methods', '2019-09-17 23:28:15', '2019-09-17 23:28:15'),
(63, 'read_shipping_methods', 'shipping_methods', '2019-09-17 23:28:15', '2019-09-17 23:28:15'),
(64, 'edit_shipping_methods', 'shipping_methods', '2019-09-17 23:28:15', '2019-09-17 23:28:15'),
(65, 'add_shipping_methods', 'shipping_methods', '2019-09-17 23:28:15', '2019-09-17 23:28:15'),
(66, 'delete_shipping_methods', 'shipping_methods', '2019-09-17 23:28:15', '2019-09-17 23:28:15'),
(67, 'browse_orders', 'orders', '2019-09-19 00:11:46', '2019-09-19 00:11:46'),
(68, 'read_orders', 'orders', '2019-09-19 00:11:46', '2019-09-19 00:11:46'),
(69, 'edit_orders', 'orders', '2019-09-19 00:11:46', '2019-09-19 00:11:46'),
(70, 'add_orders', 'orders', '2019-09-19 00:11:46', '2019-09-19 00:11:46'),
(71, 'delete_orders', 'orders', '2019-09-19 00:11:46', '2019-09-19 00:11:46'),
(72, 'browse_ref_order_status_codes', 'ref_order_status_codes', '2019-09-19 00:15:01', '2019-09-19 00:15:01'),
(73, 'read_ref_order_status_codes', 'ref_order_status_codes', '2019-09-19 00:15:01', '2019-09-19 00:15:01'),
(74, 'edit_ref_order_status_codes', 'ref_order_status_codes', '2019-09-19 00:15:01', '2019-09-19 00:15:01'),
(75, 'add_ref_order_status_codes', 'ref_order_status_codes', '2019-09-19 00:15:01', '2019-09-19 00:15:01'),
(76, 'delete_ref_order_status_codes', 'ref_order_status_codes', '2019-09-19 00:15:01', '2019-09-19 00:15:01'),
(77, 'browse_ref_invoice_status_codes', 'ref_invoice_status_codes', '2019-09-20 18:10:41', '2019-09-20 18:10:41'),
(78, 'read_ref_invoice_status_codes', 'ref_invoice_status_codes', '2019-09-20 18:10:41', '2019-09-20 18:10:41'),
(79, 'edit_ref_invoice_status_codes', 'ref_invoice_status_codes', '2019-09-20 18:10:41', '2019-09-20 18:10:41'),
(80, 'add_ref_invoice_status_codes', 'ref_invoice_status_codes', '2019-09-20 18:10:41', '2019-09-20 18:10:41'),
(81, 'delete_ref_invoice_status_codes', 'ref_invoice_status_codes', '2019-09-20 18:10:41', '2019-09-20 18:10:41'),
(82, 'browse_invoice_line_items', 'invoice_line_items', '2019-09-20 18:33:41', '2019-09-20 18:33:41'),
(83, 'read_invoice_line_items', 'invoice_line_items', '2019-09-20 18:33:41', '2019-09-20 18:33:41'),
(84, 'edit_invoice_line_items', 'invoice_line_items', '2019-09-20 18:33:41', '2019-09-20 18:33:41'),
(85, 'add_invoice_line_items', 'invoice_line_items', '2019-09-20 18:33:41', '2019-09-20 18:33:41'),
(86, 'delete_invoice_line_items', 'invoice_line_items', '2019-09-20 18:33:41', '2019-09-20 18:33:41'),
(87, 'browse_payments', 'payments', '2019-09-24 14:34:23', '2019-09-24 14:34:23'),
(88, 'read_payments', 'payments', '2019-09-24 14:34:23', '2019-09-24 14:34:23'),
(89, 'edit_payments', 'payments', '2019-09-24 14:34:23', '2019-09-24 14:34:23'),
(90, 'add_payments', 'payments', '2019-09-24 14:34:23', '2019-09-24 14:34:23'),
(91, 'delete_payments', 'payments', '2019-09-24 14:34:23', '2019-09-24 14:34:23'),
(92, 'browse_invoices', 'invoices', '2019-09-24 22:02:20', '2019-09-24 22:02:20'),
(93, 'read_invoices', 'invoices', '2019-09-24 22:02:20', '2019-09-24 22:02:20'),
(94, 'edit_invoices', 'invoices', '2019-09-24 22:02:20', '2019-09-24 22:02:20'),
(95, 'add_invoices', 'invoices', '2019-09-24 22:02:20', '2019-09-24 22:02:20'),
(96, 'delete_invoices', 'invoices', '2019-09-24 22:02:20', '2019-09-24 22:02:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(4, 3),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(23, 3),
(24, 1),
(24, 3),
(25, 1),
(25, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(39, 3),
(40, 1),
(40, 3),
(41, 1),
(41, 3),
(57, 1),
(57, 3),
(58, 1),
(58, 3),
(59, 1),
(59, 3),
(60, 1),
(60, 3),
(61, 1),
(61, 3),
(62, 1),
(62, 3),
(63, 1),
(63, 3),
(64, 1),
(64, 3),
(65, 1),
(65, 3),
(66, 1),
(66, 3),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 1),
(70, 3),
(71, 1),
(71, 3),
(72, 1),
(72, 3),
(73, 1),
(73, 3),
(74, 1),
(74, 3),
(75, 1),
(75, 3),
(76, 1),
(76, 3),
(77, 1),
(77, 3),
(78, 1),
(78, 3),
(79, 1),
(79, 3),
(80, 1),
(80, 3),
(81, 1),
(81, 3),
(82, 1),
(82, 3),
(83, 1),
(83, 3),
(84, 1),
(84, 3),
(85, 1),
(85, 3),
(86, 1),
(86, 3),
(87, 1),
(87, 3),
(88, 1),
(88, 3),
(89, 1),
(89, 3),
(90, 1),
(90, 3),
(91, 1),
(91, 3),
(92, 1),
(92, 3),
(93, 1),
(93, 3),
(94, 1),
(94, 3),
(95, 1),
(95, 3),
(96, 1),
(96, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `product_parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_size` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_details` text COLLATE utf8mb4_unicode_ci,
  `units_in_stock` int(11) DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_quick_description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `product_price`, `product_parent_id`, `product_size`, `product_description`, `product_details`, `units_in_stock`, `images`, `created_at`, `updated_at`, `deleted_at`, `product_quick_description`) VALUES
(56, 10, 'Disco Estado Solido Adata 120Gb Su650 Ultimate / S', 40.17, NULL, '100 x 150', NULL, NULL, 25, '[\"products\\\\September2019\\\\eFz5O1EMmBgFAXdSz3H2.jpg\"]', '2019-09-26 16:52:43', '2019-09-26 16:52:43', NULL, NULL),
(57, 10, 'Disco Duro Ext 2.5 Adata 1 Tb 3.0 Hd710Mp Military Pro', 79.46, NULL, '15 x 45', NULL, NULL, 52, '[\"products\\\\September2019\\\\zYAOXtI63Qgi9EFTjl98.jpg\"]', '2019-09-26 16:53:50', '2019-09-26 16:53:50', NULL, NULL),
(58, 15, 'Impresora Multifuncion Epson Tinta Continua L850 Imprime En Foto/Cd 37 Ppm', 624.11, NULL, '15 x 45', NULL, NULL, 25, '[\"products\\\\September2019\\\\K40UYHHgVNZQIsowGWaf.jpg\",\"products\\\\September2019\\\\B49dMv8DdeZHOjXAkv4o.jpg\",\"products\\\\September2019\\\\KlSNTzRz2rkJsjT6sc7O.jpg\"]', '2019-09-26 16:54:00', '2019-09-26 17:41:41', '2019-09-26 17:41:41', NULL),
(59, 14, 'Audifono Klip Xtreme In Ear Rosado', 3.57, NULL, '15 x 45', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Di&aacute;metro de auricular 10mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conector 3.5mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Potencia de salida m&aacute;xima: 50mW.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Longitud de cable 125cm.</span></p>', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Di&aacute;metro de auricular 10mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conector 3.5mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Potencia de salida m&aacute;xima: 50mW.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Longitud de cable 125cm.</span></p>', 125, '[\"products\\\\September2019\\\\eIs3ywzvQ5D1tKvghTl2.jpg\"]', '2019-09-26 17:01:00', '2019-09-26 17:15:51', NULL, NULL),
(60, 12, 'Camara De Seguridad Btg Bolide Tipo Domo 2Mp 4 En 1 2.8-12Mm Gris Varifocal', 69.66, NULL, NULL, NULL, NULL, NULL, '[\"products\\\\September2019\\\\fIQoqhOXQ3gSyf95C1Nn.jpg\"]', '2019-09-26 17:04:00', '2019-09-26 17:15:09', NULL, NULL),
(61, 11, 'Monitor/Flat Panel Lg 27\" Led *Gamer* Led Hdmi-Vga-Display Port Fhd', 418.75, NULL, NULL, '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Resoluci&oacute;n 1920x1080.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Aspect ratio 16:9.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conexiones: HDMI, VGA y Display Port.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Refresh rate 75Hz.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Tiempo de respuesta: 1ms.</span></p>', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Resoluci&oacute;n 1920x1080.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Aspect ratio 16:9.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conexiones: HDMI, VGA y Display Port.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Refresh rate 75Hz.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Tiempo de respuesta: 1ms.</span></p>', NULL, '[\"products\\\\September2019\\\\0DvMSRe7BuUZ8PA0VRFB.jpg\"]', '2019-09-26 17:04:00', '2019-09-26 17:14:12', NULL, NULL),
(62, 10, 'Router Tplink 450 Mbps 3 Antenas Fijas/4PtoS Ethernet/Wps/Wds', 25.89, NULL, '15 x 45', NULL, NULL, 25, '[\"products\\\\September2019\\\\pIx9GrFuzZGcPl6OA6i2.jpg\"]', '2019-09-26 17:13:41', '2019-09-26 17:13:41', NULL, NULL),
(63, 16, 'Tv Smart Player Roku Streaming Stick', 58.03, NULL, '15 x 45', NULL, NULL, 32, '[\"products\\\\September2019\\\\3caRZuAxD74Z2K15kp9O.jpg\"]', '2019-09-26 17:19:01', '2019-09-26 17:19:01', NULL, NULL),
(64, 10, 'Access Point Ubiquiti Wireless Centralizado Poe Incl.', 231.25, NULL, NULL, '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Frecuencia 2.4 GHz, 5GHz.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- 450 Mbps - 2.4 GHz.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- 300 Mbps - 5.0 GHz.</span></p>', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Frecuencia 2.4 GHz, 5GHz.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- 450 Mbps - 2.4 GHz.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- 300 Mbps - 5.0 GHz.</span></p>', NULL, '[\"products\\\\September2019\\\\9ssPRPmge8vT1PkUeajb.jpg\"]', '2019-09-26 17:20:00', '2019-09-26 17:21:10', NULL, NULL),
(65, 11, 'Monitor/Flat Panel Aoc 19.5\" Led 1600X900 Hdmi-Vga (3A)', 97.32, NULL, '15 x 45', '<p><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">Monitor de 19.5\" LED - libre de mercurio, bajo consumo de energ&iacute;a y alt&iacute;simo contraste.S&oacute;lido desempe&ntilde;o para tareas diarias con conectividad HDMI.</strong><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Resoluci&oacute;n 1600 x 900 @ 60 HZ.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conexiones: HDMIx1 y RGB (VGA)x1.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Dimensiones: 465mm x 346.4mm x 156mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Incluye: Cable VGA, Cable de corriente y Manual.</span></p>', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Resoluci&oacute;n 1600 x 900 @ 60 HZ.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conexiones: HDMIx1 y RGB (VGA)x1.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Dimensiones: 465mm x 346.4mm x 156mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Incluye: Cable VGA, Cable de corriente y Manual.</span></p>', 32, '[\"products\\\\September2019\\\\PtgKhzz2nmxwd0ZJJTBh.jpg\"]', '2019-09-26 17:22:00', '2019-09-26 17:23:06', NULL, 'Monitor de 19.5\" LED - libre de mercurio, bajo consumo de energía y altísimo contraste.Sólido desempeño para tareas diarias con conectividad HDMI.'),
(66, NULL, 'Audifonos + Microfono Fifo In Ear 3.5 Mm', 4.46, NULL, '15 x 45', NULL, NULL, 25, '[\"products\\\\September2019\\\\dtRAswGdh86cZi7ZEzBW.jpg\"]', '2019-09-26 17:23:33', '2019-09-26 17:23:33', NULL, NULL),
(67, 8, 'Power Bank Klip Xtreme 5.000MAh Plateado', 12.5, NULL, '15 x 45', '<p><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">CARGADOR DE BATER&Iacute;A PORT&Aacute;TIL</strong><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Entrada: Micro USB para carga.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Tensi&oacute;n de entrada. CC 5V - 2000mA.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Salida: CC m&aacute;x. de 5V, 2100mA.</span></p>', '<p><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">CARGADOR DE BATER&Iacute;A PORT&Aacute;TIL</strong><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Entrada: Micro USB para carga.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Tensi&oacute;n de entrada. CC 5V - 2000mA.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Salida: CC m&aacute;x. de 5V, 2100mA.</span></p>', 25, '[\"products\\\\September2019\\\\OsVOiiyjGhwHpbrum8Uu.jpg\"]', '2019-09-26 17:24:37', '2019-09-26 17:24:37', NULL, 'CARGADOR DE BATERÍA PORTÁTIL'),
(68, 10, 'Disco Duro Int Seagate 2Tb Skyhawk Vigilancia', 102.67, NULL, '15 x 45', NULL, NULL, 25, '[\"products\\\\September2019\\\\Ks9BegAZJ1lBJIFrJqkH.jpg\"]', '2019-09-26 17:25:46', '2019-09-26 17:25:46', NULL, NULL),
(69, 17, 'Televisor-Tv Xtratech 32\" Led Hd Smart 1080 Wifi Isdb-T Eco-Future (1A)', 231.25, NULL, '734x467x52.8', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Sintonizador digital ISDBT.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Dimensiones del panel (mm) - 734x467x52.8.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conexiones HDMIx2 - USBx2 - VGA.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Salida de audio 3.5mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Puerto LAN</span></p>', '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Sintonizador digital ISDBT.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Dimensiones del panel (mm) - 734x467x52.8.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Conexiones HDMIx2 - USBx2 - VGA.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Salida de audio 3.5mm.</span><br style=\"box-sizing: border-box; margin: 0px; padding: 0px; -webkit-font-smoothing: antialiased; color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\" /><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">- Puerto LAN</span></p>', 24, '[\"products\\\\September2019\\\\nYN6hgjMuTpCv72saiAU.jpg\",\"products\\\\September2019\\\\IvhSCo5CRrJIvATkjIgH.jpg\",\"products\\\\September2019\\\\xjy05zlhOudxnMEwTkSq.jpg\"]', '2019-09-26 17:28:00', '2019-09-26 17:39:30', NULL, NULL),
(70, 15, 'Toner Samsung Negro Ml-3312Nd / Ml-3712Nd / Scx-4835Fd (5K)', 147.32, NULL, NULL, '<p><span style=\"color: #000000; font-family: Montserrat, sans-serif; font-size: 15px;\">5.000 PAGINAS</span></p>', NULL, NULL, '[\"products\\\\September2019\\\\oCYtPwdj4djWYO2VVU9L.jpg\"]', '2019-09-26 17:31:00', '2019-09-26 17:32:36', NULL, 'Toner Samsung Negro Ml-3312Nd / Ml-3712Nd / Scx-4835Fd (5K) / 5.000 PAGINAS'),
(71, 10, 'Mouse Logitech Inalambrico M187 Color Bright Teal', 16.07, NULL, '15 x 45', NULL, NULL, 14, '[\"products\\\\September2019\\\\nYD1vtZSY9TG1uPvhk4G.jpg\"]', '2019-09-26 17:44:25', '2019-09-26 17:44:25', NULL, NULL),
(72, 18, 'Laptop/Notebook Xtratech Roja Dual Core N3350 14\"/4Gb/120Ssd/Wifi/W10', 266.96, NULL, '100 x 150', NULL, NULL, 12, '[\"products\\\\September2019\\\\Wlc5vgDK2YiIMxHdV65n.jpg\"]', '2019-09-26 17:46:39', '2019-09-26 17:46:39', NULL, NULL),
(73, 21, 'Adorno elefante Dorado 21 cm', 25, NULL, '21', NULL, NULL, 2, '[\"products\\\\September2019\\\\ppBzJ5vfOVWaA4SOpkOE.PNG\",\"products\\\\September2019\\\\EBQIkpNmclGCDkEyjM5c.PNG\"]', '2019-09-26 19:23:00', '2019-09-26 19:55:42', NULL, NULL),
(74, 21, 'Adorno elefante Dorado 26 cm', 29, NULL, '26', NULL, NULL, 2, '[\"products\\\\September2019\\\\HwVq7YBGq5rf1AFbrLEj.PNG\",\"products\\\\September2019\\\\VHHTDLhlElgv8tD1nYHW.PNG\"]', '2019-09-26 19:23:00', '2019-09-26 19:55:55', NULL, NULL),
(75, 21, 'Adorno Decorativo Perrito Globo Dorado', 14.98, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Hermoso y divertido adorno en forma de Perrito, el globo </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">es de color Dorado, ideal para la decoraci&oacute;n </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">de tu hogar, gracias a su dise&ntilde;o y color esta figura decorativa le </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">dar&aacute; m&aacute;s personalidad al ambiente festivo</span></p>', NULL, 10, '[\"products\\\\September2019\\\\InnhgArslrXidzhr2Kuk.PNG\"]', '2019-09-26 19:32:00', '2019-09-26 19:55:32', NULL, NULL),
(76, 22, 'Cojines infantiles', 12.98, NULL, NULL, NULL, NULL, 20, '[\"products\\\\September2019\\\\lBTkho6CQYwy7iZIB3bV.PNG\",\"products\\\\September2019\\\\prWjhzxLxWLorTwz4FUr.PNG\",\"products\\\\September2019\\\\PgdHLjuF3tIZz9y77lUP.PNG\"]', '2019-09-26 19:43:00', '2019-09-26 19:55:20', NULL, NULL),
(77, 22, 'Cojines', 8.89, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Los cojines son una gran herramienta decorativa seg&uacute;n sus </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">dise&ntilde;os, formas y textura.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Gracias a su color y dise&ntilde;o le dar&aacute; un toque de originalidad a</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">tus ambientes.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Ideal para la decoraci&oacute;n de tu sala o dormitorio.</span></p>', NULL, NULL, '[\"products\\\\September2019\\\\k36seeRbPctCv6nAnYR2.PNG\",\"products\\\\September2019\\\\l1Yire7LYv3riLcMquuD.PNG\",\"products\\\\September2019\\\\K1CmZyqKFFl5vI5eHWrD.PNG\",\"products\\\\September2019\\\\MECf35Mfv3fpf27dR9ly.PNG\",\"products\\\\September2019\\\\WkCD6EZBodKR5rwZoMdq.PNG\"]', '2019-09-26 19:53:00', '2019-09-26 19:55:07', NULL, NULL),
(78, 23, 'Corona Santa Claus Reino de la Felicidad', 22.59, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Te presentamos esta tradicional y c&aacute;lida colecci&oacute;n navide&ntilde;a </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Reino de la Felicidad el cual mezcla colores cl&aacute;sicos como el </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">rojo, el verde oscuro, el blanco y el dorado.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Esta hermosa Corona Santa Claus trae un mu&ntilde;eco Santa Claus </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">con leyende de \"Merry Christmas\", es ideal para colocarlo en tu </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">puerta de entrada.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Celebra la Navidad con la colecci&oacute;n Reino de la Felicidad y dale </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">un toque especial a tu hogar.</span></p>', NULL, 10, '[\"products\\\\September2019\\\\nlC5RVVMrlYd6jkdNan5.PNG\"]', '2019-09-26 19:59:38', '2019-09-26 19:59:38', NULL, NULL),
(79, 23, 'Muñeco Snowman Sentado Reino de la Felicidad', 9.5, NULL, NULL, NULL, NULL, NULL, '[\"products\\\\September2019\\\\okVEpE2Jd0iCobENoFYl.PNG\"]', '2019-09-26 20:05:00', '2019-09-26 20:12:32', NULL, NULL),
(80, 23, 'Corona Santa Claus Feliz Navidad', 17.97, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Te presentamos esta tradicional y c&aacute;lida colecci&oacute;n navide&ntilde;a </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Reino de la Felicidad el cual mezcla colores cl&aacute;sicos como el </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">rojo, el verde oscuro, el blanco y el dorado.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Esta hermosa Corona Santa Claus trae un mu&ntilde;eco Santa Claus </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">con leyende de \"Feliz Navidad\", es ideal para colocarlo en tu </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">puerta de entrada.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Celebra la Navidad con la colecci&oacute;n Reino de la Felicidad y dale </span><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">un toque especial a tu hogar.</span></p>', NULL, NULL, '[\"products\\\\September2019\\\\FqmQG5biXAYtGsgyFciC.PNG\"]', '2019-09-26 20:06:49', '2019-09-26 20:06:49', NULL, NULL),
(81, 23, 'Nacimiento de Resina (11 piezas)', 98.99, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Para celebrar la navidad no puede faltar los nacimientos en la decoraci&oacute;n de &eacute;sta &eacute;poca.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Te presentamos este nacimiento de 11 piezas.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Incluye las siguientes figuras: Jos&eacute;, Mar&iacute;a, Ni&ntilde;o Jes&uacute;s, 3 Reyes Magos, &Aacute;ngel, pastor, asno, vaca y oveja.</span></p>', NULL, 10, '[\"products\\\\September2019\\\\C1seE2aWhyoPsM2MuInt.PNG\",\"products\\\\September2019\\\\TctHkujgw3LqWJlvgKwv.PNG\"]', '2019-09-26 20:08:00', '2019-09-26 20:08:51', NULL, NULL),
(82, 23, 'Nacimiento de Resina (9 piezas)', 69.99, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Para celebrar la navidad no puede faltar los nacimientos en la decoraci&oacute;n de &eacute;sta &eacute;poca.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Te presentamos este nacimiento de 9 piezas.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Incluye las siguientes figuras: Jos&eacute;, Mar&iacute;a, ni&ntilde;o Jes&uacute;s, 3 Reyes Magos, &Aacute;ngel, asno, vaca.</span></p>', NULL, 10, '[\"products\\\\September2019\\\\mCDukaNWpqF1jF3iCdrX.PNG\"]', '2019-09-26 20:09:00', '2019-09-26 20:10:01', NULL, NULL),
(83, 23, NULL, 13.98, NULL, NULL, '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Te presentamos este hermoso coj&iacute;n sublimado con motivo navide&ntilde;o, relleno de plum&oacute;n suave.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Con dise&ntilde;o de cara de mu&ntilde;eco de nieve en color rojo,con leyenda d&iacute;a de nieve.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Que podr&aacute;s colocar en todas partes como sillas, sof&aacute;, sillones, en sala o habitaci&oacute;n.</span></p>', NULL, NULL, '[\"products\\\\September2019\\\\Ybtz1rmadsX9GtSHQKnQ.PNG\",\"products\\\\September2019\\\\A1Np8GvW1DvlpWru1Dff.PNG\",\"products\\\\September2019\\\\SNc9UavQm1BAbf9Y7LE9.PNG\"]', '2019-09-26 20:26:40', '2019-09-26 20:26:40', NULL, NULL),
(84, 23, 'Corona de Adviento', 18.49, NULL, '27', '<p><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Hermosa corona navide&ntilde;a para el adviento realizada con pi&ntilde;as y bolas.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Para usarla como centro de mesa.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Decorada en colores dorado con rojo y verde.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Con 4 porta velas.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Adviento significa la venida de jesuscristo.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Se da el nombre de adviento a las 4 semanas que preceden a la navidad.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Se enciende una vela por cada domingo.</span><br style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; font-family: Lato, sans-serif; color: #808080; letter-spacing: 0.5px;\" /><span style=\"color: #808080; font-family: Lato, sans-serif; letter-spacing: 0.5px;\">Se reune la familia y se realiza una oraci&oacute;n.</span></p>', NULL, 8, '[\"products\\\\September2019\\\\W70dKFe9ruWxJlw0nYrr.PNG\"]', '2019-09-26 20:28:34', '2019-09-26 20:28:34', NULL, NULL),
(85, 24, 'Porta Zapatos Doble', 29.99, NULL, NULL, '<p class=\"MsoNormal\">Soporta 36 pares</p>\r\n<p class=\"MsoNormal\">Con lona cobertora y cierre.</p>\r\n<p class=\"MsoNormal\">Con estructura s&oacute;lida y resistente.</p>\r\n<p class=\"MsoNormal\">Tama&ntilde;o: ancho 110 x fondo 27 x alto 105cm</p>', NULL, NULL, '[\"products\\\\September2019\\\\HVaSpJOfmiPNXryEgYPC.jpg\",\"products\\\\September2019\\\\Mhe2EYaPzuKymKaqjIr9.jpg\",\"products\\\\September2019\\\\2bjrcPhgICw0vqpmHf7Q.png\"]', '2019-09-26 20:35:28', '2019-09-26 20:35:28', NULL, NULL),
(86, 24, 'Porta Zapatos de 8 Pares', 6.99, NULL, NULL, '<p class=\"MsoNormal\">El peque&ntilde;o ordenador de zapatos</p>\r\n<p class=\"MsoNormal\">De cuatro pisos, muy estable</p>', NULL, 15, '[\"products\\\\September2019\\\\yFSBujZgHO3dTVnfu8xp.jpg\"]', '2019-09-26 20:36:19', '2019-09-26 20:36:19', NULL, NULL),
(87, 24, 'Porta Zapatos de 10 Pisos', 29.99, NULL, NULL, '<p class=\"MsoNormal\">Resistente y estable.</p>\r\n<p class=\"MsoNormal\">Soporta 3 pares por piso.</p>\r\n<p class=\"MsoNormal\">Alto 156, ancho 56, fondo 25cm.</p>\r\n<p class=\"MsoNormal\">Le obsequiamos el cobertor</p>', NULL, 3, '[\"products\\\\September2019\\\\WCcIKKu6wMZdFIUjgOTh.jpg\"]', '2019-09-26 20:37:44', '2019-09-26 20:37:44', NULL, NULL),
(88, 25, 'Máquina cocedora de huevos', 11, NULL, NULL, '<p><span style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\">El secreto para cocer el huevo y obtener el resultado deseado esta en su cocci&oacute;n, con la Cocedora de Huevos el secreto est&aacute; en la cantidad de agua que a&ntilde;adas.</span><br style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\" /><span style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\">&iquest;Como te gustan, blandos, poco hechos o duros? Olv&iacute;date del tiempo de cocci&oacute;n y obt&eacute;n los huevos cocidos en tu punto exacto con la Cocedora de Huevos de Tristar.</span><br style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\" /><span style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\">Cuece hasta 7 huevos al mismo tiempo. Su jarra de agua medidora te indica la cantidad de agua que debes usar seg&uacute;n como los quieras obtener.</span><br style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\" /><span style=\"box-sizing: border-box; color: rgba(17, 17, 17, 0.75); font-family: \'open sans\', sans-serif;\">Cuando los huevos est&eacute;n listos sonar&aacute; el timbre de la cocedora, extrae la bandeja para enjuagarlos con total seguridad y &iexcl;listos para servir</span></p>', NULL, NULL, '[\"products\\\\September2019\\\\5ElovabJkpVnfp8GoIDM.jpg\",\"products\\\\September2019\\\\kgi8Ooc7OQO1KoLs8oBw.jpg\"]', '2019-09-26 20:42:00', '2019-09-26 20:49:28', NULL, NULL),
(89, 25, 'Licuadora Prima 3 Velocidades 1.5lts Vidrio pn-y63g 400', 24, NULL, NULL, '<p class=\"MsoNormal\">- Jarra de vidrio con capacidad de 5 Tz.</p>\r\n<p class=\"MsoNormal\">- Sistema de acople reforzado.</p>\r\n<p class=\"MsoNormal\">- Funcion de pulso-</p>\r\n<p class=\"MsoNormal\">- Cuchilla de acero de inoxidable</p>', NULL, 4, '[\"products\\\\September2019\\\\fiSw6uGHBDGTSCCOfGBr.jpg\",\"products\\\\September2019\\\\9BwGV40HI0ONQOhm4A9G.jpg\"]', '2019-09-26 20:44:00', '2019-09-26 21:02:12', NULL, NULL),
(90, 24, 'Plancha Oster Eléctrica, Suela Antiadherente 1200 W colores', 17.99, NULL, NULL, '<p class=\"MsoNormal\">-Bot&oacute;n de rociado fino que facilita la tarea del planchado</p>\r\n<p class=\"MsoNormal\">-Sistema anti-Calc que protege y prolonga la vida de la plancha, y mejora el desempe&ntilde;o de la plancha</p>\r\n<p class=\"MsoNormal\">-Sistema anti-Calc que protege y prolonga la vida de la plancha, y mejora el desempe&ntilde;o de la plancha</p>\r\n<p class=\"MsoNormal\">-Potente vapor permite eliminar arrugas mucho m&aacute;s r&aacute;pido</p>\r\n<p class=\"MsoNormal\">-Suela antiadherente en color que facilita el deslizamiento de la plancha</p>\r\n<p class=\"MsoNormal\">-Cable giratorio de 180&ordm; permite una mayor comodidad al momento de utilizar la plancha</p>\r\n<p class=\"MsoNormal\">-Tanque de agua con 180 ml de capacidad</p>\r\n<p class=\"MsoNormal\">-Potencia de la plancha de 1200 W</p>\r\n<p class=\"MsoNormal\">-Consumo de energ&iacute;a: 180.87 Wh/d&iacute;a</p>', NULL, NULL, '[\"products\\\\September2019\\\\dPo5AUzCeTSM1fbOnSZf.jpg\"]', '2019-09-26 20:46:19', '2019-09-26 20:46:19', NULL, NULL),
(91, 9, 'hdfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-26 20:48:00', '2019-09-26 20:54:22', '2019-09-26 20:54:22', NULL),
(92, 25, 'Set parrillero parrillada cuchillos pinza guante barbacoa', 14, NULL, NULL, '<p class=\"MsoNormal\">- Estuche en tafeta</p>\r\n<p class=\"MsoNormal\">- 6 piezas, 100% en acero pulido</p>\r\n<p class=\"MsoNormal\">- Composici&oacute;n:</p>\r\n<p class=\"MsoNormal\">- Pincel</p>\r\n<p class=\"MsoNormal\">- Tenedor 2 puntas</p>\r\n<p class=\"MsoNormal\">- Cuchillo grande</p>\r\n<p class=\"MsoNormal\">- Esp&aacute;tula</p>\r\n<p class=\"MsoNormal\">- Pinza</p>\r\n<p class=\"MsoNormal\">- Guante</p>', NULL, NULL, '[\"products\\\\September2019\\\\G8vO9kn1WbWU1qgHxka8.jpg\"]', '2019-09-26 20:57:14', '2019-09-26 20:57:14', NULL, NULL),
(93, 25, 'Cafetera Oster Bvstdcdw12 1.8 litros 12 Tazas 900watts', 30, NULL, NULL, '<p class=\"MsoNormal\">- Capacidad para 12 tazas</p>\r\n<p class=\"MsoNormal\">- Interruptor iluminado de encendido</p>\r\n<p class=\"MsoNormal\">- Incluye filtro permanente lavable</p>\r\n<p class=\"MsoNormal\">- Tanque de agua con doble ventanillas</p>\r\n<p class=\"MsoNormal\">- Incluye guarda cable para mantener el mostrador organizado</p>\r\n<p class=\"MsoNormal\">- La canasta removible facilita su llenado y limpieza</p>\r\n<p class=\"MsoNormal\">- Indicador de agua</p>\r\n<p class=\"MsoNormal\">- Cafeteras por goteo</p>', NULL, 2, '[\"products\\\\September2019\\\\tBYyX6IqJ0FpNJBkKLWx.jpg\"]', '2019-09-26 20:58:33', '2019-09-26 20:58:33', NULL, NULL),
(94, 25, 'Olla Arrocera Oster ckstrc1700 10tazas 1.7lts', 35, NULL, NULL, '<p class=\"MsoNormal\">-Taz&oacute;n antiadherente removible para una f&aacute;cil limpieza</p>\r\n<p class=\"MsoNormal\">-Funci&oacute;n autom&aacute;tica para mantener calientes los alimentos y luces indicadoras de cocci&oacute;n para mayor comodidad</p>\r\n<p class=\"MsoNormal\">-Bot&oacute;n de un solo toque para f&aacute;cil manejo</p>\r\n<p class=\"MsoNormal\">-Incluye taza medidora de 180 ml (6.08 oz)</p>', NULL, 2, '[\"products\\\\September2019\\\\bEPgTXrpi3NQI0BPv7Pc.jpg\"]', '2019-09-26 21:04:39', '2019-09-26 21:04:39', NULL, NULL),
(95, 25, 'Extractor De Jugos Oster Fpstje316w-013 400w 2 tazas', 59, NULL, NULL, '<p class=\"MsoNormal\">Envase para la pulpa con capacidad de 1,5 litros para mayor productividad</p>\r\n<p class=\"MsoNormal\">Jarra con capacidad para extraer hasta 600 ml de jugo</p>\r\n<p class=\"MsoNormal\">Las superficies en contacto directo con los alimentos son libres de BPA (bisfenol-A)</p>\r\n<p class=\"MsoNormal\">Palancas de cierre que aseguran el funcionamiento</p>\r\n<p class=\"MsoNormal\">Filtro completamente de acero</p>\r\n<p class=\"MsoNormal\">Componentes f&aacute;ciles de desarmar</p>\r\n<p class=\"MsoNormal\">Color rojo y blanco</p>\r\n<p class=\"MsoNormal\">Potencia de 400 Watts</p>', NULL, 3, '[\"products\\\\September2019\\\\WrikRygODFzz8xBWs6C5.jpg\"]', '2019-09-26 21:06:01', '2019-09-26 21:06:01', NULL, NULL),
(96, 25, 'Exprimidor de Cítricos Oster 25w 1 Litro Fpstju407w-013 Jugo', 24, NULL, NULL, '<p class=\"MsoNormal\">Potente y duradero motor para una mezcla homog&eacute;nea</p>\r\n<p class=\"MsoNormal\">unci&oacute;n de potencia adicional aumenta la velocidad de los batidores con s&oacute;lo tocar un bot&oacute;n</p>\r\n<p class=\"MsoNormal\">M&aacute;xima versatilidad con los batidores y ganchos amasadores</p>\r\n<p class=\"MsoNormal\">Bot&oacute;n de un solo toque que expulsa f&aacute;cilmente los batidores</p>\r\n<p class=\"MsoNormal\">Dise&ntilde;o moderno y elegante le brinda un toque especial a la cocina</p>\r\n<p class=\"MsoNormal\">5 velocidades para mayor control</p>\r\n<p class=\"MsoNormal\">Mango ergon&oacute;mico para un mejor equilibrio y control de la batidora</p>\r\n<p class=\"MsoNormal\">Conveniente base inclinada para mayor estabilidad</p>', NULL, 8, '[\"products\\\\September2019\\\\sKE5crJkGHQYyhBo6K0w.jpg\",\"products\\\\September2019\\\\o5toar9QkrRoeGka7jES.jpg\",\"products\\\\September2019\\\\H13pbaTjbzC9z1582aLw.jpg\"]', '2019-09-26 21:07:56', '2019-09-26 21:07:56', NULL, NULL),
(97, 26, 'Parlante XIAOMI MI bluetooth speaker original', 21.5, NULL, NULL, NULL, NULL, 10, '[\"products\\\\September2019\\\\mtAtT9RtQqRl69xoMHws.jpg\",\"products\\\\September2019\\\\FxIYVoD9179avetw1Hpz.jpg\",\"products\\\\September2019\\\\TFN5m4022Ekj3MTLFlU8.jpg\",\"products\\\\September2019\\\\mPZCtbE4qLoeu4PtXOjY.jpg\",\"products\\\\September2019\\\\zZRuLGMQ9H7r4qFUO20Y.png\",\"products\\\\September2019\\\\bTy8zAntxQ11ZK3DHnZC.png\"]', '2019-09-26 21:18:35', '2019-09-26 21:18:35', NULL, NULL),
(98, 14, 'Auriculares bluetooth, dorados con negro bluetooth stereo', 15, NULL, NULL, '<p class=\"MsoNormal\">Versi&oacute;n Bluetooth: V4.2</p>\r\n<p class=\"MsoNormal\">Respuesta de frecuencia: 20-22 KHz</p>\r\n<p class=\"MsoNormal\">Rango de frecuencia: 2.4-2.48GHz</p>\r\n<p class=\"MsoNormal\">Distancia de transmisi&oacute;n: 10M</p>\r\n<p class=\"MsoNormal\">Soporta Tarjeta memoria (TF Max. 32 GB): S&iacute;</p>\r\n<p class=\"MsoNormal\">Capacidad de bater&iacute;a: 240mAh</p>\r\n<p class=\"MsoNormal\">Tiempo espera: 120 horas</p>\r\n<p class=\"MsoNormal\">Tiempo de trabajo: 8-9 horas</p>', NULL, 5, '[\"products\\\\September2019\\\\seRTufdZBeEee5nq2mLd.jpg\"]', '2019-09-26 21:20:04', '2019-09-26 21:20:04', NULL, NULL),
(99, 14, 'Xiaomi Airdots bluetooth audífonos / auriculares negros', 40.99, NULL, NULL, '<p class=\"MsoNormal\">Los Redmi AirDots cuentan con tecnolog&iacute;a Bluetooth 5.0, mejor conexi&oacute;n con menos interferencias<span style=\"mso-spacerun: yes;\">&nbsp; </span>y un consumo reducido.</p>\r\n<p class=\"MsoNormal\">Altavoz de 7,2 mil&iacute;metros, disfrutaremos de unos graves profundos y un sonido<span style=\"mso-spacerun: yes;\">&nbsp; </span>de gran calidad.</p>\r\n<p class=\"MsoNormal\">Tecnolog&iacute;a TWS (True Wireless Stereo) que permite el sonido est&eacute;reo en ambos al mismo tiempo.</p>\r\n<p class=\"MsoNormal\">El estuche de los Redmi AirDots tiene autonom&iacute;a de 300 mAh con posibilidad para cargarlos, hasta en tres ocasiones.</p>\r\n<p class=\"MsoNormal\">M&aacute;s tiempo sin cables, 40 mAh de autonom&iacute;a en cada auricular</p>', NULL, 3, '[\"products\\\\September2019\\\\w6nvlaWwLm7tNsAnizmg.jpg\",\"products\\\\September2019\\\\aCgxBh7ioG5l7CtAtdn8.jpg\"]', '2019-09-26 21:22:10', '2019-09-26 21:22:10', NULL, NULL),
(100, 14, 'Audifonos dobles bluetooth i7s mini', 11.99, NULL, NULL, '<p class=\"MsoNormal\">Categor&iacute;a: Auriculares de tel&eacute;fono celular</p>\r\n<p class=\"MsoNormal\">Breve descripci&oacute;n:</p>\r\n<p class=\"MsoNormal\">Este es el nuevo dise&ntilde;o, el tama&ntilde;o es m&aacute;s peque&ntilde;o que I7S. Ser&aacute; m&aacute;s c&oacute;modo.</p>\r\n<p class=\"MsoNormal\">Cantidad: 1 pieza</p>\r\n<p class=\"MsoNormal\">Tama&ntilde;o del paquete: 11.0 * 8.0 * 4.0 (cm)</p>\r\n<p class=\"MsoNormal\">Peso bruto / paquete: 0.07 (kg)</p>', NULL, 3, '[\"products\\\\September2019\\\\nQWy8Ca23Yq6eO4v3Qsg.jpg\",\"products\\\\September2019\\\\LQ19QbRBvZ9Mo98eJbLM.jpeg\",\"products\\\\September2019\\\\42gm3Y4sUzGAQZuapVlE.jpg\"]', '2019-09-26 21:24:11', '2019-09-26 21:24:11', NULL, NULL),
(101, 28, 'Cepillo Alisador Plancha De Cabello Con Placas De Cerámica Cepillo Alisador Plancha De Cabello Con Placas De Cerámica', 8.99, NULL, NULL, '<p class=\"MsoNormal\">- Tensi&oacute;n nominal: 110-240 (V)</p>\r\n<p class=\"MsoNormal\">- Potencia nominal: 29 (W)</p>\r\n<p class=\"MsoNormal\">- Frecuencia nominal: 50-60 (Hz)</p>\r\n<p class=\"MsoNormal\">- Color: rosa</p>\r\n<p class=\"MsoNormal\">- Longitud del cable: acerca de 230 cm</p>\r\n<p class=\"MsoNormal\">- Tiempo de calentamiento &oacute;ptimo: 30 segundos</p>\r\n<p class=\"MsoNormal\">- Alisar el cabello en 5 segundos (Dependiendo de cada tipo de cabello)</p>\r\n<p class=\"MsoNormal\"><span style=\"mso-spacerun: yes;\">&nbsp;</span>- Temperatura m&aacute;xima: 230 grados</p>', NULL, 3, '[\"products\\\\September2019\\\\AmgMbDrEim3ORMQZ1KBO.jpg\",\"products\\\\September2019\\\\uz3V4lFtDGcn9xe3ry6n.jpg\",\"products\\\\September2019\\\\NsdINYsYjN3oJIxl9bHH.jpg\"]', '2019-09-26 21:31:00', '2019-09-27 16:36:28', NULL, NULL),
(102, 28, 'Plancha 3 en 1 para cabello, alisado, risado y ondas', 28, NULL, NULL, '<p class=\"MsoNormal\">- Multifuncional 3 en 1 alisado, risado y ondas</p>\r\n<p class=\"MsoNormal\">- Material de conductividad t&eacute;rmica: de cer&aacute;mica y Turmalina</p>\r\n<p class=\"MsoNormal\">- Voltaje: 110-240 V - Frecuencia: 50/60Hz</p>\r\n<p class=\"MsoNormal\">- Potencia: 40W</p>\r\n<p class=\"MsoNormal\">- Control de temperatura Constante de 180 &deg;C - Cable longitud: 2 m</p>', NULL, 2, '[\"products\\\\September2019\\\\qRaQfa96PVYCr20YsR3e.jpg\"]', '2019-09-26 21:33:01', '2019-09-26 21:33:01', NULL, NULL),
(103, 28, 'Cepillo alisador y hace ondas + cepillo alisador plancha de cabello', 15.99, NULL, NULL, '<p class=\"MsoNormal\"><strong style=\"mso-bidi-font-weight: normal;\">CEPILLO ALISADOR Y HACE ONDAS </strong></p>\r\n<p class=\"MsoNormal\">Alisa mientras cepillas, facil uso.</p>\r\n<p class=\"MsoNormal\">Elimina el frizz y sella cuticulas</p>\r\n<p class=\"MsoNormal\">Suavidad y acabado impecable</p>\r\n<p class=\"MsoNormal\">Efecto liso natural, brillo asombroso,</p>\r\n<p class=\"MsoNormal\">Cerdas de cer&aacute;mica</p>\r\n<p class=\"MsoNormal\">Dientes antiquemaduras</p>\r\n<p class=\"MsoNormal\">Temperatura regulable de 80&deg; - 230&deg;</p>\r\n<p class=\"MsoNormal\">Cable rotatorio 360&ordm;.</p>\r\n<p class=\"MsoNormal\">Pantalla LCD con indicador de temperatura.</p>\r\n<p class=\"MsoNormal\">Voltaje: 110 V.</p>\r\n<p class=\"MsoNormal\">Fuente de energ&iacute;a: El&eacute;ctrico.</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\"><strong style=\"mso-bidi-font-weight: normal;\">CEPILLO ALISADOR PLANCHA DE CABELLO ELECTRICO</strong></p>\r\n<p class=\"MsoNormal\">Alisa tu cabello sin da&ntilde;arlo o enredarlo lo mejor sin tener el riesgo de quemar el cabello o el cuero cabelludo causado por las las planchas convencionales. Cuenta con un dise&ntilde;o inovador , que te permite visualizar f&aacute;cilmente la configuraci&oacute;n y la temperatura por medio de una pantalla LCD.</p>', NULL, 2, '[\"products\\\\September2019\\\\OL52uqC6sknJ7s6OEEDR.jpg\"]', '2019-09-26 21:35:00', '2019-09-26 21:35:00', NULL, NULL),
(104, 29, 'Masajeador corporal HM-112', 47.82, NULL, NULL, '<p class=\"MsoNormal\">Ofrece diferentes modos de masajes con los ajustes flexibles del cabezal</p>', NULL, 2, '[\"products\\\\September2019\\\\xCTkVKkYkAnFyTZq3lg6.jpg\",\"products\\\\September2019\\\\5XcTRjxbPUXzFwTHNtWU.jpg\"]', '2019-09-26 21:41:00', '2019-09-26 21:41:29', NULL, NULL),
(105, 29, 'Masajeador de electroterapia', 49.1, NULL, NULL, '<p class=\"MsoNormal\">El Masajeador de Electroterapia Port&aacute;til brinda alivio del dolor mediante Estimulaci&oacute;n Nerviosa El&eacute;ctrica Transcut&aacute;nea (T.E.N.S ). Permite utilizar un tratamiento sin f&aacute;rmacos para aliviar el dolor en m&uacute;sculos y nervios a trav&eacute;s de la estimulaci&oacute;n leve. Los electrodos de larga duraci&oacute;n (Long life Pads) son resistentes y autoadheribles, pueden lavarse hasta 10 veces y utilizarse hasta 150 veces. 3 programas de tratamiento</p>\r\n<p class=\"MsoNormal\">&nbsp;3 diferentes programas de tratamiento dise&ntilde;ados especialmente para el alivio del dolor de hombros, articulaciones, etc. 5 niveles de intensidad :</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">5 Diferentes niveles de intensidad que se puede ajustar en alta o suave, adem&aacute;s de poder seleccionar el electrodo y as&iacute; focalizar el tratamiento Contenido del Empaque : *Masajeador de Electroterapia Port&aacute;til *Manual de instrucciones *Gu&iacute;a de inicio r&aacute;pido con gu&iacute;a para la colocaci&oacute;n de los electrodos *Soporte de electrodos *Cable para electrodos *2 electrodos (Long Life Pads) 2 pilas AAA (LR03)</p>', NULL, 2, '[\"products\\\\September2019\\\\iYkbrNmPhKCzbJ0eaLOO.jpg\",\"products\\\\September2019\\\\6pCoWKQcfJlWkkDyQ8ws.jpg\",\"products\\\\September2019\\\\Ij65NQoYBPWfvuEpriZ3.jpg\"]', '2019-09-26 21:44:26', '2019-09-26 21:44:26', NULL, NULL);
INSERT INTO `products` (`id`, `category_id`, `product_name`, `product_price`, `product_parent_id`, `product_size`, `product_description`, `product_details`, `units_in_stock`, `images`, `created_at`, `updated_at`, `deleted_at`, `product_quick_description`) VALUES
(106, 33, 'Smartwatch U8 Reloj Inteligente Bluetooth Para Android', 10.99, NULL, NULL, '<p class=\"MsoNormal\">SmartWatch U8 Reloj Intelignete Bluetooth ( Android Todas las funciones) ( con Ios solo contestar realizar llamadas) Disponible en blanco y negro</p>\r\n<p class=\"MsoNormal\">Con su funci&oacute;n de reproductor de m&uacute;sica, podr&aacute;s escuchar la m&uacute;sica que tienes almacenada en tu Smartphone hasta por 6 Horas con s&oacute;lo unos cuantos toques en la pantalla de tu Smartwatch. Es toda una experiencia.</p>\r\n<p class=\"MsoNormal\">Cuenta con una alarma anti-robo, la cual sonar&aacute; si es que el reloj se aleja de tu Smartphone despu&eacute;s de 15 metros. Con su funci&oacute;n de alarma tendr&aacute;s hasta 5 alarmas en tu reloj, adem&aacute;s tiene cronometro integrado</p>\r\n<p class=\"MsoNormal\">Cuenta con un pasometro con el cual registraras tus pasos cuando hagas ejercicio, y que mejor que al hacer ejercicio, cuando estes manejando o cuando est&eacute;s en una junta, poder leer tus mensajes, incluyendo WeChat, Facebook, Twitter, WhatsApp, Skype, etc.</p>\r\n<p class=\"MsoNormal\">Notificaciones de mensajes, Whatsapp, Mails, Etc ( Funciones solo para Android)</p>\r\n<p class=\"MsoNormal\">Especificaciones.</p>\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Pantalla t&aacute;ctil capacitiva 1.48\" TFT LCD</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Cron&oacute;metro</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Llamadas a traves del manos libres del reloj (el reloj tiene microfono mas un parlante).</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Material: acero + Silicona.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->En la pantalla puede ver la semana, bater&iacute;a, fecha y hora</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Timbre de aviso cuando se recibe una llamada</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Muestra el n&uacute;mero o el nombre de las llamadas entrantes</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Sincronizaci&oacute;n contactos / SMS / historial de llamadas (s&oacute;lo para tel&eacute;fonos Android).</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Usted puede controlar su tel&eacute;fono celular para tomar fotos desde su mu&ntilde;eca.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Alcance del reloj( conexion Bluetooth): 10m.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Tiempo de conversaci&oacute;n: aproximadamente 3 horas</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Tiempo de carga: alrededor de 2 horas</p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Tiempo en espera: hasta 160 horas</p>', NULL, 20, '[\"products\\\\September2019\\\\Io35B3rNZ2tQgrh9n8mQ.PNG\",\"products\\\\September2019\\\\C5W5mEoCuxoRsyPhUg45.PNG\",\"products\\\\September2019\\\\CO9OPqGrdDdWEaBPo1MN.PNG\"]', '2019-09-30 21:54:00', '2019-09-30 21:55:14', NULL, NULL),
(107, 33, 'Pulsera Deportiva, Reloj Deportivo Smartwatch Bluetooth M2', 10.99, NULL, NULL, '<p class=\"MsoNormal\">- Visualizar Hora: la hora aparece en la pantalla y La fecha..</p>\r\n<p class=\"MsoNormal\">- Medidas de Actividad F&iacute;sica: cuenta y sigue los pasos a diario (pod&oacute;metro) y la distancia recorrida.</p>\r\n<p class=\"MsoNormal\">- Se recomienda mantener alejada del agua.</p>\r\n<p class=\"MsoNormal\">- Mide Sensor de Ritmo Card&iacute;aco, gracias al Sensor fotoel&eacute;ctrico que posee.</p>\r\n<p class=\"MsoNormal\">-Nombre De App para Sincronizaci&oacute;n Android: FundoBracelet</p>\r\n<p class=\"MsoNormal\">- Encendido y apagado manteniendo el bot&oacute;n del centro oprimido y seleccionar la opci&oacute;n a ejecutar.</p>\r\n<p class=\"MsoNormal\">-Pod&oacute;metro.</p>', NULL, 5, '[\"products\\\\September2019\\\\6FiLbAPa3kEt49XEtWzU.PNG\",\"products\\\\September2019\\\\nquOYSlFhCQw6Q0UFoTl.PNG\",\"products\\\\September2019\\\\sLcPmgFoE8PoLLQr7gwm.PNG\",\"products\\\\September2019\\\\Mg6GufkCw2qpKOsnmaE7.PNG\"]', '2019-09-30 21:59:53', '2019-09-30 21:59:53', NULL, NULL),
(108, 33, 'Smartwatch Reloj Samsung Watch Active R500', 270, NULL, NULL, '<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Marca: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Samsung</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">L&iacute;nea: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Gear</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Modelo: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Gear</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Modelo alfanum&eacute;rico: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">SM-R500</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Memoria interna: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">4 GB</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Duraci&oacute;n de la bater&iacute;a: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">4 h</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Resoluci&oacute;n m&aacute;xima: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">360 x 360 px</span></p>\r\n<p class=\"MsoNormal\" style=\"margin: 0cm; margin-bottom: .0001pt; text-indent: -18.0pt; line-height: normal; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white; vertical-align: baseline;\"><!-- [if !supportLists]--><span style=\"font-size: 10.0pt; mso-bidi-font-size: 12.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #333333; mso-fareast-language: ES-EC;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #999999; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">Profundidad m&aacute;xima de resistencia al agua: </span><span style=\"font-size: 12.0pt; mso-fareast-font-family: \'Times New Roman\'; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: #333333; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; mso-fareast-language: ES-EC;\">1.5 m</span></p>', NULL, 2, '[\"products\\\\September2019\\\\bVQ8iCCYhTzmylcSw7Jc.PNG\",\"products\\\\September2019\\\\HYSpEr9P2nARXHE4Ru7v.PNG\",\"products\\\\September2019\\\\Tu2vYyGhaF8tAseB15At.PNG\",\"products\\\\September2019\\\\zaGYMiGoAiCUKoO9DNfK.PNG\",\"products\\\\September2019\\\\1DlEKMBafvooHtjOernY.PNG\"]', '2019-09-30 22:06:00', '2019-09-30 22:10:17', NULL, NULL),
(109, 33, 'Smart Wacth Reloj Touch Para Samsung Y Apple Deportivo', 13.99, NULL, NULL, '<p class=\"MsoNormal\">RELOJ INTELIGENTE (BT), 45.2 * 53.4 * 10.3MM, 50G, 1.4 \", 128 * 128, CAP. TOUCH PANEL, IP57, MULTICOLOR PARA ELEGIR), REVESTIMIENTO DE PL&Aacute;STICO, 28MM PERSONALIZADO, BATER&Iacute;A DE POL&Iacute;MERO DE IONES DE LITIO, 3.7 V / 230MAH. RELOJ INTELIGENTE NEGRO 100PCS, AZUL 100PCS</p>', NULL, 25, '[\"products\\\\September2019\\\\9AIy0lQfZSrQcTiPNDZa.PNG\"]', '2019-09-30 22:08:39', '2019-09-30 22:08:39', NULL, NULL),
(110, 33, 'Smartwatch Fossil Q Gen 3 Watch 46mm Original Reloj Smart', 350, NULL, NULL, '<p class=\"MsoNormal\">Caracter&iacute;sticas completas y especificaciones t&eacute;cnicas Fossil Q Explorist Gen 3</p>\r\n<p class=\"MsoNormal\">&bull;Aplicaciones</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Gesti&oacute;n de la informaci&oacute;n</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Calend&aacute;rio</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Utilidades</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Alarma, Cron&oacute;metro</p>\r\n<p class=\"MsoNormal\">&bull;Audio y video</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Formatos reproductor de m&uacute;sica: Si</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Otras caracter&iacute;sticas audio: Altavoz, Micr&oacute;fono</p>\r\n<p class=\"MsoNormal\">&bull;Bateria</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Capacidad bater&iacute;a</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">370 Amperios hora</p>\r\n<p class=\"MsoNormal\">&bull;Conexion</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Conectividad</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Bluetooth 4.1, Wi-Fi 802.11 b/g/n</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Contenido paquete venta</p>\r\n<p class=\"MsoNormal\">&bull;Contenido del paquete de venta</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Cargador USB, Gu&iacute;a r&aacute;pida</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Dise&ntilde;o</p>\r\n<p class=\"MsoNormal\">&bull;Colores</p>\r\n<p class=\"MsoNormal\" style=\"text-indent: 35.4pt;\">Plata</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left: 0cm; mso-add-space: auto; text-indent: 18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Dimensiones (alto): 46 Mil&iacute;metros</p>\r\n<p class=\"MsoNormal\">Dimensiones (ancho): 46 Mil&iacute;metros</p>\r\n<p class=\"MsoNormal\">Dimensiones (profundidad): 12.5 Mil&iacute;metros</p>\r\n<p class=\"MsoNormal\">Forma: Esfera redonda</p>\r\n<p class=\"MsoNormal\">Materiales: Acero inoxidable, Grado de protecci&oacute;n IP67 (Agua y Polvo), Resistente al agua</p>\r\n<p class=\"MsoNormal\">Peso: 100 Gr</p>\r\n<p class=\"MsoNormal\">&bull;GPS</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Aplicaciones GPS</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Google Maps</p>\r\n<p class=\"MsoNormal\" style=\"margin-left: 35.4pt;\">Memoria</p>\r\n<p class=\"MsoNormal\">Memoria tel&eacute;fono: 4 Gb</p>\r\n<p class=\"MsoNormal\">Mensajeria</p>\r\n<p class=\"MsoNormal\">Email</p>\r\n<p class=\"MsoNormal\">Pantalla</p>', NULL, 8, '[\"products\\\\September2019\\\\gdO03izDwXqPZobyO5VC.PNG\",\"products\\\\September2019\\\\G2ET50I6fJw790sJceBN.PNG\",\"products\\\\September2019\\\\tSM2ZBOLFbYLJcn1ESev.PNG\",\"products\\\\September2019\\\\r9T8l0UBkVsdt9LFKvFd.PNG\"]', '2019-09-30 22:13:40', '2019-09-30 22:13:40', NULL, NULL),
(111, 34, 'Reloj Guess Mujer W0911L6', 182.65, NULL, NULL, '<p class=\"MsoNormal\">Marca: Guess</p>\r\n<p class=\"MsoNormal\">Modelo: W0911L6</p>\r\n<p class=\"MsoNormal\">Tipo: An&aacute;logo</p>\r\n<p class=\"MsoNormal\">G&eacute;nero: Dama</p>\r\n<p class=\"MsoNormal\">Dimensiones de la esfera: 38.0</p>\r\n<p class=\"MsoNormal\">Material de la pulsera: Silicona</p>\r\n<p class=\"MsoNormal\">Color de la pulsera: Azul</p>\r\n<p class=\"MsoNormal\">Color de la esfera: Blanco</p>\r\n<p class=\"MsoNormal\">Funciones: An&aacute;logo</p>\r\n<p class=\"MsoNormal\">Garant&iacute;a del proveedor: 12 meses</p>', NULL, 2, '[\"products\\\\September2019\\\\YrrFVegqAQcv9cWItCwK.PNG\"]', '2019-09-30 22:19:12', '2019-09-30 22:19:12', NULL, NULL),
(112, 34, 'Reloj Guess Mujer W1068L5', 242.65, NULL, NULL, '<p class=\"MsoNormal\">Marca: Guess</p>\r\n<p class=\"MsoNormal\">Modelo: W1068L5</p>\r\n<p class=\"MsoNormal\">Tipo: An&aacute;logo</p>\r\n<p class=\"MsoNormal\">G&eacute;nero: Dama</p>\r\n<p class=\"MsoNormal\">Dimensiones de la esfera: 41.0</p>\r\n<p class=\"MsoNormal\">Material de la pulsera: Cuero</p>\r\n<p class=\"MsoNormal\">Color de la pulsera: Tan</p>\r\n<p class=\"MsoNormal\">Color de la esfera: Blanco</p>\r\n<p class=\"MsoNormal\">Funciones: An&aacute;logo</p>\r\n<p class=\"MsoNormal\">Garant&iacute;a del proveedor: 12 meses</p>', NULL, 3, '[\"products\\\\September2019\\\\3bHq0nbN7ga2zJtYJKNo.PNG\"]', '2019-09-30 22:21:31', '2019-09-30 22:21:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `province` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `provinces`
--

INSERT INTO `provinces` (`id`, `province`, `created_at`, `updated_at`, `deleted_at`, `country_id`) VALUES
(1, 'AZUAY', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(2, 'BOLIVAR', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(3, 'CAÑAR', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(4, 'CARCHI', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(5, 'COTOPAXI', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(6, 'CHIMBORAZO', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(7, 'EL ORO', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(8, 'ESMERALDAS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(9, 'GUAYAS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(10, 'IMBABURA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(11, 'LOJA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(12, 'LOS RIOS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(13, 'MANABI', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(14, 'MORONA SANTIAGO', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(15, 'NAPO', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(16, 'PASTAZA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(17, 'PICHINCHA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(18, 'TUNGURAHUA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(19, 'ZAMORA CHINCHIPE', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(20, 'GALAPAGOS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(21, 'SUCUMBIOS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(22, 'ORELLANA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(23, 'SANTO DOMINGO DE LOS TSACHILAS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(24, 'SANTA ELENA', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1),
(90, 'ZONAS NO DELIMITADAS', '2019-09-11 14:37:05', '2019-09-11 14:37:21', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ref_invoice_status_codes`
--

CREATE TABLE `ref_invoice_status_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_status_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ref_invoice_status_codes`
--

INSERT INTO `ref_invoice_status_codes` (`id`, `invoice_status_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Por pagar', '2019-09-20 18:12:12', '2019-09-20 18:12:12', NULL),
(2, 'Por confirmar', '2019-09-20 18:12:21', '2019-09-20 18:12:21', NULL),
(3, 'Pagada', '2019-09-20 18:12:24', '2019-09-20 18:12:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ref_order_status_codes`
--

CREATE TABLE `ref_order_status_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_status_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ref_order_status_codes`
--

INSERT INTO `ref_order_status_codes` (`id`, `order_status_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Nueva', '2019-09-19 00:16:11', '2019-09-19 00:16:11', NULL),
(2, 'Procesando', '2019-09-19 00:16:43', '2019-09-19 00:16:43', NULL),
(3, 'Completada', '2019-09-19 00:16:57', '2019-09-19 00:16:57', NULL),
(4, 'Cerrada', '2019-09-19 00:17:10', '2019-09-19 00:17:10', NULL),
(5, 'Cancelada', '2019-09-19 00:17:19', '2019-09-19 00:17:19', NULL),
(6, 'En espera', '2019-09-19 00:17:27', '2019-09-19 00:17:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'Super administrator', '2019-08-31 00:21:52', '2019-09-28 15:57:39'),
(2, 'user', 'Cliente', '2019-08-31 00:22:40', '2019-09-02 22:13:30'),
(3, 'administrador', 'administrador', '2019-09-28 15:58:53', '2019-09-28 15:58:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Aladdin', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\September2019\\Pk7J6OjxzJ9Cp2mGYAE2.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Crisol Admin', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\September2019\\CiR8Ek9q3x94ZUNeaG6h.gif', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.tax_rate', 'Porcentaje de impuesto', '12', NULL, 'text', 6, 'Site'),
(12, 'site.image_logo_solo', 'Logo solo', 'settings\\September2019\\0fSupioBRSL9THjU1bHV.png', NULL, 'image', 7, 'Site'),
(13, 'site.terminos_condiciones', 'terminos y condiciones', '<h1>PROTECCI&Oacute;N DEL COMPRADOR</h1>\r\n<h2 style=\"text-align: justify;\">GARANT&Iacute;A DE REEMBOLSO</h2>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">Te garantizamos el reembolso completo de tu dinero si el art&iacute;culo que recibes es muy distinto a lo que se presenta en la descripci&oacute;n o si no lo recibes dentro del plazo de entrega estimado. Recibir&aacute;s tu dinero de vuelta en un plazo de 15 d&iacute;as desde que aprobamos la solicitud.</p>\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">1.<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Contacta con el vendedor</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-align: justify;\">Ve al historial del pedido y selecciona el art&iacute;culo. Explica tu situaci&oacute;n al vendedor para llegar a un acuerdo y barajar las posibles soluciones.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">2.<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Solicita el reembolso</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-align: justify;\">Si no llegas a un acuerdo con el vendedor, abre una disputa en la p&aacute;gina del pedido. Aseg&uacute;rate de que no han pasado m&aacute;s de 15 d&iacute;as desde que recibiste el art&iacute;culo.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><!-- [if !supportLists]--><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">3.<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Recibe el dinero en 15 d&iacute;as</p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"text-align: justify;\">La mayor&iacute;a de los vendedores devuelven el dinero en 15 d&iacute;as, si no lo hacen, contacta con nosotros en la p&aacute;gina de descripci&oacute;n del pedido para escalar tu disputa y que podamos ayudarte.</p>\r\n<h2>Pol&iacute;tica de Compra</h2>\r\n<p class=\"MsoNormal\">Para comprar en CRISOL es necesario que te hayas registrado, as&iacute; que si no tienes una cuenta, en el momento en que finalices la compra te solicitaremos registrar tu cuenta de usuario, luego de lo cual continuar&aacute;s con el proceso de compra.</p>\r\n<p class=\"MsoNormal\">En CRISOL queremos que tus compras sean r&aacute;pidas, f&aacute;ciles y seguras.</p>\r\n<h2>Pol&iacute;tica de devoluci&oacute;n</h2>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">El producto deber&aacute; entregarse en su caja original con todos los componentes internos con los que vino, el papel de garant&iacute;a t&eacute;cnica del producto (si el producto lo tiene) y el certificado de compra emitido por la tienda.</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">Se puede aplicar devoluci&oacute;n del bien o servicio solo dentro de los primeros 3 d&iacute;as de haberlo recibido, siempre y cuando lo permita su naturaleza y el estado del bien sea el mismo en el que lo recibi&oacute;. Causas de devoluci&oacute;n:</p>\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo2;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Art&iacute;culo no corresponde al detalle de la factura.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo2;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Art&iacute;culo con defectos de f&aacute;brica.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo2;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Art&iacute;culo entregado con golpes, hendiduras o defectos f&iacute;sicos.</p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"text-align: justify; text-indent: -18.0pt; mso-list: l1 level1 lfo2;\"><!-- [if !supportLists]--><span style=\"font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;\"><span style=\"mso-list: Ignore;\">&middot;<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]-->Cambio por otro de mayor valor.</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">Para poder devolver tu producto debes tener en cuenta las siguientes condiciones con respecto al estado del mismo:</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">&bull; El producto no debe haber sido abierto ni tener violentado los sellos de seguridad del mismo cuando aplique y por tanto no mostrar se&ntilde;ales de uso.</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">&bull; El producto se debe encontrar con todos sus accesorios, manuales, con su embalaje original y sin deterioros (rayas, piquetes, abolladuras, manchas, desgaste, etc.).</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">&bull; El producto no debe tener m&aacute;s de CINCO (5) d&iacute;as h&aacute;biles de haber sido entregado.</p>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">&bull; En el caso de productos que se vendieron en promoci&oacute;n, se requiere la entrega de todos los productos o regalos, incluidos en la promoci&oacute;n correspondiente.</p>\r\n<h2>Costes de env&iacute;o para devoluciones</h2>\r\n<p class=\"MsoNormal\" style=\"text-align: justify;\">El comprador asume los costes de env&iacute;o a la direcci&oacute;n de devoluci&oacute;n local especificada.</p>', NULL, 'rich_text_box', 8, 'Site');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipments`
--

CREATE TABLE `shipments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `shipment_tracking_number` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipment_date` datetime DEFAULT NULL,
  `order_shipment_details` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping_methods`
--

CREATE TABLE `shipping_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `shipping_method` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` double UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `shipping_methods`
--

INSERT INTO `shipping_methods` (`id`, `shipping_method`, `cost`, `created_at`, `updated_at`) VALUES
(1, 'servientrega', 5, '2019-09-17 23:33:31', '2019-09-17 23:33:31'),
(2, 'moto', 2, '2019-09-17 23:33:00', '2019-09-18 18:28:46'),
(3, 'glovo', 1.9, '2019-09-18 18:28:38', '2019-09-18 18:28:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider_images`
--

CREATE TABLE `slider_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_path` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_1` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_2` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_3` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `text_right` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `slider_images`
--

INSERT INTO `slider_images` (`id`, `image_path`, `label_1`, `label_2`, `label_3`, `created_at`, `updated_at`, `deleted_at`, `text_right`) VALUES
(4, 'slider-images\\September2019\\PCgn21Bk8QksP4juN8do.png', 'APROVECHA', 'MAGNÍFICOS DESCUENTOS', 'SOLO POR ESTE MES', '2019-09-26 22:06:00', '2019-09-26 22:25:15', '2019-09-26 22:25:15', 0),
(5, 'slider-images\\September2019\\fEhc5VNYbZ1ZXB33dCGM.png', NULL, NULL, NULL, '2019-09-26 22:24:00', '2019-09-26 22:58:48', '2019-09-26 22:58:48', 0),
(6, 'slider-images\\September2019\\vUlKWucX87aMspclbCp3.jpg', NULL, NULL, NULL, '2019-09-26 22:57:00', '2019-09-26 23:02:16', '2019-09-26 23:02:16', 0),
(7, 'slider-images\\September2019\\vQTG06jI1ROg8GbKRvjk.png', NULL, NULL, NULL, '2019-09-26 23:02:00', '2019-09-27 15:15:46', '2019-09-27 15:15:46', 0),
(8, 'slider-images\\September2019\\PZeHUDuzoBa864yamzHI.png', NULL, NULL, NULL, '2019-09-26 23:03:06', '2019-09-27 13:47:07', '2019-09-27 13:47:07', 0),
(9, 'slider-images\\September2019\\RH643nGb3l2vU1tntyRt.png', NULL, NULL, NULL, '2019-09-27 13:47:18', '2019-09-27 14:44:54', '2019-09-27 14:44:54', 0),
(10, 'slider-images\\September2019\\9FUsPOECH2mFIOqBDgcT.png', NULL, NULL, NULL, '2019-09-27 14:15:00', '2019-09-27 15:15:51', '2019-09-27 15:15:51', 0),
(11, 'slider-images\\September2019\\CbZ3Sfx0W6OUwfqylXkT.png', NULL, NULL, NULL, '2019-09-27 14:44:28', '2019-09-27 14:44:28', NULL, 0),
(12, 'slider-images\\September2019\\oIkoDa1eEbyJwelOgpm5.png', NULL, NULL, NULL, '2019-09-27 15:09:00', '2019-09-27 19:32:39', '2019-09-27 19:32:39', 0),
(13, 'slider-images\\September2019\\tBWncBJ48lq6VbcxlcFr.png', NULL, NULL, NULL, '2019-09-27 15:38:00', '2019-09-27 19:32:42', '2019-09-27 19:32:42', 0),
(14, 'slider-images\\September2019\\umclc1mKpZy3oWcO5b55.png', NULL, NULL, NULL, '2019-09-27 19:32:56', '2019-09-27 19:32:56', NULL, 0),
(15, 'slider-images\\September2019\\hn2jEW2QrMJ47GSNJAEO.png', NULL, NULL, NULL, '2019-09-27 19:33:05', '2019-09-27 19:36:05', '2019-09-27 19:36:05', 0),
(16, 'slider-images\\September2019\\Ne7W3XgDV58nIasT85af.png', NULL, NULL, NULL, '2019-09-27 19:36:15', '2019-09-27 19:36:15', NULL, 0),
(17, 'slider-images\\September2019\\zKyM8ZADHJ4or03l7YeP.jpg', NULL, NULL, NULL, '2019-09-27 22:17:21', '2019-09-27 22:39:14', '2019-09-27 22:39:14', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'moshe', 'arturo.ronquillo94@gmail.com', 'users/default.png', NULL, '$2y$10$fbEXHR/J5WMWtfB1slftqeXOfvh54NC4ZinrdC5HghnaT2nkefQMK', 'AxCVfgChnxxlgaty9FKYEYMG55o7LoF2qp9p5tDtkikT2Qm3YUoGz62Or74u', NULL, '2019-08-31 00:21:52', '2019-08-31 00:21:52'),
(2, 2, 'Jaime Roldos', 'jaime.rolditos@gmail.com', 'users/default.png', NULL, '$2y$10$3H/RtFdLHm7U8z4ifqwJpOD5NGJ0Nbb9szWrxhfi2RBtj3X/10hae', 'BwFHzkUpds8jxJKiS6KwhjZkzJzw6ql9irTQLqT1Ef8FNn1Tv2MsY9sJWVrA', NULL, '2019-09-02 22:09:35', '2019-09-02 22:09:35'),
(3, 2, 'ruffo', 'ruffito@noexiste.com', 'users/default.png', NULL, '$2y$10$tKmyZvd3zkEhgSalRbvFZuQEtX6fGmAQoo3FUZn3PSrgYowK3gS6W', 'P42nucoHxxKrkDR405bpm5Y6voAusdvtyW9p7ReSy3Gx4lRHDfcmrRTiqrho', NULL, '2019-09-04 02:06:26', '2019-09-04 02:06:26'),
(4, 2, 'patricio', 'patricio@gmail.com', 'users/default.png', NULL, '$argon2i$v=19$m=1024,t=2,p=2$TkpSNDRrcjVuWTBDdTJESg$rj6JF1jHDJ1btv1T9W85lh5+yvmywHMozuyXswUzGmU', 'KxIibzaf9ojcM2kEbBcfzGyb6UDpyA277J5TQheMNamFV8mRRgWvGKCEqh9a', NULL, '2019-09-25 19:23:04', '2019-09-25 19:23:04'),
(5, 2, 'carlos', 'arturo1@gmail.com', 'users/default.png', NULL, '$argon2i$v=19$m=1024,t=2,p=2$SS9CQXJFdi81ZXdyRVYvWQ$vVLMMJkIO7Q0BTJgoL743LaQFjwQQ+/p18EN1ldLmYw', NULL, NULL, '2019-09-26 16:04:35', '2019-09-26 16:04:35'),
(6, 3, 'Ing. Abigail Morales', 'mmorales@romerodyasociados.com', 'users\\September2019\\6q5551pkKttmTQH8FHlq.jpg', NULL, '$argon2i$v=19$m=1024,t=2,p=2$S3Q1SHpEc1dNdEJlUlVWcA$C4aFO+4Gk+r03udlCReGdDDqR4uxgSAj/7bqoIXqbHU', '1kt7rLY5py5jC44cu3dmC5TetbobXGC9XyCYBpobhNZCIvNyw1sigZy53iXE', '{\"locale\":\"en\"}', '2019-09-26 16:05:18', '2019-09-28 15:59:16'),
(7, 3, 'Monica Gomez', 'mgomez@romerodyasociados.com', 'users/default.png', NULL, '$2y$10$mx/dpKu2Gm7oddGpU84QPO06Bct.HDoEvPBkZN4mhf5mh1S9e4ln6', NULL, '{\"locale\":\"en\"}', '2019-09-26 16:05:24', '2019-09-28 16:02:30'),
(8, 2, 'Kenia Morales', 'abimorales2393@gmail.com', 'users/default.png', NULL, '$argon2i$v=19$m=1024,t=2,p=2$dW5YUXBlRi5pTlBudVNZbQ$hHxN+DUmk4uk1N9wDqkJGkyGWt92XqNlDgkrOOdaSgk', NULL, NULL, '2019-09-30 18:57:40', '2019-09-30 18:57:41'),
(9, 2, 'michi', 'michi@gmail.com', 'users/default.png', NULL, '$argon2i$v=19$m=1024,t=2,p=2$VjRLOU0yalpGRVcxU1hWRQ$ifvS5RjX72B1UoFkO6ebLalF2QF62s6BYxlVNiVHcu0', NULL, NULL, '2019-09-30 20:54:13', '2019-09-30 20:54:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_address_users_id_foreign` (`user_id`);

--
-- Indices de la tabla `customer_information`
--
ALTER TABLE `customer_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_information_users_id_foreign` (`user_id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `discount_codes`
--
ALTER TABLE `discount_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_orders_id_foreign` (`order_id`),
  ADD KEY `invoices_ref_invoice_status_codes_foreign` (`invoice_status_code`);

--
-- Indices de la tabla `invoice_line_items`
--
ALTER TABLE `invoice_line_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_line_items_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_line_items_products_id_foreign` (`product_id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_users_id_foreign` (`user_id`),
  ADD KEY `order_status_code_idx` (`order_status_code`);

--
-- Indices de la tabla `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_order_id_foreign` (`order_id`),
  ADD KEY `order_items_product_id_foreign` (`product_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_invoices_id_foreign` (`invoice_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categories_id_foreign` (`category_id`);

--
-- Indices de la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ref_invoice_status_codes`
--
ALTER TABLE `ref_invoice_status_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ref_order_status_codes`
--
ALTER TABLE `ref_order_status_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipments_orders_id_foreign` (`order_id`),
  ADD KEY `shipments_invoices_id_foreign` (`invoice_id`);

--
-- Indices de la tabla `shipping_methods`
--
ALTER TABLE `shipping_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `customer_information`
--
ALTER TABLE `customer_information`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `discount_codes`
--
ALTER TABLE `discount_codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT de la tabla `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT de la tabla `ref_invoice_status_codes`
--
ALTER TABLE `ref_invoice_status_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ref_order_status_codes`
--
ALTER TABLE `ref_order_status_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `shipments`
--
ALTER TABLE `shipments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `shipping_methods`
--
ALTER TABLE `shipping_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD CONSTRAINT `customer_address_users_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `customer_information`
--
ALTER TABLE `customer_information`
  ADD CONSTRAINT `customer_information_users_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_orders_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `invoices_ref_invoice_status_codes_foreign` FOREIGN KEY (`invoice_status_code`) REFERENCES `ref_invoice_status_codes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `invoice_line_items`
--
ALTER TABLE `invoice_line_items`
  ADD CONSTRAINT `invoice_line_items_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `invoice_line_items_order_items_id_foreign` FOREIGN KEY (`id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `invoice_line_items_products_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ref_code_id_foreign` FOREIGN KEY (`order_status_code`) REFERENCES `ref_order_status_codes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_users_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_invoices_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_categories_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `shipments`
--
ALTER TABLE `shipments`
  ADD CONSTRAINT `shipments_invoices_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `shipments_orders_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
