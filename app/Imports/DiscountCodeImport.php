<?php

namespace App\Imports;

use App\DiscountCode;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
class DiscountCodeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // \Log::info(Hash::make($row[0]));
        return new DiscountCode([
            'ci' => $row[0],
            'code' =>$row[1],
            'discount_percentage' => $row[2]
        ]);
    }
}
