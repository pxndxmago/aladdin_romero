<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GregoryDuckworth\Encryptable\EncryptableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCode extends Model
{
    use EncryptableTrait;
    use SoftDeletes;
    
	/**
     * Encryptable Rules
	 *
     * @var array
	 */
    protected $encryptable = [
        'ci'
	];
    protected $fillable = ['ci', 'code', 'discount_percentage'];
}
