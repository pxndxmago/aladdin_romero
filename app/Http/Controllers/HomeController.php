<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SliderImage;
use App\Product;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $slider_images = SliderImage::all();
        $latest_products = Product::orderBy('created_at', 'desc')->get();

        return view('home', compact('slider_images','latest_products'));
    }
}
