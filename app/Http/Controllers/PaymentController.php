<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\CustomerInformation;
use App\CustomerAddress;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\OrderItem;
use App\Invoice;
use App\InvoiceLineItem;
use App\DiscountCode;
use App\Payment;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class PaymentController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $cart= \Session::get('cart');
        $countries = Country::all();
        $user = Auth::user();
        $cust_inf = null;

        // Si el carrito no tiene productos se redirecciona a la pagina de productos
        if (!isset($cart) or count($cart)==0 ) {
            return redirect()->route('shop');
        }

        // Validar si el cliente ha seleccionado una direccion de entrega y metodo de envío
        $errors_ = [];
        if( !(isset($cart['shipping_method']) and $cart['shipping_method']!=null) ){
            $errors_[] = "Debe seleccionar un metodo de envío";
        }

        if( !(isset($cart['shipping_address']) and $cart['shipping_address']!=null) ){
            $errors_[] = "Debe seleccionar una dirección de envío";
        }

        if ($user) {
            $cust_inf = CustomerInformation::select('customer_information.*')
                                            ->where('user_id', $user->id)
                                            ->orderBy('customer_information.created_at','DESC')
                                            ->first();

            // Se verifica si la ultima iinformacion ingresada por el usuario esta completa o no
            if($cust_inf!=null){
                if(
                    trim($cust_inf->id) == '' or
                    trim($cust_inf->first_name) == '' or
                    trim($cust_inf->last_name) == '' or
                    trim($cust_inf->dni) == '' or
                    trim($cust_inf->telephone) == '' or
                    trim($cust_inf->mobile) == '' or
                    trim($cust_inf->email) == ''
                ){
                    $errors_[] = "Debe llenar todos los campos de informacion de usuario";
                }
                
            }else{
                $errors_[] = "Debe llenar todos los campos de informacion de usuario";
            }

            if ($errors_) {
                \Session::flash('errors_', $errors_);
                return redirect()->route('checkout');
            }
        } else {

            return redirect()->route('checkout');
            
        }
        
        return view('review_payment', compact('cart','countries','cust_inf'));
    }

    public function place_order(){
        DB::beginTransaction();
        $cart= \Session::get('cart'); 
        \Log::info("Inicio de place order");
        
        try {

            // Se crea una nueva orden
            $new_order = new Order();
            $new_order->user_id = Auth::user()->id;
            $new_order->order_status_code = 1;//Order status code 1 = Nueva
            $new_order->date_order_placed = date("Y-m-d H:i:s");
            $new_order->order_details = "";
            $new_order->save();
            \Log::info("Orden creada");

            // Se crea una facura (Invoice) con el estado por pagar
            $new_invoice = new Invoice();
            $new_invoice->order_id = $new_order->id;
            $new_invoice->invoice_status_code = 1;//Invoice status code 1 = Por pagar
            $new_invoice->invoice_date = date("Y-m-d H:i:s");
            $new_invoice->invoice_details = "";//******
            $new_invoice->save();
            \Log::info("Factura creada");

            $order_amount = 0;
            $invoice_details = "";
            // Se crean order_items por cada item en el carrito
            foreach ($cart['items'] as $key => $item) {

                // Order item
                $new_order_item = new OrderItem();
                $new_order_item->order_id = $new_order->id;
                $new_order_item->product_id = $item->id;
                $new_order_item->product_quantity = $item->quantity;
                $new_order_item->other_order_item_details = "$item->product_name x $item->quantity \n";
                $new_order_item->save();

                // Se concatenan los detalles de cada item
                $invoice_details = $invoice_details . $new_order_item->other_order_item_details;

                \Log::info("Order item creado");

                // Invoice line item
                $new_invoice_line_item = new InvoiceLineItem();
                $new_invoice_line_item->id = $new_order_item->id;
                $new_invoice_line_item->invoice_id = $new_invoice->id;
                $new_invoice_line_item->product_id = $item->id;
                $new_invoice_line_item->product_title = $item->product_name;
                $new_invoice_line_item->product_quantity = $item->quantity;
                $new_invoice_line_item->product_price = $item->product_price;

                    //El descuento empieza en 1 por que sera multiplicado por el valor derivado de esta forma si no hay descuento no dismuye el valor
                    $discount = 1; 
                    if (isset($cart['discountCode'])) {
                        $discountCode = $cart['discountCode'];
                        if($discountCode){
                            $discount = $discount - round($discountCode->discount_percentage/100,2);
                        }
                    }

                    //precio x cantidad x el descuento 
                    $derived_product_cost = round($item->product_price * $discount * $item->quantity, 2);
                    
                $new_invoice_line_item->derived_product_cost = $derived_product_cost;
                    
                    //se calcula el iva por
                    $derived_vat = round($derived_product_cost * setting('site.tax_rate')/100, 2);

                $new_invoice_line_item->derived_vat = $derived_vat;

                $new_invoice_line_item->derived_total_cost = $derived_product_cost + $derived_vat;
                    $order_amount = $order_amount + $new_invoice_line_item->derived_total_cost;
                $new_invoice_line_item->other_line_item_details = $item->product_name." x ".$item->quantity ;
                
                $new_invoice_line_item->save(); 
                \Log::info("Invoice line item creado");
                    
            }

            $new_order->amount = round($order_amount, 2);             
            $new_order->save();
                // Luego de ser usado un codigo de descuento se elimina de forma logica 
                if(isset($discountCode)){
                    $discount_code = DiscountCode::find($discountCode->id);
                    $discount_code->delete();
                }            
            $new_invoice->invoice_details = $invoice_details;
            $new_invoice->save();
            $new_order->order_details = $invoice_details;
            $new_order->save();

            DB::commit();
            \Session::forget('cart');
        } catch (\Exception $e) {
            \Log::info("Error".$e->getMessage());
            DB::rollBack();
            return response()->json([
                'ok'=>false
            ]);
        }

        return response()->json([
            'ok'=>true,
            'message'=>"La orden se ha enviado con éxito",
            'route'=> URL::signedRoute('myaccount.show-order', $new_order->id)
        ]);
    }

    public function store(Request $request){
        try {
            // Procesa la subida de los comprobantes de pago por factura
            $new_payment = new Payment();
            $new_payment->invoice_id = $request->invoice_id;
            $new_payment->payment_date = date("Y-m-d H:i:s");
            $images = [];

            foreach($request->vouchers as $file)
            {
                $path = Storage::putFile('/public/vouchers', $file);
                //extrae SOLO el nombre del archivo que se genera al final de la ruta path 
                $name = explode('/', $path)[2]; 
                $images[] = '\\vouchers\\'.$name;  
            }

            $new_payment->voucher_images = json_encode($images);
            $new_payment->save();

            $invoice = Invoice::find($request->invoice_id);

            if ($invoice) {
                $invoice->invoice_status_code = 2;
                $invoice->save();
            }
        } catch (Exception $e) {
            \Log::info(print_r($e->getTrace(), true));
        }
    } 
}
