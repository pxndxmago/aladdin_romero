<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use DB;

class ShopController extends Controller
{
    function list_id_categories($categories){
        $data = [];
        
        foreach($categories as $category){
            $data[] = $category->id;
            foreach ($this->list_id_categories($category->children) as $category_2) {
                $data[] = $category_2;
            }
        }
        return $data;
    }

    public function index(Request $request){
        $per_page = 10;
        $products = Product::query(); //Consulto los productos paginados por grupos de 5

        $categories = null; 
        $parents_categories = null;
        // Se cargan las caracteristicas qe no tienen padres (principales) 
        $child_categories = Category::with('children')->whereNull('parent_category_id')->get();
        
        // Si se envio una categoria en la url se verifica si existe y se consultan sus productos
        if($request->has('category')){

            $category_name = $request->only('category')['category'];
            $id_category =null;
            
            $all_categories = Category::all();

            // Se busca la categoria por el nombre
            foreach ($all_categories as $key => $category) {
                if($category->category_name == $category_name){
                    $id_category = $category->id;
                    break;
                }
            }
            // si la categoria existe se buscan las 
            // categorias padres(para el breadcumb) e hijas para la seccion Categorias

            if($id_category!= null){
                $parents_categories = Category::find($id_category)->getAllParents()->reverse();

                // Se consultan categorias hijas en caso de tener
                $child_categories_2 = Category::where('parent_category_id', $id_category)->get();
                
                $category_ids = $this->list_id_categories($child_categories_2);
                $category_ids[] = $id_category;
                
                $products = $products->whereIn('category_id',$category_ids);
            }
            // $categories = Category::where('parent_category_id')->get();
        }
        if($request->has('price_range')){
            $price_range = explode('-', $request->only('price_range')['price_range']);
            // dd($request->only('price_range')['price_range']);
            $min = $price_range[0];
            $max = $price_range[1];

            $products = $products->whereBetween('product_price',[$min,$max]);
        }

        if($request->has('search')){
            $search = $request->only('search')['search'];
            $products = $products->where('product_name','LIKE','%'.$search.'%');
        }
        
        $products = $products->paginate($per_page)->appends($request->all());
        return view('shop', compact('products','parents_categories','child_categories'));
    }
    
    public function product_description($product_slug){
        $product = Product::find($product_slug);
        if ($product) {
            return view('product', compact('product'));
        } else {
            return redirect('shop');
        }
    }
}
