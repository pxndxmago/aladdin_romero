<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\DiscountCode;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\DiscountCodeImport;

class DiscountCodesController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        \Log::info($request->file);
        $path = $request->file('file')->getRealPath();
        Excel::import(new DiscountCodeImport, $path);
        \Log::info("importado con exito");

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }
}
