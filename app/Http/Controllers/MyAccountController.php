<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Order;
use App\Invoice;
use App\OrderItem;
use App\Payment;
use App\CustomerInformation;
use DB;


class MyAccountController extends Controller
{
    public function index(){
        return view('my-account');
    }

    public function account_information(){
        $cust_inf = CustomerInformation::where('user_id', Auth::user()->id)
                                        ->orderBy('customer_information.created_at', 'DESC')
                                        ->first();

        return view('account_information', compact('cust_inf'));
    }

    public function my_orders(){
        $orders = Order::withTrashed()
                        ->select('orders.id as order_id',
                                'user_id',
                                'date_order_placed',
                                'order_details',
                                'ref_code.order_status_description',
                                'orders.amount'
                                )
                        ->where('user_id', Auth::user()->id)
                        ->join('ref_order_status_codes as ref_code','ref_code.id','orders.order_status_code')
                        ->get();
        return view('my-orders', compact('orders'));
    }
 
    public function show_order( $order){
        $order = Order::withTrashed()->find($order);

        // DB::listen(function($query){
            //Imprimimos la consulta ejecutada
            $invoice = Invoice::withTrashed()
                                ->where('order_id', $order->id)
                                ->join('ref_invoice_status_codes as ref_i','ref_i.id','invoices.invoice_status_code')
                                ->select('invoices.*', 'ref_i.invoice_status_description')
                                ->first();//Por el momento solo se lista una sola factura pero la estructura de la tabla contempla manejas varias facturas
            

            $order_items = OrderItem::where('order_id', $order->id)
                                    ->join('invoice_line_items as ili','ili.id','order_items.id')
                                    ->join('products as prod','prod.id','ili.product_id')
                                    ->select('ili.*','order_items.*', 'prod.images','prod.product_name')
                                    ->get();
            
            if ($invoice->invoice_status_code == 1 ) {
                $f_payable = true;
            }else {
                $f_payable = false;
            }
            $payments = Payment::withTrashed()
                                ->where('invoice_id', $invoice->id)
                                ->orderBy('payment_date','ASC')
                                ->get();

        // \Log::info($query->sql);
        // });
        return view('show-order', compact('order', 'payments', 'invoice' ,'order_items','f_payable'));
    }
}
