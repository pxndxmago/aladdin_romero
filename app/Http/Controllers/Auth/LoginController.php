<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\URL;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }
    
    public function login(Request $request)
    {
    //validate the fields.... 
        
        $credentials = $request->only('login_email', 'login_password','redirect_route');

        if (Auth::attempt(['email'=>$credentials['login_email'], 'password'=>$credentials['login_password'] ],$request)) {
            // Authentication passed...
            \Log::info($credentials['redirect_route']);
            return redirect($credentials['redirect_route']);
        }else{
            return redirect()->route('home');
        }

    }
    // Se sobreescribe el metodo logout para evitar que se eliminen todas las sesiones incluido el carrito 'cart' 
    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect()->route('home');
    }
}
