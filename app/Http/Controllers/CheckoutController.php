<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\CustomerInformation;
use Illuminate\Support\Facades\Auth;
use App\ShippingMethod;
use App\CustomerAddress;
use App\Http\Controllers\CartController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class CheckoutController extends Controller
{

    public function index(){
        $cart= \Session::get('cart');
        
        if (!isset($cart) || count($cart['items'])==0) {
            return redirect()->route('shop');
        }

        $countries = Country::all();
       
        $cust_inf = null;

        if (!Auth::guest()) {
            $cust_inf = CustomerInformation::select('customer_information.*')
                                            ->where('user_id', Auth::user()->id)
                                            ->orderBy('customer_information.created_at','DESC')
                                            ->first();
        }
        
        $shipping_methods = ShippingMethod::all();

        $cust_add = null;

        if (!Auth::guest()) {
            $cust_add = CustomerAddress::join('cities as ci','customer_addresses.city_id','ci.id')
                                        ->join('provinces as pr','ci.province_id','pr.id')
                                        ->join('countries as co','pr.country_id','co.id')
                                        ->select('customer_addresses.*','ci.city', 'pr.province', 'co.country')
                                        ->where('user_id',Auth::user()->id)
                                        ->get();
        }
        
        return view('checkout', compact('cart','countries','cust_inf','cust_add', 'shipping_methods'));
    }

    public function store_customer_information(Request $request){
        $user = Auth::user();
        $ok = false;
        $redirect ="";
        $detail = "";
        $cart= \Session::get('cart');

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|regex:/^[a-zA-Z ]{2,254}+$/u',
            'last_name' => 'required|regex:/^[a-zA-Z ]{2,254}+$/u',
            'dni' => 'required|numeric|digits:10',
            'telephone' => 'required|numeric|digits_between:9,10',
            'mobile' => 'required|numeric|digits_between:9,10',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
       }

        if(!$user){
            $detail = "user_not_logged";
        }else {
            $customer_information =  CustomerInformation::firstOrCreate(
                [   
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'dni'=>$request->dni,
                    'telephone'=>$request->telephone,
                    'mobile'=>$request->mobile,
                    'email'=>$request->email,
                    'user_id'=>$user->id
                ],
                [
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'dni'=>$request->dni,
                    'telephone'=>$request->telephone,
                    'mobile'=>$request->mobile,
                    'email'=>$request->email,
                    'user_id'=>$user->id
                ]
            );

            $redirect = URL::previous(); 
            $ok = true;
        }
        
        
        \Session::put('cart',$cart);
        return response()->json([
            'ok' => $ok,
            'detail' => $detail,
            'redirect' => $redirect
        ]);

    }

    public function store_customer_address(Request $request){

        $user = Auth::user();
        $ok = false;
        $redirect ="";
        $detail = "";

        if(!$user){

            $detail = "user_not_logged";
            $redirect = route('home'); 

        }else {

            if ($request->id_address!=null) {
                
                $cut_add = CustomerAddress::find($request->id_address);
                if($cut_add){
                    $cut_add->delete();
                }
            }

            $customer_address = new CustomerAddress();
            $customer_address->city_id = $request->city;
            $customer_address->user_id = $user->id;
            $customer_address->address_1 = $request->address_1;
            $customer_address->address_2 = $request->address_2;
            $customer_address->save();

            // $redirect = route('payment'); 
            $ok = true;
        }

        $cartController = new CartController();
        $cartController->setAddress($customer_address->id);

        return redirect()->route('checkout');

    }
    public function edit_customer_address($addresss){
        
        $customer_address = CustomerAddress::join('cities as ci','customer_addresses.city_id','ci.id')
                                            ->join('provinces as pr','ci.province_id','pr.id')
                                            ->join('countries as co','pr.country_id','co.id')
                                            ->select('customer_addresses.*','ci.city','pr.id as province_id', 'pr.province','co.id as country_id', 'co.country')
                                            ->where('customer_addresses.id', $addresss)
                                            ->first();

        return $customer_address;
    }
}
