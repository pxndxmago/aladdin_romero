<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;
use App\CustomerInformation;
use App\ShippingMethod;
use App\CustomerAddress;
use Auth;
use App\DiscountCode;

class CartController extends Controller
{
    public function __construct()
    {
        // Si la variable de sesion 'cart' no existe se crea al momento de invocar la clase
    	if(!\Session::has('cart'))\Session::put('cart', array('items'=>[],'shipping_method'=>null,'shipping_address'=>null));
    }
   
    public function show()
    {
        
        $cart = \Session::get('cart');
        
        $subtotal = 0;
        
        // Se recorren los items para realizar el recuento del subtotal
        if($cart!=null){
            foreach ($cart['items'] as $key => $item) {
                $subtotal = $subtotal + $item->product_price * $item->quantity;
            }
        }

        $discount = 0;
        $discount_p = 1;
        if (isset($cart['discountCode'])) {
            $discountCode = $cart['discountCode'];
            
            if($discountCode){
                $discount_p = $discount_p - round($discountCode->discount_percentage/100, 2);
            }
        }
        
        $discount = round($subtotal - ($subtotal * $discount_p) ,2);//es el valor que se descuenta al cte
        $subtotal_d = round($subtotal - $discount, 2);//es el subtotal con el descuento aplicado
        $tax = round($subtotal_d * (setting('site.tax_rate')/100),2);

        $total = round($subtotal_d + $tax ,2);

    	return view('cart', compact('cart','subtotal_d','subtotal','tax', 'total','discount'));
    }

    public function add(Product $product)
    {
    		
        $cart= \Session::get('cart');
        // Se guarda en el carrito el producto en la posicion de su id
        
        $items = $cart['items'];
        
        
    	if(isset($items[$product->id])){
            $product->quantity=$items[$product->id]->quantity+1;
        }else{
            $product->quantity=1;
        }

        $items[$product->id]= $product ;

        $cart['items'] = $items;
        \Session::put('cart',$cart);
        return redirect()->route('cart-show');
    }

    public function delete(Product $product)
    {
        $cart= \Session::get('cart');
        $items = $cart['items'];
        // Se elimina de la variable 'cart' (Array) el producto con el id 
        unset($items[$product->id]);
        
        // Si solo queda el codigo de descuento almacenado en el carrito se elimina
        if( count($items)===1 && isset($cart['discountCode']) ){
            
            unset($cart['discountCode']);
        }

        $cart['items'] = $items;
    	\Session::put('cart', $cart);

    	return redirect()->route('cart-show');
    	// return dd($cart);
    }
    
    // Vacia todo el carrito 
    public function trash()
    {
        \Session::forget('cart');
        return redirect()->route('cart-show');
    }

    // Actualizar el producto almacenado en el carrito
    public function update(Product $product, $quantity){
       
        $cart= \Session::get('cart');
        $items = $cart['items'];
        
        $product->quantity= $quantity;
        $items[$product->id]= $product ;
        $cart['items'] = $items;
        
        \Session::put('cart',$cart);  

        return redirect()->route('cart-show');
    }
    
    // Agrega un codigo de descuento en el carrito
    public function addDiscountCode($discountCode)
    {
        $cart= \Session::get('cart');
    	
        // Se obtiene la ultima informacion registrada de un cliente para extraer su dni
        $logged_user_cust_inf = CustomerInformation::where('user_id', Auth::user()->id)->orderBy('created_at','DESC')->first();

        $disc_codes = DiscountCode::where("code", $discountCode)->first();
        if ($logged_user_cust_inf) {
            if ($disc_codes) {
                // Se compara la ci con la ci encriptada guardada con el codigo
                if ($disc_codes->ci == $logged_user_cust_inf->dni) {
                    $cart['discountCode'] = $disc_codes;
                }else{
                    return response()->json([
                        "message" => "Codigo no valido"
                    ], 500);
                }
            }else{
                return response()->json([
                    "message" => "Codigo no existe"
                ], 500);
            }
        } else {
            return response()->json([
                "message" => "Debe ingresar su informacion primero",
                "route" => route('myaccount.account-information')
            ], 500);
        }
        \Session::put('cart',$cart);
        return response()->json([
            "message" => "Codigo correcto",
            "route" => route('cart-show')
        ]);

    }

    // Remueve el codigo de descuento
    public function removeDiscountCode()
    {
    		
        $cart= \Session::get('cart');
    	unset($cart['discountCode']);
        \Session::put('cart',$cart);
        return route('cart-show');
    }

    // Setea la direccion de envio en el carrito
    public function setAddress($address)
    {	
        $cart= \Session::get('cart');
        $address_finded = CustomerAddress::join('cities as ci','customer_addresses.city_id','ci.id')
                                            ->join('provinces as pr','ci.province_id','pr.id')
                                            ->join('countries as co','pr.country_id','co.id')
                                            ->select('customer_addresses.*','ci.city', 'pr.province', 'co.country')
                                            ->where('customer_addresses.id', $address)
                                            ->first();
        if ($address_finded) {
            $cart['shipping_address'] = $address_finded;
        }                                    

    	// $cart['shipping_address'] = CustomerAddress::find($address);
        \Session::put('cart',$cart);
        return redirect()->route('checkout');
    }

    public function setShippingMethod($shippingMethod)
    {	
        $cart= \Session::get('cart');

        $shippingMethod = ShippingMethod::find($shippingMethod);

        if ($shippingMethod) {
            $cart['shipping_method'] = $shippingMethod;
        }
        
    	// $cart['shipping_address'] = CustomerAddress::find($address);
        \Session::put('cart',$cart);
        return redirect()->route('checkout');
    }
}
