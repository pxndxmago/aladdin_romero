<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LocationController extends Controller
{
    public function provinces($country_id){
        return DB::SELECT("SELECT * FROM provinces WHERE country_id = $country_id");
    }

    public function cities($province_id){
        return DB::SELECT("SELECT * FROM cities WHERE province_id = $province_id");
    }
}
