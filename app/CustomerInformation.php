<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerInformation extends Model
{
    use SoftDeletes;
    protected $fillable= [
        'first_name',
        'last_name', 
        'dni', 
        'telephone', 
        'mobile', 
        'email',
        'user_id'
    ];
    protected $dates = ['deleted_at'];
}
