<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Category extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public $fillable = ['category_name','parent_category_id'];

    protected $with = ['children'];

    //each category might have one parent
    public function parent()
    {
        // return $this->belongsToOne(Category::class, 'parent_category_id');
        return $this->hasOne( Category::class, 'id', 'parent_category_id' );
    }
    public function getAllParents(){
        $parents = collect([]);
        $parent = $this->parent;
        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }
        return $parents;
    }
    //each category might have multiple children
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_category_id', 'id')->orderBy('category_name', 'asc');
    }
}
